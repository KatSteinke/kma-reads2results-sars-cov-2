"""
Extract metadata for upload to SSI from MADS report and the sample sheet generated in the
SARS-CoV-2 analysis pipeline.
"""

__author__ = "Kat Steinke"

import logging
import pathlib
import re
from argparse import ArgumentParser
from datetime import datetime, timedelta
from typing import Dict

import pandas as pd
import yaml

import check_config
from helpers import get_number_letter_combination
import pipeline_config
import version
__version__ = version.__version__


# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# start logging
logger = logging.getLogger("extract_metadata")
logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
console_log.setLevel(logging.WARNING)
logger.addHandler(console_log)

def get_highest_depth(depth_group_1: pathlib.Path, depth_group_2: pathlib.Path) -> pd.DataFrame:
    """From ARTIC depth files giving depth values by position for both read groups in a sample,
    extract the highest depth at each position.
    Arguments:
        depth_group_1:  depth file for read group 1
        depth_group_2:  depth file for read group 2
    Returns:
        The higher of the two depth values for each position, represented as a dataframe.
    """
    # sanity check: do base names of the two read groups match?
    if not depth_group_1.with_suffix("").with_suffix("").name == depth_group_2.with_suffix("").with_suffix("").name:
        raise ValueError("Depth files for different samples supplied. "
                         "Names of depth files must match until .1/2.depths")
    if not depth_group_1.exists():
        raise FileNotFoundError("Depth file for read group 1 not found")
    if not depth_group_2.exists():
        raise FileNotFoundError("Depth file for read group 2 not found")
    # do the extensions match with the order?
    if not depth_group_1.name.endswith("1.depths"):
        raise ValueError("Depth file for read group 1 must end in .1.depths.")
    if not depth_group_2.name.endswith("2.depths"):
        raise ValueError("Depth file for read group 2 must end in .2.depths.")
    read_group_1_depths = pd.read_csv(depth_group_1, sep="\t", names=["reference", "read_group",
                                                                      "position", "depth"])
    # sanity check: does the read group number match? Backwards compatibility: older versions
    # include primer category thing
    if not ((read_group_1_depths["read_group"] == 1).all()
            or (read_group_1_depths["read_group"] == "nCoV-2019_1").all()):
        raise ValueError("Depth file for read group 1 contained values for another read group")
    read_group_2_depths = pd.read_csv(depth_group_2, sep="\t", names=["reference", "read_group",
                                                                      "position", "depth"])
    # sanity check: does the read group number match?
    if not ((read_group_2_depths["read_group"] == 2).all()
            or (read_group_2_depths["read_group"] == "nCoV-2019_2").all()):
        raise ValueError("Depth file for read group 2 contained values for another read group")
    read_groups_both = pd.merge(read_group_1_depths[["position", "depth"]],
                                read_group_2_depths[["position", "depth"]],
                                on="position", suffixes=["_group_1", "_group_2"])
    read_groups_both["max_depth"] = read_groups_both[["depth_group_1", "depth_group_2"]].max(axis=1)
    return read_groups_both[["position", "max_depth"]]

# for every directory in the ARTIC directory:
# check .depths files
# get maximum coverage
# average over genome length
# TODO: restructure - is this needed?
def get_average_depth(depth_group_1: pathlib.Path, depth_group_2: pathlib.Path) -> float:
    """Find the average depth across all positions in a pair of depth files, with the depth
    at each position being the higher of the two recorded.
     Arguments:
        depth_group_1:  depth file for read group 1
        depth_group_2:  depth file for read group 2
    Returns:
        The average depth across all positions.
     """
    max_depths = get_highest_depth(depth_group_1, depth_group_2)
    average_depth = max_depths["max_depth"].mean()
    return average_depth

def get_all_depths(artic_dir: pathlib.Path) -> pd.DataFrame:
    """Get mean depth of all samples in an ARTIC result directory.
    Arguments:
        artic_dir:  Directory containing subdirectories with ARTIC results for each sample.
    Returns:
        A DataFrame mapping sample name (parsed out from the directory name) to average depth
    """
    # find sample results
    # for globbing, easiest to look for anything containing a barcode prefix
    sample_dirs = artic_dir.glob(f"*_{workflow_config['barcode_prefix']}*")
    # for anything else we'll need a fancier pattern
    positive_control_pattern = f'({"|".join(workflow_config["sample_number_settings"]["positive_control"])})'
    sample_id_pattern = f'(({workflow_config["sample_number_settings"]["sample_number_format"]}' \
                        f'|{workflow_config["sample_number_settings"]["negative_control"]}' \
                        f'|{positive_control_pattern})_{workflow_config["barcode_format"]})'
    average_depths = []
    for sample_dir in sample_dirs:
        if not sample_dir.is_dir():
            continue
        sample_name = re.search(sample_id_pattern, sample_dir.name).group(1)
        depth_files = sorted(sample_dir.glob("*.coverage_mask.txt.*.depths"))
        if not depth_files:
            logger.warning(f"No depth files found for {sample_name}")
            continue
        # check that there is the right amount of depth files
        if not len(depth_files) == 2:
            raise ValueError(f"{len(depth_files)} depth files found for sample {sample_name}. "
                             f"Exactly 2 depth files are required.")
        # extract depth per position from depth files
        average_depth = get_average_depth(depth_files[0], depth_files[1])
        depth_for_sample = pd.DataFrame(data={"sample": [sample_name], "coverage": [average_depth]})
        average_depths.append(depth_for_sample)
    if not average_depths:
        raise FileNotFoundError("No depth files found for any sample.")
    all_average_depths = pd.concat(average_depths, ignore_index=True).sort_values(by=["sample"]).reset_index(drop=True)
    return all_average_depths


def get_lab_protocol(runsheet: pathlib.Path, protocol_identifier: str,
                     protocol_choices: Dict[str,str]) -> str:
    """Identify wetlab protocol used from information in runsheet.
    Arguments:
        runsheet:               path to runsheet for run
        protocol_identifier:    name of column in runsheet specifying the condition protocol choice
                                depends on ("default" for one sample)
        protocol_choices:       mapping of conditions to protocol versions
    Returns:
         The protocol version used for the condition given in the runsheet
    """
    # identify metadata
    if len(protocol_choices) > 1:
        sheet_metadata = pd.read_excel(runsheet, skiprows=1, nrows=2)
        lab_condition = sheet_metadata[protocol_identifier].iloc[0]
        # complain informatively if there is no protocol version for the condition
        if lab_condition not in protocol_choices:
            raise KeyError(f"No protocol version found for run type {lab_condition}")
        sequencing_protocol = protocol_choices[lab_condition]
    else:
        sequencing_protocol = protocol_choices["default"]
    return sequencing_protocol


# TODO: replace uses of sample sheet with runsheet?

def get_metadata(mads_report_file: pathlib.Path, artic_dir: pathlib.Path,
                 sample_sheet: pathlib.Path, runsheet: pathlib.Path,
                 ssi_dir: str, prefix_translation: Dict[str, str]) -> pd.DataFrame:
    """Automatically generate metadata file for upload to SSI from MADS report and sample sheet.
    Arguments:
        mads_report_file:   Path to MADS report file
        artic_dir:          Directory containing subdirectories with ARTIC results
                            (including consensus fastas) for each sample.
        sample_sheet:       Path to sample sheet generated by SARS-CoV-2 analysis pipeline
        runsheet:           Path to runsheet for run
        ssi_dir:            Name of directory containing files for upload to SSI (date + SHAKID)
        prefix_translation: Translation of input sample number prefix to prefix in MADS report
    Returns:
        Metadata required by SSI (sample ID, patient CPR number, sampling date, SHAKID, file names
        of raw data and consensus fasta files, sequencing platform and CT value).
    """
    # sanity checks: do we have what we need?
    # Missing files will fail later, but we may as well fail early and informatively.
    if not mads_report_file.exists():
        raise FileNotFoundError("MADS report file could not be found.")
    if not artic_dir.exists():
        raise FileNotFoundError("ARTIC directory could not be found.") # TODO make more specific?
    if not artic_dir.is_dir():
        raise NotADirectoryError("Specified path for ARTIC directory is not a directory.")
    # identify output directory - parent directory of the ARTIC directory
    output_dir = artic_dir.parent
    if not sample_sheet.exists():
        raise FileNotFoundError("Sample sheet could not be found.")
    if not ssi_dir:
        raise ValueError("No name given for SSI directory.")
    if not (output_dir / ssi_dir).exists():
        raise FileNotFoundError(f"SSI directory not found in expected location "
                                f"({str(output_dir / ssi_dir)}).")

    # mark start of new run in logfile for easier troubleshooting
    logger.info("Starting metadata file generation.")

    # check if MADS report is current
    mads_change_date = datetime.fromtimestamp(mads_report_file.stat().st_mtime)
    mads_file_age = datetime.now() - mads_change_date
    if mads_file_age > timedelta(days=1):
        logger.warning("Warning: MADS report is older than one day.")
    # get MADS report
    mads_report = pd.read_csv(mads_report_file, encoding="latin1", dtype={"afsendt": str,
                                                                          "cprnr.": str,
                                                                          "prøvenr": str})
    # format sample ids in MADS report to match those from runsheet etc.
    samples_total = pd.read_csv(sample_sheet, dtype={"KMA nr": str, "Barkode NB": str})
    samples_total["sample_name_full"] = samples_total["KMA nr"] + "_" + samples_total["Barkode NB"]
    # get coverage for each sample
    coverage_for_samples = get_all_depths(artic_dir)
    samples_total = samples_total.merge(coverage_for_samples, how="left",
                                        left_on="sample_name_full",
                                        right_on="sample")

    # find consensus fasta files, get names
    consensus_fastas = [consensus_fasta.with_suffix('').stem
                        for consensus_fasta in artic_dir.glob("*/*.consensus.fasta")]
    # parse sample ID out of consensus fastas instead!
    # build pattern
    positive_control_pattern = f'({"|".join(workflow_config["sample_number_settings"]["positive_control"].keys())})'
    sample_id_pattern = f'_(' \
                        f'({workflow_config["sample_number_settings"]["sample_number_format"]}' \
                        f'|{workflow_config["sample_number_settings"]["negative_control"]}' \
                        f'|{positive_control_pattern})' \
                        f'_{workflow_config["barcode_format"]})'
    consensus_fastas = [re.search(sample_id_pattern, filename).group(1)
                        for filename in consensus_fastas]
    # sanity check: do samples match?
    # if we have more consensus fastas then samples, something's gone very wrong
    if set(consensus_fastas) - set(samples_total["sample_name_full"].tolist()):
        logger.warning("Warning: more consensus files than samples entered")

    # filter out controls before checking consensus files since SSI doesn't get these
    samples_total = samples_total[~samples_total["KMA nr"].str.match(workflow_config["sample_number_settings"]["negative_control"])]
    samples_total = samples_total[~samples_total["KMA nr"].isin(workflow_config["sample_number_settings"]["positive_control"].keys())]
    # if we have more samples than consensus fastas, there weren't enough reads for some of the samples
    missing_fastas = {str(missing_fasta) for missing_fasta in (set(samples_total["sample_name_full"].tolist())
                                                               - set(consensus_fastas))}
    if missing_fastas:
        logger.warning(f"Warning: consensus files missing for {str(sorted(missing_fastas))}")

    # check if we have duplicates
    if samples_total.duplicated(subset=["KMA nr"]).any():
        duplicated_fastas = list(samples_total[samples_total.duplicated(subset=["KMA nr"])]["KMA nr"])
        logger.warning(f"Warning: multiple consensus files for sample(s) {str(duplicated_fastas)}. "
                       f"Select one for manual upload and correct zip archive and metadata file accordingly.")

    # TODO: prettier way?
    metadata_report = pd.DataFrame()
    metadata_report["sample_id"] = samples_total["KMA nr"]
    metadata_report["coverage"] = samples_total["coverage"]
    # get raw data/consensus file names
    # these don't contain the version number, so we need to strip it out
    ssi_base = re.sub(r"_v?[0-9]+(_[0-9]+)?", "", ssi_dir)
    metadata_report["consensus_filename"] = metadata_report["sample_id"].apply(
        lambda x: f"{ssi_base}.{x}.fasta")
    # check if files actually are there, alert if they're not
    consensus_files_ssi = {ssi_file.name for ssi_file in (output_dir / ssi_dir).glob("*.fasta")}
    missing_in_ssi = set(metadata_report["consensus_filename"].tolist()) - consensus_files_ssi
    if missing_in_ssi:
        logger.warning(
            f"Warning: the following consensus file(s) were found in ARTIC directory "
            f"but not SSI directory: {sorted(missing_in_ssi)}")
    metadata_report["raw_file_name"] = ""  # blank for now

    # convert sample numbers to P/H-based - parse out start of pattern
    # for now: less compact regex: for each number in number_to_letter, add it to the pattern with a |
    start_options = "|".join(prefix_translation.keys())
    start_pattern = f"^({start_options})"
    metadata_report["sample_id"] = metadata_report["sample_id"].apply(lambda x: re.sub(start_pattern,
                                                                                       lambda match:
                                                                                       prefix_translation.get(match.group(),
                                                                                                              match.group()),
                                                                                       x))
    # get CPR number and sampling date from MADS
    metadata_report = metadata_report.merge(mads_report, how="left", left_on="sample_id",
                                            right_on="prøvenr")[["sample_id", "cprnr.", "afsendt",
                                                                 "raw_file_name",
                                                                 "consensus_filename",
                                                                 "coverage"]]
    # if any sample IDs weren't in MADS, they will be lost with an inner join, so we use this to test if there
    # are any left over
    samples_not_in_mads = set(metadata_report["sample_id"].tolist()) \
                          - set(metadata_report.merge(mads_report,left_on="sample_id",
                                                      right_on="prøvenr")["sample_id"].tolist())


    if samples_not_in_mads:
        logger.warning(f"Warning: sample(s) {str(sorted(samples_not_in_mads))} not found in MADS.")

    # we test separately for blank CPR or sent date
    if metadata_report["cprnr."].isnull().values.any():
        samples_no_cpr = metadata_report[metadata_report["cprnr."].isnull()]["sample_id"].tolist()
        logger.warning(f"Warning: no CPR number reported for sample(s) {str(sorted(samples_no_cpr))}")
    if metadata_report["afsendt"].isnull().values.any():
        samples_no_date = metadata_report[metadata_report["afsendt"].isnull()]["sample_id"].tolist()
        logger.warning(f"Warning: no sampling date reported for sample(s) {str(sorted(samples_no_date))}")
    # now we've checked this, we can discard anything that doesn't have a consensus fasta
    sample_id_missing_fastas = samples_total[samples_total["sample_name_full"].isin(missing_fastas)]["KMA nr"].tolist()
    metadata_report = metadata_report[~metadata_report["sample_id"].isin(sample_id_missing_fastas)]
    metadata_report = metadata_report.rename(columns={"cprnr.": "cpr", "afsendt": "sampling_date"})
    # change sampling date format - TODO: simplify to avoid having to import library?
    metadata_report["sampling_date"] = metadata_report["sampling_date"].apply(
        lambda x: datetime.strftime(datetime.strptime(x, "%d%m%Y"),"%Y-%m-%d")
        if pd.notna(x) else pd.NA)

    # add constant values
    metadata_report["kma_id"] = workflow_config["shakid"]
    metadata_report["platform"] = "nanopore"
    metadata_report["ct"] = "NA"
    metadata_report["primer_scheme"] = f"artic_v{workflow_config['primer_version']}"
    protocol_version = get_lab_protocol(runsheet, workflow_config['protocol_identifier'],
                                        workflow_config['sequencing_protocol'])
    metadata_report["pipeline"] = f"{protocol_version}_KMAReads2Results_v{__version__}"
    # sort columns
    metadata_report = metadata_report[["sample_id", "cpr", "sampling_date", "kma_id",
                                       "raw_file_name", "consensus_filename", "platform", "ct",
                                       "coverage", "primer_scheme", "pipeline"]]

    logger.info("Metadata file generated.")
    return metadata_report

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Generate metadata sheet from MADS report and ARTIC results")
    arg_parser.add_argument("mads_report", help="File with MADS report on samples")
    arg_parser.add_argument("artic_path",
                            help="Directory with consensus fasta files from ARTIC pipeline")
    arg_parser.add_argument("samplesheet",
                            help="Sample sheet generated by analysis pipeline "
                                 "(sample name + barcode as CSV)")
    arg_parser.add_argument("runsheet", help="Path to runsheet for run")
    arg_parser.add_argument("ssi_name",
                            help="Name (not path!) of directory with files for SSI (date + SHAKID)")
    arg_parser.add_argument('-l', '--logfile',
                            help="Path to logfile for warnings (default: metadata_log.txt)",
                            default="metadata_log.txt")
    arg_parser.add_argument('-o', '--outfile',
                            help="Path to output file (default: [SSI_NAME].tsv in output folder",
                            default="")
    arg_parser.add_argument("--workflow_config_file",
                            help="Config file for run (overrides default config given in script,"
                                 " can be overridden by commandline options)")
    args = arg_parser.parse_args()
    mads_report_path = pathlib.Path(args.mads_report)
    artic_path = pathlib.Path(args.artic_path)
    samplesheet = pathlib.Path(args.samplesheet)
    run_sheet = pathlib.Path(args.runsheet)
    ssi_name = args.ssi_name
    logfile_path = pathlib.Path(args.logfile)
    outfile = args.outfile
    out_dir = artic_path.parent
    # log to file
    log_file = logging.FileHandler(logfile_path)
    log_file.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_file.setFormatter(logfile_formatter)

    logger.addHandler(log_file)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file)
        if not default_config_file.exists():
            raise FileNotFoundError("Config file not found.")
        with open(default_config_file, "r", encoding="utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        check_config.check_config(workflow_config)
    if outfile:
        outfile = pathlib.Path(outfile)
    else:
        outfile = out_dir / f"{ssi_name}.tsv"
    # set logging to file
    log_file = logging.FileHandler(logfile_path)
    log_file.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_file.setFormatter(logfile_formatter)
    logger.addHandler(log_file)

    prefix_translate = get_number_letter_combination(workflow_config["sample_number_settings"]["number_to_letter"],
                                                       workflow_config["sample_number_settings"]["sample_numbers_in"],
                                                       workflow_config["sample_number_settings"]["sample_numbers_out"])
    metadata_out = get_metadata(mads_report_path, artic_path, samplesheet, run_sheet, ssi_name,
                                prefix_translate)

    metadata_out.to_csv(path_or_buf=outfile, sep="\t", index=False)
