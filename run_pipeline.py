#!/usr/bin/env python3

"""
Run SARS-CoV-2 analysis pipeline consisting of consensus file generation with ARTIC and
subtyping with Pangolin and Nextclade in Snakemake.
"""


__author__ = "Kat Steinke"

import pathlib
import re
import readline
import subprocess
import sys
import warnings

from argparse import ArgumentParser
from datetime import date
from typing import List

import pandas as pd
import yaml

import check_config
import pipeline_config
import version
from extract_metadata import get_lab_protocol # TODO: move this?

# workaround for version for now
__version__ = version.__version__

# TODO: log?

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# handle bad filenames separately
class BadPathError(Exception):
    """Exception raised when a path to be created contains illegal characters or reserved
    filenames. Workaround to be able to distinguish from incorrect user input in classic mode.
    """
    pass


def find_rundir(run_dir: pathlib.Path, minion_basedir: pathlib.Path) -> pathlib.Path:
    """Check whether run directory exists as full path or directory in MinION dir and
    adjust path of run directory accordingly.
    Arguments:
        run_dir:        absolute or relative path to run directory
        minion_basedir: absolute path to directory of MinION results
    Returns:
        The unchanged run directory if it exists, or the full path to the directory
        within the MinION dir if this was given.
    """
    # we only need to do something if the directory doesn't exist:
    if not run_dir.exists():
        # ...check in MinION dir
        if (minion_basedir / run_dir).exists():
            run_dir = minion_basedir / run_dir
        # if neither of them exists, complain and stop
        else:
            raise FileNotFoundError(f"""{str(run_dir)} or {str(minion_basedir / run_dir)} does not exist 
Aborting ARTIC pipeline...""")
    # Check if fastq_pass folder exist
    check_fastq_pass = list(run_dir.glob("rawdata/*/fastq_pass"))
    if not check_fastq_pass:
        raise FileNotFoundError(f"""fastq_pass folder(s) not found in expected location:
{str(run_dir)}/rawdata/*/fastq_pass
Ensure correct directory and/or directory structure is used.
Aborting ARTIC pipeline...""")
    else:
        print(f"Data is retrieved from following folders: \n {str([str(fastq_dir) for fastq_dir in check_fastq_pass])}")
    return run_dir

def get_existing_path(path_to_check: pathlib.Path) -> pathlib.Path:
    """Recursively check if path exists, else go down one level until an existing path is found.
    Adapted from https://stackoverflow.com/a/39489505
    Arguments:
        path_to_check:  Path whose components should be checked
    Returns:
        The existing parts of the path
    """
    if path_to_check.exists():
        return path_to_check
    return get_existing_path(path_to_check.parent)

def get_clean_outdir(outdir_path: pathlib.Path) -> pathlib.Path:
    """Check which parts of a path already exist and sanitize the new ones by removing spaces
    and special characters if needed.
    Arguments:
        outdir_path:    Path to check for spaces and special characters
    Returns:
         The sanitized version of the path
    """
    illegal_in_windows = r'[<>:"|?*]'
    # "magic" filenames in Windows, should not be used
    #device_names = {"CON", "PRN", "AUX", "NUL", "COM0", "COM1", "COM2", "COM3", "COM4", "COM5",
    #                "COM6", "COM7", "COM8", "COM9", "LPT0", "LPT1", "LPT2", "LPT3", "LPT4",
    #                "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"}
    # find out until which point path exists
    existing_path = get_existing_path(outdir_path)
    # for anything up to that, if it contains a "bad" character or a space, fail immediately
    if " " in str(existing_path):
        raise BadPathError("The path you are trying to save results to contains a space "
                           "in an existing folder's name. This can break the pipeline. "
                           "\nAborting....")
    if re.search(illegal_in_windows, str(existing_path)):
        raise BadPathError("The path you are trying to save results to contains a character that "
                           "can't be used in Windows in an existing folder's name. "
                           "This can break the pipeline. "
                           "\nAborting....")
    if existing_path == outdir_path:
        return existing_path
    # get rest of the path: relative to existing path
    new_path = outdir_path.relative_to(existing_path)
    # for the new part of the filename:
    # otherwise remove the "illegal" characters and substitute spaces with underscores
    # TODO: any way to use pathlib for this?
    plain_path = str(new_path)
    plain_path = plain_path.replace(" ", "_")
    plain_path = re.sub(illegal_in_windows, "", plain_path)
    cleaned_path = existing_path / plain_path
    # if the path contains a device name, stop and complain
    # if device_names.intersection(set(new_path.parts)):
    if pathlib.PureWindowsPath(cleaned_path).is_reserved():
        raise BadPathError("The path you are trying to save results to contains a name that is "
                           "reserved in Windows. Cannot create this path. \n"
                           "Aborting....")
    return cleaned_path


def validate_runsheet_format(run_sheet: pathlib.Path) -> None:
    """Identify wrong sample name or barcode formats in runsheet.
    Arguments:
        run_sheet:   Path to runsheet to check
    """
    # check if runsheet was formatted correctly, throw error and print everything that's wrong in one go
    # get only the relevant columns here: KMA nr, Ct værdi, barkode; skip the first three lines (not usable for
    # parsing)
    sheet_data = pd.read_excel(run_sheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                            "Barkode NB": str})
    sheet_issues = False
    data_missing = False
    # set up record of issues so they can all be printed at once
    fail_record = "The following issue(s) were detected with the runsheet:"
    # check that sample numbers and barcodes have been entered, fail early if so
    no_sample_ids = sheet_data["KMA nr"].isna().all()
    if no_sample_ids:
        fail_record += "\nNo sample IDs found."
        data_missing = True
    no_barcodes = sheet_data["Barkode NB"].isna().all()
    if no_barcodes:
        fail_record += "\nNo barcodes found."
        data_missing = True
    if data_missing:
        raise ValueError(fail_record)
    # now we can be sure there are sample IDs and barcodes, we can check them
    # start by checking if we have the same amount of sample IDs and barcodes
    amount_sample_ids = sheet_data["KMA nr"].dropna().size
    amount_barcodes = sheet_data["Barkode NB"].dropna().size
    if amount_sample_ids != amount_barcodes:
        sheet_issues = True
        fail_record += f"\nAmount of sample IDs and barcodes don't match. " \
                       f"There are {amount_sample_ids} sample IDs but {amount_barcodes} barcodes."
        # check that positive and negative controls are included
        # for positive controls: see if there are any sample numbers matching the controls
    # check controls
    positive_controls_in_sheet = set(workflow_config["sample_number_settings"]["positive_control"].keys()).intersection(
        sheet_data["KMA nr"])
    if not positive_controls_in_sheet:
        sheet_issues = True
        fail_record += "No positive controls given in runsheet."
    # for negative controls: see if there is anything matching negative control pattern
    negative_controls_in_sheet = sheet_data["KMA nr"].str.fullmatch(
        workflow_config["sample_number_settings"]["negative_control"],
        na=False)
    if not negative_controls_in_sheet.any():
        sheet_issues = True
        fail_record += "No negative controls given in runsheet."
    # positive control format has been checked, remove positive controls before checking the rest
    # TODO: could be more explicit by compiling positive control list as pattern but
    #  likely at the cost of speed
    sheet_data = sheet_data.loc[
        ~sheet_data["KMA nr"].isin(workflow_config["sample_number_settings"]["positive_control"].keys())]
    # TODO: represent as raw string?
    id_pattern = '^(' \
                 + workflow_config["sample_number_settings"]["sample_number_format"] \
                 + '|' \
                 + workflow_config["sample_number_settings"]["negative_control"] \
                 + ')$'
    fail_ids = sheet_data["KMA nr"][~sheet_data["KMA nr"].apply(str).str.match(id_pattern,
                                                                               na=False)].dropna().tolist()
    if fail_ids:
        sheet_issues = True
        # adapt error message to sample number format - TODO: do we need a second sanity check here
        if workflow_config['sample_number_settings']['sample_numbers_in'] == "number":
            allowed_start = workflow_config['sample_number_settings']['number_to_letter'].keys()
        else:
            allowed_start = workflow_config['sample_number_settings']['number_to_letter'].values()
        # TODO: make sample number length variable?
        fail_record += f"""\nSample IDs {fail_ids} are not valid. \
Sample IDs must start with {" or ".join(allowed_start)} followed by eight numbers (six if leaving out year). \
Negative controls must be given in the format {workflow_config['sample_number_settings']['negative_control']}. \
Please correct sample IDs in runsheet."""

    # check that Ct value doesn't have anything that's not a number
    #ct_format = sheet_data["CT/CP værdi"]
    # check that barcodes have correct format: use negative lookbehind to find all lines not matching the format
    fail_barcodes = sheet_data["Barkode NB"][~sheet_data["Barkode NB"].apply(str).str.match(workflow_config["barcode_format"],
                                                                                            na=False)].dropna().tolist()
    if fail_barcodes:
        sheet_issues = True
        # here we append to the record of issues
        fail_record += f"\nBarcodes {fail_barcodes} are not valid barcodes. " \
                       f"Barcodes must consist of {workflow_config['barcode_prefix']} " \
                       f"+ a number between 01 and 96."
    # duplicated sample numbers have been checked in the separate runsheet check
    # duplicated barcodes indicate a serious issue though
    if any(sheet_data["Barkode NB"].dropna().duplicated()):
        sheet_issues = True
        duplicated_barcodes = sheet_data["Barkode NB"][sheet_data["Barkode NB"].duplicated()].dropna().unique()
        fail_record += f"\nBarcode(s) {duplicated_barcodes} are duplicated."
    if sheet_issues:
        raise ValueError(fail_record)

def check_paths(paths_to_check: List[pathlib.Path]) -> None:
    """Check whether required paths (LIS report if used, databases,...) are present.
    Arguments:
        paths_to_check: Paths for whose presence to check
    Raises:
        FileNotFoundError if one or more paths don't exist
    """
    missing_paths = []
    for path_to_check in paths_to_check:
        if not path_to_check.exists():
            missing_paths.append(str(path_to_check))
    if missing_paths:
        all_missing = '\n'.join(missing_paths)
        raise FileNotFoundError(f"The following paths cannot be found:\n"
                                f"{all_missing}\n"
                                f"Check if they have been renamed, moved or deleted.")


def validate_ssi_dir_name(ssi_name: str) -> None:
    """Check if name of SSI directory matches the correct format (date as YYMMDD and SHAKID
    separated by period, followed by _v2 etc. for reanalyses and _2 etc. for additional submission
    of new data on the same day).
    Arguments:
        ssi_name:   Name of SSI folder to check
    """
    # regex format: date and ID are mandatory, number is optional; marker for reanalysis first
    check_ssi = re.fullmatch(r"[0-9]{6}\.[0-9]{7}(_v?[0-9]+(_[0-9]+)?)?", ssi_name)
    if not check_ssi:
        raise ValueError(
            "Invalid format for SSI directory. SSI directory must follow the format YYMMDD.KMASHAK,"
            " with _v2 etc. for reanalyses and _2 etc. for multiple runs on one day.")

if __name__ == "__main__":
    # deactivate traceback in classic mode
    def quiet_except_hook(exc_type, exc_value, traceback):
        """
        Hide traceback unless a debug flag is specified.
        Adapted from https://stackoverflow.com/a/27674608/15704972
        and
        https://stackoverflow.com/questions/6598053/python-global-exception-handling/6598286#6598286
        """
        # if the debug flag is set, give the original exception
        if debug:
            sys.__excepthook__(exc_type, exc_value, traceback)
        else:
            print(f"\n{exc_type.__name__}: {exc_value}")
    sys.excepthook = quiet_except_hook
    # traceback is on from the start since if things fail so early we'll want to know why
    debug = True
    # we also want to disable the warning about DataValidation as it won't impact the data
    # (compare https://stackoverflow.com/a/66571471/15704972)
    warnings.filterwarnings('ignore',
                            message="Data Validation extension is not supported and will be removed",
                            module="openpyxl")
    arg_parser = ArgumentParser(description="Start SARS-CoV-2 analysis pipeline")
    arg_parser.add_argument("--rundir", help="Full path or name of sequencing folder")
    arg_parser.add_argument("--runsheet", help="Path to runsheet")
    arg_parser.add_argument("--outdir",
                            help=f"Path to output directory "
                                 f"(default: "
                                 f"{workflow_config['paths']['output_base_path']}/[name of rundir])",
                            default=False)
    arg_parser.add_argument("--ssi_dir",
                            help="Name of folder for data upload to SSI (default: date + SHAKID)",
                            default=False)
    arg_parser.add_argument("--primer_version",
                            help=f"Primer version used for run (3, 4, midnight, ont_midnight_3; "
                                 f"default: {workflow_config['primer_version']})",
                            choices=["3", "4", "4.1", "midnight", "ont_midnight_3"], default=False)
    arg_parser.add_argument("--workflow_config_file",
                            help="Config file for run (overrides default config given in script, "
                                 "can be overridden by commandline options)")
    arg_parser.add_argument("--continue_pipeline", action="store_true",
                            help="Continue pipeline after interruption")
    arg_parser.add_argument("--test_run", action="store_true",
                            help="Start a test run (don't add results to all_results and all_fastas; "
                                 "give more detailed error messages)")
    arg_parser.add_argument("--reanalyze", action="store_true",
                            help="Reanalyze (part of) a run: add results to all_results but add a "
                                 "*new* file to all_fastas. Overridden by test_run.")
    arg_parser.add_argument("--snake_flags", nargs="*",
                            help="Flags to be passed to Snakemake, enclosed in quotes")
    args = arg_parser.parse_args()
    # parse and check required arguments
    # required arguments are also named for ease of use, but this means we need to check for them explicitly
    # check if args have been specified in the first place - hacky workaround with sys.argv
    if len(sys.argv) == 1:
        readline.set_completer_delims('\t\n=') # allow tab completion of paths
        readline.parse_and_bind("tab: complete")
        # as we want to be able to test this as well, we now take settings from the default config
        debug = workflow_config["debug"]
        append_to_databases = workflow_config["append_to_databases"]
        primer_version = workflow_config["primer_version"]
        # we can't have any snakemake arguments
        snake_flags = ""
        # set directories
        minion_base = pathlib.Path(workflow_config["paths"]["minion_path"])
        output_base = pathlib.Path(workflow_config["paths"]["output_base_path"])
        # run "classic" mode
        print("### ARTIC reference genome assembly")
        print("# Setup analysis -------------------------------")
        # ask for sequencing folder as before - dragging and dropping introduces single quotes
        # that need to be stripped
        rundir = pathlib.Path(input("Type full path or name of sequencing folder and press enter: ").strip().strip("'"))
        rundir = find_rundir(rundir, minion_base)
        # remove prefix; remove ARTIC version name if needed
        rundir_name = rundir.name.split("/")[0]
        # check dir and clean
        try:
            # check if output directory is wrong to begin with
            output_dir = get_clean_outdir((output_base / rundir_name))
            # ask user for confirmation
            target_accept = input(f"Do you accept {str(output_dir)} as target folder [y/n]")
            if target_accept == "n":
                output_dir = pathlib.Path(input("Type full path or name of target folder and press enter: ").strip().strip("'"))
            elif target_accept == "y":
                pass
            else:
                raise ValueError("Output folder not entered. Aborting")
        except BadPathError as path_err:
            print("The default target folder contains characters that can break the pipeline.")
            output_dir = pathlib.Path(input("Type full path to new target folder and press enter: ").strip().strip("'"))
        # clean output dir again since it can be changed
        output_dir = get_clean_outdir(output_dir)
        # get folder for output data to SSI - infer but ask
        ssi_dir_date = date.strftime(date.today(), "%y%m%d")
        ssi_accept = input(f"Do you accept {ssi_dir_date}.{str(workflow_config['shakid'])} as name for SSI folder [y/n]")
        if ssi_accept == "n":
            ssi_dir = input("Type name of SSI folder and press enter:")
            validate_ssi_dir_name(ssi_dir)
        elif ssi_accept == "y":
            ssi_dir = f"{ssi_dir_date}.{str(workflow_config['shakid'])}"
        else:
            raise ValueError("SSI folder not entered. Aborting")

        runsheet = pathlib.Path(input("Enter path to runsheet: ").strip().strip("'")).resolve()
        # dealing with a stopped pipeline will likely be handled from commandline mode, so we may
        # get away with defaulting to aborting the pipeline if run on a directory with results from
        # classic mode
        continue_after_stop = False

    else:
        if not args.rundir:
            raise ValueError("Run directory not specified.")
        if not args.runsheet:
            raise ValueError("Runsheet not specified.")
        # load config if present
        if args.workflow_config_file:
            default_config_file = pathlib.Path(args.workflow_config_file).resolve()
            with open(default_config_file, "r", encoding="utf-8") as config_file:
                workflow_config = yaml.safe_load(config_file)
            check_config.check_config(workflow_config)
        rundir = find_rundir(pathlib.Path(args.rundir),
                             pathlib.Path(workflow_config["paths"]["minion_path"]))
        # remove prefix; remove version name if needed
        rundir_name = rundir.name.split("/")[0]

        runsheet = pathlib.Path(args.runsheet).resolve()

        # parse and check optional arguments
        output_dir = args.outdir
        ssi_dir = args.ssi_dir
        if args.snake_flags:
            snake_flags = args.snake_flags[0].split()
        else:
            snake_flags = ""
        # initialize test-related values, to be modified by config or commandline
        debug = workflow_config["debug"]
        append_to_databases = workflow_config['append_to_databases']
        # if no primer version is given...
        if not args.primer_version:
            # we take the configuration from the config file
            primer_version = workflow_config['primer_version']
        # if both are given, commandline overrides config
        else:
            primer_version = args.primer_version
        # recode midnight primers from more human-readable to shorter names
        if primer_version == "midnight":
            primer_version = "1200"
        if primer_version == "ont_midnight_3":
            primer_version = "1200.3"
        continue_after_stop = args.continue_pipeline
        # test_run defaults to False, but we don't want to override config settings if it's missing
        # -> only override if test_run is actively specified
        if args.test_run:
            debug = True
            append_to_databases = False
        # output directory can (easily) have a different name than run directory if specified
        # if not, it's named for the run directory as before
        if not output_dir:
            output_dir = pathlib.Path(workflow_config["paths"]["output_base_path"]) / rundir_name
        else:
            output_dir = pathlib.Path(output_dir)
        # sanitize output directory if needed
        output_dir = get_clean_outdir(output_dir)
        # SSI directory's name can be automatically generated from date/SHAKID...
        if not ssi_dir:
            ssi_dir_date = date.strftime(date.today(), "%y%m%d")
            ssi_dir = f"{ssi_dir_date}.{str(workflow_config['shakid'])}"
        # ...but may be changed in case of reruns etc. - we need to check it matches the format
        else:
            validate_ssi_dir_name(ssi_dir)

    # check if all required paths are present now that config has been specified for sure
    required_paths = [pathlib.Path(static_path)
                      for static_path in workflow_config["paths"].values()]
    # TODO: only check LIS report if used once making that optional
    # if workflow_config["lab_info_system"]["use_lis_features"]:
    required_paths.append(pathlib.Path(workflow_config["lab_info_system"]["lis_report"]))
    check_paths(required_paths)

    # sanity check runsheet (same in both setups)
    if not runsheet.exists():
        raise FileNotFoundError("Runsheet not found")
    # check if there are issues with runsheet here to avoid starting snakemake for a broken runsheet
    validate_runsheet_format(runsheet)

    # create output folder and SSI dir - TODO: can we handle this via snakemake?
    if not output_dir.exists():
        output_dir.mkdir()
    ssi_path = output_dir / ssi_dir
    if not ssi_path.exists():
        ssi_path.mkdir()

    # logging folder needs to be created since we need to log Snakemake output there
    log_path = output_dir / "logs"
    if not log_path.exists():
        log_path.mkdir()

    # check if there already is something in the folder
    results_dir = output_dir / "results"
    artic_dir = output_dir /"artic"
    old_dirs = [data_dir for data_dir in [results_dir, artic_dir] if data_dir.exists()]
    if old_dirs and not continue_after_stop:
        old_dir_prettyprint = '\n'.join([str(old_dir) for old_dir in old_dirs])
        raise FileExistsError(f"Output folders already exist:\n{old_dir_prettyprint}")

    # start by appending to metadata database if needed - will not be done for reruns
    if append_to_databases and not (continue_after_stop or args.reanalyze):
        print("Appending to metadata database...")
        lab_protocol = get_lab_protocol(runsheet, workflow_config['protocol_identifier'],
                                        workflow_config['sequencing_protocol'])
        append_to_database_cmd = ["python3",
                                  str(pathlib.Path(__file__).parent / "metadata-database"
                                      / "parse_nanopore_runsheet.py"),
                                  str(runsheet),
                                  str(rundir),
                                  workflow_config["paths"]["metadata_database"],
                                  workflow_config["lab_info_system"]["lis_report"],
                                  "--platform", workflow_config["platform"],
                                  "--config_file", default_config_file,
                                  "--protocol",
                                  f"{lab_protocol}_primer_{workflow_config['primer_version']}"]
        try:
            subprocess.run(append_to_database_cmd, check=True)
        except subprocess.CalledProcessError as subprocess_err:
            if debug:
                raise subprocess_err
            else:
                print("Appending to metadata database failed.")
                sys.exit(1)
    # test overrides reanalysis, reanalysis overrides routine: only check for reanalysis if
    # appending even is an option
    if append_to_databases:
        if args.reanalyze:
            target_rule = "run_reanalysis"
        else:
            target_rule = "run_all"
    else:
        target_rule = "run_all_test"


    print("Running sequence analysis pipeline...")
    snakemake_cmd = ["snakemake", str(target_rule),
                     "-s", str(pathlib.Path(__file__).parent / "Snakefile"),
                     "--cores", str(workflow_config["snakemake_cores"]), "--use-conda",
                     "--conda-frontend", workflow_config["conda_frontend"],
                     "--conda-prefix",
                     str(pathlib.Path(workflow_config["conda_prefix"]).resolve()),
                     "--keep-going",
                     "--configfile", str(default_config_file),
                     "--config",
                     f'runsheet="{str(runsheet)}"',
                     f'ssi_dir="{str(ssi_dir)}"', f'rundir="{str(rundir)}"',
                     f'outdir="{str(output_dir)}"', f'primer_version="{primer_version}"',
                     f'config_path="{str(default_config_file)}"',
                     *snake_flags]
    subprocess.run(snakemake_cmd) # TODO: error handling with check=True
