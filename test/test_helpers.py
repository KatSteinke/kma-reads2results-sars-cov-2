import re
import unittest

import helpers
import pytest


class TestMappers(unittest.TestCase):
    def test_number_to_letter(self):
        sample_number = "7099000001"
        sample_letter = "P99000001"
        sample_letter_test = re.sub("^70", helpers.map_sample_number_to_letter, sample_number)
        assert sample_letter == sample_letter_test

    def test_number_not_mapped(self):
        sample_number = "8099000001"
        sample_number_test = re.sub("^80", helpers.map_sample_number_to_letter, sample_number)
        assert sample_number == sample_number_test

    def test_letter_to_number(self):
        sample_number = "7099000001"
        sample_letter = "P99000001"
        sample_number_test = re.sub("^P", helpers.map_sample_letter_to_number, sample_letter)
        assert sample_number == sample_number_test

    def test_letter_not_mapped(self):
        sample_letter = "X99000001"
        sample_letter_test = re.sub("^X", helpers.map_sample_letter_to_number, sample_letter)
        assert sample_letter == sample_letter_test

class TestSnakifyPattern(unittest.TestCase):
    def test_no_special_chars(self):
        test_pattern = "test"
        result_pattern = "test"
        assert helpers.double_up_escapes(test_pattern) == result_pattern
    def test_special_chars(self):
        test_pattern = "{}\\"
        result_pattern = "{{}}\\\\"
        assert helpers.double_up_escapes(test_pattern) == result_pattern

class TestTranslateSampleNumbers(unittest.TestCase):
    number_to_letter = {"40": "H", "70": "P"}
    def test_wrong_sample_in(self):
        with pytest.raises(ValueError,
                           match="Invalid initial sample format int. Sample format can only be number or letter"):
            helpers.get_number_letter_combination(self.number_to_letter, "int", "letter")
    def test_wrong_sample_out(self):
        with pytest.raises(ValueError,
                           match="Invalid desired sample format str. Sample format can only be number or letter"):
            helpers.get_number_letter_combination(self.number_to_letter, "number", "str")
    def test_correct_results(self):
        number_to_number = helpers.get_number_letter_combination(self.number_to_letter, "number", "number")
        number_to_letter = helpers.get_number_letter_combination(self.number_to_letter, "number", "letter")
        letter_to_letter = helpers.get_number_letter_combination(self.number_to_letter, "letter", "letter")
        letter_to_number = helpers.get_number_letter_combination(self.number_to_letter, "letter", "number")
        self.assertEqual(number_to_number, {"40": "40", "70": "70"})
        self.assertEqual(number_to_letter, self.number_to_letter)
        self.assertEqual(letter_to_letter, {"H": "H", "P": "P"})
        self.assertEqual(letter_to_number, {"H": "40", "P": "70"})