import pathlib
import re
import unittest

import pandas as pd
import pytest

import make_sample_sheet as mk_sheet

# TODO: add check for samples as numbers

class TestAddYear(unittest.TestCase):
    fake_mads_path = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "fake_mads_data.csv"
    def test_add_year(self):
        test_runsheet = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "test_translate_runsheet.xlsx"
        fake_mads = self.fake_mads_path
        success_translate = pd.read_csv((pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "splice_year_success.csv"),
                                        dtype={"KMA nr": str})
        test_translate = mk_sheet.parse_and_fix_ids(test_runsheet, fake_mads)
        print(test_translate)
        pd.testing.assert_frame_equal(success_translate.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_translate.sort_values(by="KMA nr").reset_index(drop=True))
    def test_multiple_fails(self):
        fail_runsheet = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "test_notinmads_runsheet.xlsx"
        fake_mads = self.fake_mads_path
        with pytest.raises(ValueError,
                           match=re.escape("Samples ['70400000'] were not found in MADS report."
                                           " Please check that sample numbers are correct.\n"
                                           "Samples ['40410000'] were not found in MADS report. "
                                           "Please check that sample numbers are correct.")):
            mk_sheet.parse_and_fix_ids(fail_runsheet, fake_mads)

    def test_different_prefix(self):
        # test how things work with a different prefix
        test_runsheet = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "test_translate_different_prefix.xlsx"
        fake_mads = self.fake_mads_path
        prefix_translation = {"50": "P", "20": "H"}
        success_translate = pd.read_csv(
            (pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "splice_year_different_prefix.csv"),
            dtype={"KMA nr": str})
        test_translate = mk_sheet.parse_and_fix_ids(test_runsheet, fake_mads, prefix_translation)
        pd.testing.assert_frame_equal(success_translate.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_translate.sort_values(by="KMA nr").reset_index(drop=True))
    def test_different_negk(self):
        test_runsheet = pathlib.Path(
            __file__).parent / "data" / "sample_sheet_test" / "test_translate_different_negk.xlsx"
        fake_mads = self.fake_mads_path
        different_negk = "Neg K[0-9]*"
        success_translate = pd.read_csv(
            (pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "splice_year_different_negk.csv"),
            dtype={"KMA nr": str})
        test_translate = mk_sheet.parse_and_fix_ids(test_runsheet, fake_mads,
                                                    negk_format=different_negk)
        pd.testing.assert_frame_equal(success_translate.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_translate.sort_values(by="KMA nr").reset_index(drop=True))
    def test_duplicates(self):
        test_runsheet = pathlib.Path(
            __file__).parent / "data" / "sample_sheet_test" / "test_translate_runsheet_duplicate.xlsx"
        fake_mads = self.fake_mads_path
        success_translate = pd.read_csv(
            (pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "splice_year_duplicate.csv"),
            dtype={"KMA nr": str})
        test_translate = mk_sheet.parse_and_fix_ids(test_runsheet, fake_mads)
        pd.testing.assert_frame_equal(success_translate.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_translate.sort_values(by="KMA nr").reset_index(drop=True))

    def test_different_positive_control(self):
        test_runsheet = pathlib.Path(
            __file__).parent / "data" / "sample_sheet_test" / "test_translate_runsheet_positive_control.xlsx"
        fake_mads = self.fake_mads_path
        success_translate = pd.read_csv(
            (pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "splice_year_posk.csv"),
            dtype={"KMA nr": str})
        test_translate = mk_sheet.parse_and_fix_ids(test_runsheet, fake_mads,
                                                    positive_controls=['87654321'])
        pd.testing.assert_frame_equal(success_translate.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_translate.sort_values(by="KMA nr").reset_index(drop=True))

    def test_only_controls(self):
        test_runsheet = pathlib.Path(
            __file__).parent / "data" / "sample_sheet_test" / "only_controls_runsheet.xlsx"
        fake_mads = self.fake_mads_path
        success_sheet = pd.DataFrame(data = {"KMA nr": ["34567890", "NegK1"],
                                             "Barkode NB": ["RB01", "RB02"],
                                             "CT/CP værdi": [30.0, 100.0]})
        test_sheet = mk_sheet.parse_and_fix_ids(test_runsheet, fake_mads)
        pd.testing.assert_frame_equal(success_sheet.sort_values(by = "KMA nr").reset_index(drop = True),
                                      test_sheet.sort_values(by = "KMA nr").reset_index(drop = True))


class TestAddYearToSubset(unittest.TestCase):
    test_runsheet = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "test_translate_runsheet.xlsx"
    fake_mads = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "fake_mads_data.csv"
    sheet_data = pd.read_excel(test_runsheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                                "Barkode NB": str})
    sheet_data = sheet_data.dropna()

    lab_info_data = pd.read_csv(fake_mads, encoding="latin1", dtype={"afsendt": str, "cprnr.": str,
                                                                     "modtaget": str})
    def test_add_to_subset(self):
        # make dataframe for H-samples
        success_data = pd.DataFrame(data={"KMA nr": ["4021710000", "4021700000", "4022000000"],
                                          "Barkode NB": ["NB03", "NB04", "NB06"],
                                          "CT/CP værdi": [24.0, 35.0, 33.0]})
        test_data = mk_sheet.fix_ids_by_prefix(self.sheet_data, self.lab_info_data, "40", "H")
        pd.testing.assert_frame_equal(success_data.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_data.sort_values(by="KMA nr").reset_index(drop=True))

    def test_prefix_not_in_sheet(self):
        with pytest.raises(ValueError, match="No samples with prefix X found in runsheet."):
            mk_sheet.fix_ids_by_prefix(self.sheet_data, self.lab_info_data, "X", "P")
    def test_sample_not_in_report(self):
        fail_runsheet = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "test_notinmads_runsheet.xlsx"
        fail_data = pd.read_excel(fail_runsheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                                   "Barkode NB": str})
        fail_data = fail_data.dropna()
        with pytest.raises(ValueError,
                           match=re.escape("Samples ['40410000'] were not found in MADS report. "
                                           "Please check that sample numbers are correct.")):
            mk_sheet.fix_ids_by_prefix(fail_data, self.lab_info_data, "40", "H")

    def test_duplicates_in_report(self):
        fail_mads = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "fake_mads_duplicated.csv"
        fail_mads_data = pd.read_csv(fail_mads, encoding="latin1", dtype={"afsendt": str, "cprnr.": str,
                                                                         "modtaget": str})
        with pytest.raises(ValueError,
                           match="MADS report contains duplicated sample numbers. "
                                 "This likely means the report covers multiple years. "
                                 "Get a new MADS report with the correct start date."):
            mk_sheet.fix_ids_by_prefix(self.sheet_data, fail_mads_data, "40", "H")

    def test_numbers_in_report(self):
        numeric_mads = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "fake_mads_numeric.csv"
        numeric_mads_data = pd.read_csv(numeric_mads, encoding="latin1", dtype={"afsendt": str,
                                                                                "cprnr.": str,
                                                                         "modtaget": str})
        test_runsheet = pathlib.Path(__file__).parent / "data" / "sample_sheet_test" / "test_translate_letters.xlsx"
        sheet_data = pd.read_excel(test_runsheet, usecols="A:C", skiprows=3,
                                   dtype={"KMA nr": str, "Barkode NB": str})
        sheet_data = sheet_data.dropna()
        test_data = mk_sheet.fix_ids_by_prefix(sheet_data, numeric_mads_data, "H", "40")
        success_data = pd.DataFrame(data={"KMA nr": ["H21710000", "H21700000", "H22000000"],
                                          "Barkode NB": ["NB03", "NB04", "NB06"],
                                          "CT/CP værdi": [24.0, 35.0, 33.0]})
        pd.testing.assert_frame_equal(success_data.sort_values(by="KMA nr").reset_index(drop=True),
                                      test_data.sort_values(by="KMA nr").reset_index(drop=True))


class TestInferPrefix(unittest.TestCase):
    def test_infer_prefix(self):
        fake_mads = pd.DataFrame(data={"prøvenr": ["P99123456", "H99654321"]})
        sample_sheet = pd.DataFrame(data = {"KMA nr": ["99123456", "99654321"],
                                            "Barkode NB": ["NB01", "NB02"],
                                            "CT/CP værdi": [30.0, 30.0]})
        sheet_with_prefixes = pd.DataFrame(data = {"KMA nr": ["P99123456", "H99654321"],
                                                   "Barkode NB": ["NB01", "NB02"],
                                                   "CT/CP værdi": [30.0, 30.0]})
        test_with_prefixes = mk_sheet.infer_prefix(sample_sheet, fake_mads)
        pd.testing.assert_frame_equal(sheet_with_prefixes, test_with_prefixes)

    def test_fail_duplicates(self):
        fake_mads = pd.DataFrame(data = {"prøvenr": ["P99123456", "H99654321", "P99123456"]})
        sample_sheet = pd.DataFrame(data = {"KMA nr": ["99123456", "99654321"],
                                            "Barkode NB": ["NB01", "NB02"],
                                            "CT/CP værdi": [30.0, 30.0]})
        error_message = "Sample numbers are not unique after removing type prefix. " \
                        "Cannot identify the correct prefix for ambiguous sample numbers."
        with pytest.raises(ValueError, match = re.escape(error_message)):
            mk_sheet.infer_prefix(sample_sheet, fake_mads)

    def test_find_controls(self):
        # handle default positive and negative control
        fake_mads = pd.DataFrame(data = {"prøvenr": ["P99123456", "H99654321"]})
        sample_sheet = pd.DataFrame(data = {"KMA nr": ["99123456", "99654321", "NegK1", "34567890"],
                                            "Barkode NB": ["NB01", "NB02", "NB03", "NB04"],
                                            "CT/CP værdi": [30.0, 30.0, 100, 15.0]})
        sheet_with_prefixes = pd.DataFrame(data = {"KMA nr": ["P99123456", "H99654321",
                                                              "NegK1", "34567890"],
                                                   "Barkode NB": ["NB01", "NB02", "NB03", "NB04"],
                                                   "CT/CP værdi": [30.0, 30.0, 100, 15.0]})
        test_with_prefixes = mk_sheet.infer_prefix(sample_sheet, fake_mads)
        pd.testing.assert_frame_equal(sheet_with_prefixes, test_with_prefixes)

    def test_change_controls(self):
        fake_mads = pd.DataFrame(data = {"prøvenr": ["P99123456", "H99654321"]})
        sample_sheet = pd.DataFrame(data = {"KMA nr": ["99123456", "99654321", "Neg_K1",
                                                       "18000006"],
                                            "Barkode NB": ["NB01", "NB02", "NB03", "NB04"],
                                            "CT/CP værdi": [30.0, 30.0, 100, 15.0]})
        sheet_with_prefixes = pd.DataFrame(data = {"KMA nr": ["P99123456", "H99654321",
                                                              "Neg_K1", "18000006"],
                                                   "Barkode NB": ["NB01", "NB02", "NB03", "NB04"],
                                                   "CT/CP værdi": [30.0, 30.0, 100, 15.0]})
        test_with_prefixes = mk_sheet.infer_prefix(sample_sheet, fake_mads, negk_format=r"Neg_K\d",
                                                   positive_controls=["18000006", "18000007"])
        pd.testing.assert_frame_equal(sheet_with_prefixes, test_with_prefixes)

    def test_fail_missing_numbers(self):
        fake_mads = pd.DataFrame(data = {"prøvenr": ["P99123456", "H99654321"]})
        sample_sheet = pd.DataFrame(data = {"KMA nr": ["99123456", "99654322"],
                                            "Barkode NB": ["NB01", "NB02"],
                                            "CT/CP værdi": [30.0, 30.0]})
        error_message = "Prefix could not be inferred for the following samples: 99654322"
        with pytest.raises(ValueError, match = re.escape(error_message)):
            mk_sheet.infer_prefix(sample_sheet, fake_mads)
