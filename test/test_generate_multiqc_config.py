import pathlib
import unittest

import pandas as pd

import generate_multiqc_config

class TestGenerateMultiQC(unittest.TestCase):
    def test_get_barcodes_success(self):
        success_frame = pd.DataFrame({"barcode_name": ["barcode NB01", "barcode NB02", "barcode NB03", "barcode NB04"],
                                      "show_status": ["show", "show", "show", "show"],
                                      "pattern": ["_NB01", "_NB02", "_NB03", "_NB04"]})
        test_frame = generate_multiqc_config.make_barcode_config(pathlib.Path(__file__).parent / "data" / "multiqc_test" / "translate_sheet_ctcp.csv")
        pd.testing.assert_frame_equal(success_frame.sort_values(by="barcode_name").reset_index(drop=True),
                                      test_frame.sort_values(by="barcode_name").reset_index(drop=True))

class TestGenerateModuleOrder(unittest.TestCase):
    def test_get_modules_success(self):
        sample_data = pd.DataFrame({"KMA nr": ["7099000001", "7099000002"],
                                    "Barkode NB": ["NB01", "NB02"],
                                    "CT/CP værdi": [30, 20]})
        sample_name = "test"
        expected_result = {"top_modules": ["custom_data_lineplot", "custom_data_json_table"],
                             "report_section_order": {"test_7099000001_NB01": {"order": 60},
                                                      "test_7099000002_NB02": {"order": 50},
                                                      "mixed_positions": {"order": 40},
                                                      "all_mutations": {"order": 30},
                                                      "test_7099000001_NB01_mutations": {"order": 20},
                                                      "test_7099000002_NB02_mutations": {"order": 10}
                                                      }}
        test_result = generate_multiqc_config.get_module_order(sample_data, sample_name)
        assert expected_result == test_result

    def test_coerce_nr_to_string(self):
        sample_data = pd.DataFrame({"KMA nr": [7099000001, 7099000002],
                                    "Barkode NB": ["NB01", "NB02"],
                                    "CT/CP værdi": [30, 20]})
        sample_name = "test"
        expected_result = {"top_modules": ["custom_data_lineplot", "custom_data_json_table"],
                           "report_section_order": {"test_7099000001_NB01": {"order": 60},
                                                    "test_7099000002_NB02": {"order": 50},
                                                    "mixed_positions": {"order": 40},
                                                    "all_mutations": {"order": 30},
                                                    "test_7099000001_NB01_mutations": {"order": 20},
                                                    "test_7099000002_NB02_mutations": {"order": 10}
                                                    }}
        test_result = generate_multiqc_config.get_module_order(sample_data, sample_name)
        assert expected_result == test_result
