import re
import unittest

import pytest

import check_config

class TestCheckConfig(unittest.TestCase):
    def setUp(self) -> None:
        self.test_config = {"sample_number_settings": {"sample_numbers_in": "number", "sample_numbers_out": "letter"},
                           "primer_version": "4",
                           "conda_frontend": "conda",
                           "artic": {"workflow": "nanopolish"},
                           "pango_prediction": "usher"}

    def test_fail_sample_numbers(self):
        wrong_sample_numbers = self.test_config
        wrong_sample_numbers["sample_number_settings"]["sample_numbers_in"] = "int"
        wrong_sample_numbers["sample_number_settings"]["sample_numbers_out"] = "string"
        error_message = """The following issue(s) were detected with the config file:
Format of sample numbers used as input must be given as 'number' or 'letter'.
Desired format of sample numbers in output must be given as 'number' or 'letter'."""
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_sample_numbers)

    def test_fail_primers(self):
        wrong_primers = self.test_config
        wrong_primers["primer_version"] = "ARTIC v3"
        error_message = """The following issue(s) were detected with the config file:
Invalid primer scheme ARTIC v3. Primer scheme must be 3, 4, 4.1, 1200, or 1200.3."""
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_primers)

    def test_fail_conda_frontend(self):
        wrong_frontend = self.test_config
        wrong_frontend["conda_frontend"] = "rattlesnake"
        error_message = """The following issue(s) were detected with the config file:
Invalid conda frontend rattlesnake. Conda frontend must be conda or mamba."""
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_frontend)

    def test_fail_artic_workflow(self):
        wrong_workflow = self.test_config
        wrong_workflow["artic"]["workflow"] = "micropolish"
        error_message = """The following issue(s) were detected with the config file:
Invalid ARTIC workflow micropolish. Workflow options are nanopolish or medaka."""
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_workflow)

    def test_fail_pango_predict(self):
        wrong_prediction = self.test_config
        wrong_prediction["pango_prediction"] = "scorpio"
        error_message = "The following issue(s) were detected with the config file:\n" \
                        "Invalid Pangolin prediction setting scorpio. " \
                        "Prediction settings are pangolearn or usher."
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_prediction)