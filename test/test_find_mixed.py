import pathlib
import re
import unittest

import pandas as pd
import pytest

from Bio.Seq import Seq

import detect_mixed_positions


class TestDeduplicateVCF(unittest.TestCase):
    def test_no_filter(self):
        input_vcf = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"], "ALT": ["T", "T"],
                                         "QUAL": [500, 500],
                                         "depth": [200, 200], "ref_count": [1, 9],
                                         "alt_count": [150, 140]})
        test_result = detect_mixed_positions.deduplicate_vcf(input_vcf)
        pd.testing.assert_frame_equal(test_result, input_vcf)

    def test_no_data(self):
        input_vcf = pd.DataFrame(data = {"POS": [], "REF": [], "ALT": [],
                                         "QUAL": [],
                                         "depth": [], "ref_count": [],
                                         "alt_count": []})
        test_result = detect_mixed_positions.deduplicate_vcf(input_vcf)
        pd.testing.assert_frame_equal(test_result, input_vcf)

    def test_equal_score(self):
        input_vcf = pd.DataFrame(data = {"POS": [1, 1], "REF": ["C", "C"], "ALT": ["T", "T"],
                                         "QUAL": [500, 500],
                                         "depth": [200, 200], "ref_count": [1, 9],
                                         "alt_count": [150, 140]})
        test_result = detect_mixed_positions.deduplicate_vcf(input_vcf)
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"], "ALT": ["T"],
                                           "QUAL": [500],
                                           "depth": [200], "ref_count": [1],
                                           "alt_count": [150]})
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_find_multiple_duplicates(self):
        input_vcf = pd.DataFrame(data = {"POS": [1, 1, 3, 3], "REF": ["C", "C", "T", "T"],
                                         "ALT": ["T", "T", "C", "C"],
                                         "QUAL": [500, 500, 500, 500],
                                         "depth": [100, 100, 100, 100],
                                         "ref_count": [30, 30, 20, 20],
                                         "alt_count": [70, 70, 80, 80]})
        true_result = pd.DataFrame(data = {"POS": [1, 3], "REF": ["C", "T"],
                                           "ALT": ["T", "C"], "QUAL": [500, 500],
                                           "depth": [100, 100], "ref_count": [30, 20],
                                           "alt_count": [70, 80]})
        test_result = detect_mixed_positions.deduplicate_vcf(input_vcf)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_better_score(self):
        input_vcf = pd.DataFrame(data = {"POS": [1, 1], "REF": ["C", "C"], "ALT": ["T", "T"],
                                         "QUAL": [400, 500],
                                         "depth": [200, 200], "ref_count": [1, 9],
                                         "alt_count": [150, 140]})
        test_result = detect_mixed_positions.deduplicate_vcf(input_vcf)
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"], "ALT": ["T"],
                                           "QUAL": [500],
                                           "depth": [200], "ref_count": [9],
                                           "alt_count": [140]})
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_log_duplicates(self):
        input_vcf = pd.DataFrame(data = {"POS": [1, 1], "REF": ["C", "C"], "ALT": ["T", "T"],
                                         "QUAL": [400, 500],
                                         "depth": [200, 200], "ref_count": [1, 9],
                                         "alt_count": [150, 140]})
        with self.assertLogs("find_mixed") as logged:
            detect_mixed_positions.deduplicate_vcf(input_vcf)
            duplicate_count_msg = "INFO:find_mixed:Removing 1 duplicated variant calls."
            assert duplicate_count_msg in logged.output


class TestParseVCF(unittest.TestCase):
    def test_parse_positions(self):
        vcfs = pathlib.Path(__file__).parent / "data" / "find_mixed_reads" / "merged.vcf"
        true_result = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"], "ALT": ["T", "T"],
                                           "depth": [200, 200], "ref_count": [1, 9],
                                           "alt_count": [150, 140]})
        test_result = detect_mixed_positions.parse_vcf(vcfs)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_parse_blank(self):
        vcfs = pathlib.Path(__file__).parent / "data" / "find_mixed_reads" / "blank.vcf"
        true_result = pd.DataFrame(data = {"POS": [], "REF": [], "ALT": [],
                                           "depth": [], "ref_count": [],
                                           "alt_count": []})
        test_result = detect_mixed_positions.parse_vcf(vcfs)
        pd.testing.assert_frame_equal(test_result, true_result, check_index_type = False,
                                      check_dtype = False)

    def test_handle_duplicates(self):
        vcfs = pathlib.Path(__file__).parent / "data" / "find_mixed_reads" / "duplicate.vcf"
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"], "ALT": ["T"],
                                           "depth": [200], "ref_count": [9],
                                           "alt_count": [140]})
        test_result = detect_mixed_positions.parse_vcf(vcfs)
        pd.testing.assert_frame_equal(test_result, true_result)


class TestFindMixedPositions(unittest.TestCase):
    def test_find_mixed_defaults(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 20], "ref_count": [30, 18],
                                                   "alt_count": [70, 1]})
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                           "ALT": ["T"],
                                           "depth": [100], "ref_count": [30],
                                           "alt_count": [70], "proportion_ref": [0.3],
                                           "proportion_alt": [0.7]})
        test_result = detect_mixed_positions.find_mixed_positions(variants_with_count)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_filter_depth(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 10], "ref_count": [30, 9],
                                                   "alt_count": [70, 1]})
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                           "ALT": ["T"],
                                           "depth": [100], "ref_count": [30],
                                           "alt_count": [70], "proportion_ref": [0.3],
                                           "proportion_alt": [0.7]})
        test_result = detect_mixed_positions.find_mixed_positions(variants_with_count)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_no_hits(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 100], "ref_count": [20, 90],
                                                   "alt_count": [80, 5]})
        true_result = pd.DataFrame(data = {"POS": [], "REF": [],
                                           "ALT": [],
                                           "depth": [], "ref_count": [],
                                           "alt_count": [], "proportion_ref": [],
                                           "proportion_alt": []})
        test_result = detect_mixed_positions.find_mixed_positions(variants_with_count)
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_change_depth(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 20], "ref_count": [30, 18],
                                                   "alt_count": [70, 1]})
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                           "ALT": ["T"],
                                           "depth": [100], "ref_count": [30],
                                           "alt_count": [70], "proportion_ref": [0.3],
                                           "proportion_alt": [0.7]})
        test_result = detect_mixed_positions.find_mixed_positions(variants_with_count,
                                                                  min_depth = 50)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_fail_wrong_depth(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 20], "ref_count": [30, 18],
                                                   "alt_count": [70, 1]})
        error_msg = "Minimum required depth cannot be lower than 1."
        with pytest.raises(ValueError, match = error_msg):
            detect_mixed_positions.find_mixed_positions(variants_with_count, min_depth = 0)

    def test_change_cutoffs(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 100], "ref_count": [20, 90],
                                                   "alt_count": [80, 5]})
        # we should find the second variant if we decrease the minimum proportion...
        true_result_min = pd.DataFrame(data = {"POS": [2], "REF": ["C"],
                                               "ALT": ["T"],
                                               "depth": [100], "ref_count": [90],
                                               "alt_count": [5], "proportion_ref": [0.9],
                                               "proportion_alt": [0.05]})
        test_result_min = detect_mixed_positions.find_mixed_positions(variants_with_count,
                                                                      min_alt_proportion = 0.05)
        pd.testing.assert_frame_equal(test_result_min, true_result_min, check_index_type = False)
        # ...and the first if we increase the maximum proportion
        true_result_max = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                               "ALT": ["T"],
                                               "depth": [100], "ref_count": [20],
                                               "alt_count": [80], "proportion_ref": [0.2],
                                               "proportion_alt": [0.8]})
        test_result_max = detect_mixed_positions.find_mixed_positions(variants_with_count,
                                                                      max_alt_proportion = 0.8)
        pd.testing.assert_frame_equal(test_result_max, true_result_max, check_index_type = False)

    def test_fail_cutoffs(self):
        variants_with_count = pd.DataFrame(data = {"POS": [1, 2], "REF": ["C", "C"],
                                                   "ALT": ["T", "T"],
                                                   "depth": [100, 100], "ref_count": [20, 90],
                                                   "alt_count": [80, 5]})
        too_low_min = "Minimum required proportion of alternate reads for mixed positions must" \
                      " be positive."
        with pytest.raises(ValueError, match = too_low_min):
            detect_mixed_positions.find_mixed_positions(variants_with_count,
                                                        min_alt_proportion = -0.05)
        too_low_max = "Maximum required proportion of alternate reads for mixed positions " \
                      "must be positive."
        with pytest.raises(ValueError, match = too_low_max):
            detect_mixed_positions.find_mixed_positions(variants_with_count,
                                                        max_alt_proportion = -0.05)
        wrong_order = "Minimum required proportion of alternate reads for mixed positions " \
                      "cannot be higher than maximum proportion."
        with pytest.raises(ValueError, match = wrong_order):
            detect_mixed_positions.find_mixed_positions(variants_with_count,
                                                        min_alt_proportion = 0.8,
                                                        max_alt_proportion = 0.05)


class TestFindHomopolymers(unittest.TestCase):
    def test_find_ref_homopolymer(self):
        ref_sequence = Seq("CAAAAAT")
        ref_base = "A"
        mut_base = "C"
        position = 6
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_find_mut_homopolymer(self):
        ref_sequence = Seq("CAAAATT")
        ref_base = "T"
        mut_base = "A"
        position = 6
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_no_match(self):
        ref_sequence = Seq("CATCATCAT")
        ref_base = "T"
        mut_base = "A"
        position = 6
        assert not detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                                ref_sequence)

    def test_keep_to_search_area(self):
        ref_sequence = Seq("CATCATCAAAAAT")
        ref_base = "T"
        mut_base = "A"
        position = 6
        assert not detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                                ref_sequence)

    def test_keep_to_seq_length(self):
        ref_sequence = Seq("CAAAAAT")
        ref_base = "A"
        mut_base = "C"
        position = 3
        # if we don't catch this, the position will first be corrected to 2 (0-based notation);
        # we will then subtract 5 and start at -3, in the middle of the string
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_find_multi_bases(self):
        ref_sequence = Seq("CATCAAT")
        ref_base = "TC"
        mut_base = "AA"
        position = 3
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_set_cutoff(self):
        ref_sequence = Seq("CATCAAATT")
        ref_base = "T"
        mut_base = "A"
        position = 8
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence, repeat_cutoff=4)
        assert not detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                                ref_sequence)

    def test_handle_deletion(self):
        ref_sequence = Seq("CATCAAAT")
        ref_base = "TC"
        mut_base = "A"
        position = 3
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_longer_deletion(self):
        ref_sequence = Seq("CATCAAAAT")
        ref_base = "ATC"
        mut_base = "A"
        position = 2
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_handle_insertion(self):
        ref_sequence = Seq("CATCAAATT")
        ref_base = "T"
        mut_base = "AA"
        position = 8
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_longer_insertion(self):
        ref_sequence = Seq("CATCAAATT")
        ref_base = "T"
        mut_base = "AAA"
        position = 8
        assert detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                            ref_sequence)

    def test_fail_wrong_base(self):
        ref_sequence = Seq("CATCAT")
        ref_base = "A"
        mut_base = "C"
        position = 4
        error_msg = f"Mismatch between reference base reported and base found at " \
                    f"position {position}"
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            detect_mixed_positions.in_homopolymer_region(ref_base, mut_base, position,
                                                         ref_sequence)


class TestFilterMixed(unittest.TestCase):
    def test_no_homopolymer(self):
        ref_sequence = Seq("CATCATCAT")
        input_mutations = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                               "ALT": ["T"],
                                               "depth": [100], "ref_count": [30],
                                               "alt_count": [70], "proportion_ref": [0.3],
                                               "proportion_alt": [0.7]})
        test_result = detect_mixed_positions.filter_homopolymers(input_mutations, ref_sequence)
        pd.testing.assert_frame_equal(input_mutations, test_result)

    def test_no_data(self):
        ref_sequence = Seq("CATCATCAT")
        input_mutations = pd.DataFrame(data = {"POS": [], "REF": [],
                                               "ALT": [],
                                               "depth": [], "ref_count": [],
                                               "alt_count": [], "proportion_ref": [],
                                               "proportion_alt": []})
        test_result = detect_mixed_positions.filter_homopolymers(input_mutations, ref_sequence)
        pd.testing.assert_frame_equal(input_mutations, test_result)

    def test_filter_homopolymer(self):
        ref_sequence = Seq("CAAAATT")
        input_mutations = pd.DataFrame(data = {"POS": [1, 6], "REF": ["C", "T"],
                                               "ALT": ["T", "A"],
                                               "depth": [100, 100], "ref_count": [30, 30],
                                               "alt_count": [70, 70], "proportion_ref": [0.3, 0.3],
                                               "proportion_alt": [0.7, 0.7]})
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                           "ALT": ["T"],
                                           "depth": [100], "ref_count": [30],
                                           "alt_count": [70], "proportion_ref": [0.3],
                                           "proportion_alt": [0.7]})
        test_result = detect_mixed_positions.filter_homopolymers(input_mutations, ref_sequence)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_multiple_homopolymers(self):
        ref_sequence = Seq("CAAAATTCAAAAAT")
        input_mutations = pd.DataFrame(data = {"POS": [1, 6, 8, 13], "REF": ["C", "T", "C", "A"],
                                               "ALT": ["T", "A", "CC", "T"],
                                               "depth": [100, 100, 100, 100],
                                               "ref_count": [30, 30, 40, 35],
                                               "alt_count": [70, 70, 60, 65],
                                               "proportion_ref": [0.3, 0.3, 0.4, 0.35],
                                               "proportion_alt": [0.7, 0.7, 0.6, 0.65]})
        true_result = pd.DataFrame(data = {"POS": [1, 8], "REF": ["C", "C"],
                                           "ALT": ["T", "CC"],
                                           "depth": [100, 100], "ref_count": [30, 40],
                                           "alt_count": [70, 60], "proportion_ref": [0.3, 0.4],
                                           "proportion_alt": [0.7, 0.6]})
        test_result = detect_mixed_positions.filter_homopolymers(input_mutations, ref_sequence)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_log_homopolymers(self):
        ref_sequence = Seq("CAAAATT")
        input_mutations = pd.DataFrame(data = {"POS": [1, 6], "REF": ["C", "T"],
                                               "ALT": ["T", "A"],
                                               "depth": [100, 100], "ref_count": [30, 30],
                                               "alt_count": [70, 70], "proportion_ref": [0.3, 0.3],
                                               "proportion_alt": [0.7, 0.7]})
        with self.assertLogs("find_mixed") as logged:
            detect_mixed_positions.filter_homopolymers(input_mutations, ref_sequence)
            info_msg = "INFO:find_mixed:Removing 1 variants likely caused by homopolymer errors."
            assert info_msg in logged.output

    def test_filter_other_cutoff(self):
        ref_sequence = Seq("CATCAAATT")
        input_mutations = pd.DataFrame(data = {"POS": [1, 8], "REF": ["C", "T"],
                                               "ALT": ["T", "A"],
                                               "depth": [100, 100], "ref_count": [30, 30],
                                               "alt_count": [70, 70], "proportion_ref": [0.3, 0.3],
                                               "proportion_alt": [0.7, 0.7]})
        true_result = pd.DataFrame(data = {"POS": [1], "REF": ["C"],
                                           "ALT": ["T"],
                                           "depth": [100], "ref_count": [30],
                                           "alt_count": [70], "proportion_ref": [0.3],
                                           "proportion_alt": [0.7]})
        test_result = detect_mixed_positions.filter_homopolymers(input_mutations, ref_sequence,
                                                                 homopolymer_cutoff = 4)
        pd.testing.assert_frame_equal(true_result, test_result)
