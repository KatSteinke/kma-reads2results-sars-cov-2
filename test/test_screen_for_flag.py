import unittest

import pandas as pd
import pytest

import screen_for_flag

class TestScreenDataframe(unittest.TestCase):
    data_to_screen = pd.DataFrame(data={"taxon": ["XYZ", "ABC"], "lineage": ["XY.1", "XY.2"]})
    def test_wrong_column(self):
        with pytest.raises(KeyError, match="Column sequence not found in report."):
            screen_for_flag.screen_for_flag(self.data_to_screen, "sequence", "XYZ")
    def test_no_hits(self):
        no_hits = screen_for_flag.screen_for_flag(self.data_to_screen, "taxon", "DEF")
        assert no_hits.empty
    def test_filter_success(self):
        success_frame = pd.DataFrame(data={"taxon": ["ABC"], "lineage": ["XY.2"]})
        test_frame = screen_for_flag.screen_for_flag(self.data_to_screen, "taxon", "ABC")
        pd.testing.assert_frame_equal(success_frame.reset_index(drop=True), test_frame.reset_index(drop=True),
                                      check_like=True)