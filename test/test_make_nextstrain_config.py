import pathlib
import unittest

from datetime import date, datetime

import pytest

import make_nextstrain_config

class TestBuildFile(unittest.TestCase):
    # TODO: set up correct data
    data_path = pathlib.Path(__file__).parent / "data" / "nextstrain_config_test"
    meta_path = data_path / "correct_metadata_file.tsv"
    seq_path = data_path / "correct_seq.fasta"
    build_name = "testbuild"
    cutoff = date(year=2021, month=1, day=1)

    def test_missing_metadata(self):
        wrong_metadata = self.data_path / "missing_metadata_file"
        with pytest.raises(FileNotFoundError, match="Metadata file does not exist."):
            make_nextstrain_config.get_build_string(self.build_name, wrong_metadata, self.seq_path, self.cutoff)

    def test_missing_seqs(self):
        wrong_seqs = self.data_path / "missing_seq_file"
        with pytest.raises(FileNotFoundError, match="Sequence file does not exist."):
            make_nextstrain_config.get_build_string(self.build_name, self.meta_path, wrong_seqs, self.cutoff)

    def test_future_date(self):
        future_sample = date(year=3000, month=1, day=1)
        with pytest.raises(ValueError, match="Earliest sampling date is in the future."):
            make_nextstrain_config.get_build_string(self.build_name, self.meta_path, self.seq_path, future_sample)

    def test_wrong_region(self):
        with pytest.raises(ValueError, match="Atlantis is not a valid region."):
            make_nextstrain_config.get_build_string(self.build_name, self.meta_path, self.seq_path, self.cutoff,
                                                    region="Atlantis")

    def test_make_plain_build(self):
        true_build = f"""inputs:
  - name: ouh-covid19
    metadata: {str(self.meta_path)}
    sequences: {str(self.seq_path)}
    


builds:
  testbuild:
      subsampling_scheme: division
      region: Europe
      country: Denmark
      division: Syddanmark


subsampling:
  testbuild:
    division:
      group_by: "year month"
      max_sequences: 4500
      min_date: "2021-01-01"
      exclude: "--exclude-where 'division!={{division}}'"
    # context samples from rest of the country
    country:
      group_by: "division year month"
      seq_per_group: 20
      min_date: "2021-01-01"
      exclude: "--exclude-where 'country!={{country}}'"
    region:
      group_by: "country division year month"
      seq_per_group: 20
      min_date: "2021-01-01"
      exclude: "--exclude-where 'region!={{region}}'"
      """
        test_build = make_nextstrain_config.get_build_string(self.build_name, self.meta_path, self.seq_path,
                                                             self.cutoff)
        assert true_build == test_build

    def test_custom_location(self):
        true_build = f"""inputs:
  - name: ouh-covid19
    metadata: {str(self.meta_path)}
    sequences: {str(self.seq_path)}
    


builds:
  testbuild:
      subsampling_scheme: division
      region: Asia
      country: China
      division: Hubei


subsampling:
  testbuild:
    division:
      group_by: "year month"
      max_sequences: 4500
      min_date: "2021-01-01"
      exclude: "--exclude-where 'division!={{division}}'"
    # context samples from rest of the country
    country:
      group_by: "division year month"
      seq_per_group: 20
      min_date: "2021-01-01"
      exclude: "--exclude-where 'country!={{country}}'"
    region:
      group_by: "country division year month"
      seq_per_group: 20
      min_date: "2021-01-01"
      exclude: "--exclude-where 'region!={{region}}'"
      """
        test_build = make_nextstrain_config.get_build_string(self.build_name, self.meta_path, self.seq_path,
                                                             self.cutoff, region="Asia", country="China",
                                                             division="Hubei")
        assert true_build == test_build

class TestMakeConfig(unittest.TestCase):
    data_path = pathlib.Path(__file__).parent / "data" / "nextstrain_config_test"
    default_path = data_path / "default_config_test.yaml"
    build_path = data_path / "test_build.yaml"

    def test_no_defaults(self):
        no_default = self.data_path / "no_default"
        with pytest.raises(FileNotFoundError, match="Default parameter file does not exist."):
            make_nextstrain_config.get_config_string(no_default, self.build_path)

    def test_no_build(self):
        no_build = self.data_path / "no_build"
        with pytest.raises(FileNotFoundError, match="Build file does not exist."):
            make_nextstrain_config.get_config_string(self.default_path, no_build)

    def test_make_config(self):
        true_config = f"""configfile:
      - {str(self.default_path)}/defaults/parameters.yaml # Pull in the default values
      - {str(self.build_path)} # Pull in our list of desired builds

# Set the maximum number of cores you want Snakemake to use for this pipeline.
cores: 2

# Always print the commands that will be run to the screen for debugging.
printshellcmds: True

# Print log files of failed jobs
show-failed-logs: True
    """
        test_config = make_nextstrain_config.get_config_string(self.default_path, self.build_path)
        assert true_config == test_config