import pathlib
import unittest

from datetime import date

import pandas as pd

import filter_by_cleaned

class TestFilterByCleaned(unittest.TestCase):
    # results and MADS data should stay the same
    def setUp(self) -> None:
        merged_results = pathlib.Path(__file__).parent / "data" / "filter_by_cleaned" / "cleaned_merged.csv"
        self.merged_data = pd.read_csv(merged_results, sep=";", decimal=",", index_col=False,
                                       dtype={"modtaget": str, "analyseret": str,
                                              "sample_number": str})
        # workaround for analysis date (todo: more elegant way?)
        self.merged_data["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
        self.merged_data = self.merged_data.sort_values(by="taxon", ignore_index=True)
        fake_mads = pathlib.Path(__file__).parent / "data" / "append_to_all" / "fake_mads_data.csv"
        self.mads_data = pd.read_csv(fake_mads, encoding="latin1", dtype={"afsendt": str,
                                                                          "cprnr.": str,
                                                                          "modtaget": str})
    # filter duplicated taxon in raw reanalysis data
    def test_filter_duplicated_taxon(self):
        new_results = pathlib.Path(__file__).parent / "data" / "filter_by_cleaned" / "same_results_existing_duplicate.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        with self.assertLogs("filter_results") as logged:
            test_data = filter_by_cleaned.filter_by_cleaned(new_data, self.merged_data)
            assert "INFO:filter_results:Dropped 1 duplicate sequences." in logged.output
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)


    # filter research samples, controls,... on merge
    def test_filter_samples(self):
        new_results = pathlib.Path(__file__).parent / "data" / "filter_by_cleaned" / "same_results_research.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        with self.assertLogs("filter_results") as logged:
            test_data = filter_by_cleaned.filter_by_cleaned(new_data, self.merged_data)
            research_dropped = "INFO:filter_results:Dropped 1 samples: \n" \
                               "test_data_4099000000_NB12"
            assert research_dropped in logged.output
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)

    def test_filter_duplicated_drops_na(self):
        new_results = pathlib.Path(
            __file__).parent / "data" / "filter_by_cleaned" / "same_results_duplicate_with_na.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        test_data = filter_by_cleaned.filter_by_cleaned(new_data, self.merged_data)
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)