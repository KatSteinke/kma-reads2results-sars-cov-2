import pathlib
import unittest

import pandas as pd

import parse_ouh_metadata


class TestParseOUH(unittest.TestCase):
    success_frame = pd.DataFrame({"strain": ["OUH-P21000001", "OUH-P21000002"],
                                  "date": ["2021-01-01", "2021-01-02"],
                                  "pango_lineage": ["B.1.177.24", "B.1.177.21"]})
    success_frame["virus"] = "ncov"
    success_frame["gisaid_epi_isl"] = ""
    success_frame["genbank_accession"] = ""
    success_frame["region"] = "Europe"
    success_frame["country"] = "Denmark"
    success_frame["division"] = "Syddanmark"
    success_frame["location"] = "Syddanmark"
    success_frame["region_exposure"] = "Europe"  # TODO: should be able to be changed
    success_frame["country_exposure"] = "Denmark"
    success_frame["division_exposure"] = "Denmark"
    success_frame["segment"] = "genome"
    success_frame["length"] = ""  # TODO: we can get genome length from results if we need
    success_frame["host"] = "Human"
    success_frame["age"] = ""  # TODO: should be able to be changed
    success_frame["sex"] = ""  # TODO: should be able to be changed
    success_frame["originating_lab"] = ""
    success_frame["submitting_lab"] = ""
    success_frame["authors"] = ""
    success_frame["url"] = ""
    success_frame["title"] = ""
    success_frame["date_submitted"] = ""
    success_frame = success_frame[["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                                   "region", "country", "division", "location", "region_exposure",
                                   "country_exposure", "division_exposure", "segment", "length",
                                   "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                                   "url", "title", "date_submitted", "pango_lineage"]]
    def test_parse_number_to_letter(self):
        prefix_mapping = {"70": "P", "40": "H"}
        input_metadata = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" / "test_success_ouh_metadata.tsv"
        input_resultfile = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" / "test_own_results.csv"
        test_nextstrain_frame = parse_ouh_metadata.parse_own_metadata(input_metadata, input_resultfile, prefix_mapping)
        pd.testing.assert_frame_equal(self.success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_nextstrain_frame.sort_values(by="strain").reset_index(drop=True))

    def test_parse_letter_to_number(self):
        prefix_mapping = {"P": "70", "H": "40"}
        input_metadata = pathlib.Path(
            __file__).parent / "data" / "nextstrain_metadata_test" / "test_success_ouh_metadata_numeric.tsv"
        input_resultfile = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" \
                           / "test_own_results_converted.csv"
        test_nextstrain_frame = parse_ouh_metadata.parse_own_metadata(input_metadata, input_resultfile, prefix_mapping)
        success_frame = self.success_frame.copy()
        success_frame["strain"] = ["OUH-7021000001", "OUH-7021000002"]
        pd.testing.assert_frame_equal(success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_nextstrain_frame.sort_values(by="strain").reset_index(drop=True))

