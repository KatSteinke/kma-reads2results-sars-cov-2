import pathlib
import unittest

import pandas as pd

import merge_pango_nextstrain

class TestMergeSuccess(unittest.TestCase):
    data_dir = pathlib.Path(__file__).parent / "data" / "pango_nextclade_test"
    def test_merge_plain(self):
        success_file_path = self.data_dir / "success_pango_nextclade_merge.csv"
        true_result = pd.read_csv(success_file_path, sep=";",  dtype={"totalDeletions": pd.Int64Dtype(),
                                                                     "totalSubstitutions": pd.Int64Dtype(), # is "totalMutations" in the old Nextclade
                                                                           "totalInsertions": pd.Int64Dtype(),
                                                                           "totalMissing": pd.Int64Dtype(),
                                                                           "totalFrameShifts": pd.Int64Dtype(),
                                                                           "totalNonACGTNs": pd.Int64Dtype(),
                                                                           "totalPcrPrimerChanges": pd.Int64Dtype(),
                                                                           "totalAminoacidSubstitutions": pd.Int64Dtype(),
                                                                           "totalAminoacidDeletions": pd.Int64Dtype(),
                                                                           "alignmentEnd": pd.Float64Dtype(),
                                                                           "alignmentScore": pd.Float64Dtype(),
                                                                           "alignmentStart": pd.Float64Dtype(),
                                                                     "qc.missingData.missingDataThreshold": pd.Float64Dtype(),
                                                                     "qc.missingData.totalMissing": pd.Int64Dtype(),
                                                                      "qc.missingData.score": pd.Float64Dtype(),
                                                                     "qc.mixedSites.mixedSitesThreshold": pd.Float64Dtype(),
                                                                     "qc.mixedSites.totalMixedSites": pd.Int64Dtype(),
                                                                      "qc.mixedSites.score": pd.Float64Dtype(),
                                                                     "qc.privateMutations.total": pd.Int64Dtype(),
                                                                      "qc.privateMutations.score": pd.Float64Dtype(),
                                                                     "qc.snpClusters.totalSNPs": pd.Int64Dtype(),
                                                                      "qc.frameShifts.score": pd.Float64Dtype(),
                                                                     "qc.frameShifts.totalFrameShifts": pd.Int64Dtype(),
                                                                     "qc.frameShifts.totalFrameShiftsIgnored": pd.Int64Dtype(),
                                                                     "qc.stopCodons.totalStopCodons": pd.Int64Dtype(),
                                                                      "qc.stopCodons.score": pd.Float64Dtype(),
                                                                      "qc.snpClusters.score": pd.Float64Dtype(),
                                                                      "qc.overallScore": pd.Float64Dtype()
                                                                     })
        pango_path = self.data_dir / "pangolin_success_report.csv"
        nextclade_path = self.data_dir / "nextclade_failfirst_report.csv"
        test_result = merge_pango_nextstrain.merge_pango_and_nextclade(pango_path, nextclade_path)
        pd.testing.assert_frame_equal(true_result.sort_values(by="taxon").reset_index(drop=True),
                                      test_result.sort_values(by="taxon").reset_index(drop=True))
    def test_merge_with_fail(self):
        success_file_path = self.data_dir / "success_pango_nextclade_merge_failfasta.csv"
        true_result = pd.read_csv(success_file_path, sep=";",   dtype={"totalDeletions": pd.Int64Dtype(),
                                                                     "totalSubstitutions": pd.Int64Dtype(), # is "totalMutations" in the old Nextclade
                                                                           "totalInsertions": pd.Int64Dtype(),
                                                                           "totalMissing": pd.Int64Dtype(),
                                                                           "totalFrameShifts": pd.Int64Dtype(),
                                                                           "totalNonACGTNs": pd.Int64Dtype(),
                                                                           "totalPcrPrimerChanges": pd.Int64Dtype(),
                                                                           "totalAminoacidSubstitutions": pd.Int64Dtype(),
                                                                           "totalAminoacidDeletions": pd.Int64Dtype(),
                                                                           "alignmentEnd": pd.Float64Dtype(),
                                                                           "alignmentScore": pd.Float64Dtype(),
                                                                           "alignmentStart": pd.Float64Dtype(),
                                                                     "qc.missingData.missingDataThreshold": pd.Float64Dtype(),
                                                                     "qc.missingData.totalMissing": pd.Int64Dtype(),
                                                                       "qc.missingData.score": pd.Float64Dtype(),
                                                                     "qc.mixedSites.mixedSitesThreshold": pd.Float64Dtype(),
                                                                     "qc.mixedSites.totalMixedSites": pd.Int64Dtype(),
                                                                       "qc.mixedSites.score": pd.Float64Dtype(),
                                                                     "qc.privateMutations.total": pd.Int64Dtype(),
                                                                       "qc.privateMutations.score": pd.Float64Dtype(),
                                                                       "qc.snpClusters.totalSNPs": pd.Int64Dtype(),
                                                                       "qc.frameShifts.score": pd.Float64Dtype(),
                                                                     "qc.frameShifts.totalFrameShifts": pd.Int64Dtype(),
                                                                     "qc.frameShifts.totalFrameShiftsIgnored": pd.Int64Dtype(),
                                                                     "qc.stopCodons.totalStopCodons": pd.Int64Dtype(),
                                                                       "qc.stopCodons.score": pd.Float64Dtype(),
                                                                       "qc.snpClusters.score": pd.Float64Dtype(),
                                                                      "qc.overallScore": pd.Float64Dtype()
                                                                     })
        pango_path = self.data_dir / "pangolin_failfirst_report.csv"
        nextclade_path = self.data_dir / "nextclade_failfirst_report.csv"
        test_result = merge_pango_nextstrain.merge_pango_and_nextclade(pango_path, nextclade_path)
        pd.testing.assert_frame_equal(true_result.sort_values(by="taxon").reset_index(drop=True),
                                      test_result.sort_values(by="taxon").reset_index(drop=True))
    def test_merge_nonstandard_filename(self):
        success_file_path = self.data_dir / "success_pango_nextclade_merge_otherpath.csv"
        true_result = pd.read_csv(success_file_path, sep = ";", dtype = {"totalDeletions": pd.Int64Dtype(),
                                                                         "totalSubstitutions": pd.Int64Dtype(),
                                                                         # is "totalMutations" in the old Nextclade
                                                                         "totalInsertions": pd.Int64Dtype(),
                                                                         "totalMissing": pd.Int64Dtype(),
                                                                         "totalFrameShifts": pd.Int64Dtype(),
                                                                         "totalNonACGTNs": pd.Int64Dtype(),
                                                                         "totalPcrPrimerChanges": pd.Int64Dtype(),
                                                                         "totalAminoacidSubstitutions": pd.Int64Dtype(),
                                                                         "totalAminoacidDeletions": pd.Int64Dtype(),
                                                                         "alignmentEnd": pd.Float64Dtype(),
                                                                         "alignmentScore": pd.Float64Dtype(),
                                                                         "alignmentStart": pd.Float64Dtype(),
                                                                         "qc.missingData.missingDataThreshold": pd.Float64Dtype(),
                                                                         "qc.missingData.totalMissing": pd.Int64Dtype(),
                                                                         "qc.missingData.score": pd.Float64Dtype(),
                                                                         "qc.mixedSites.mixedSitesThreshold": pd.Float64Dtype(),
                                                                         "qc.mixedSites.totalMixedSites": pd.Int64Dtype(),
                                                                         "qc.mixedSites.score": pd.Float64Dtype(),
                                                                         "qc.privateMutations.total": pd.Int64Dtype(),
                                                                         "qc.privateMutations.score": pd.Float64Dtype(),
                                                                         "qc.snpClusters.totalSNPs": pd.Int64Dtype(),
                                                                         "qc.frameShifts.score": pd.Float64Dtype(),
                                                                         "qc.frameShifts.totalFrameShifts": pd.Int64Dtype(),
                                                                         "qc.frameShifts.totalFrameShiftsIgnored": pd.Int64Dtype(),
                                                                         "qc.stopCodons.totalStopCodons": pd.Int64Dtype(),
                                                                         "qc.stopCodons.score": pd.Float64Dtype(),
                                                                         "qc.snpClusters.score": pd.Float64Dtype(),
                                                                         "qc.overallScore": pd.Float64Dtype()
                                                                         })
        pango_path = self.data_dir / "pangolin_otherpath_report.csv"
        nextclade_path = self.data_dir / "nextclade_otherpath_report.csv"
        test_result = merge_pango_nextstrain.merge_pango_and_nextclade(pango_path, nextclade_path)
        pd.testing.assert_frame_equal(true_result.sort_values(by = "taxon").reset_index(drop = True),
                                      test_result.sort_values(by = "taxon").reset_index(drop = True))