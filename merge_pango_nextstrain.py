"""Merge pango and nextclade output."""

__author__ = "Kat Steinke"

import pathlib
import re

from argparse import ArgumentParser

import pandas as pd
import yaml

import check_config
import pipeline_config
import version
__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF


def merge_pango_and_nextclade(pango_output: pathlib.Path, nextclade_output: pathlib.Path)\
        -> pd.DataFrame:
    """Merge Pango and Nextclade ouptut files by sample name.
    Arguments:
        pango_output:      Path to Pango output file
        nextclade_output:  Path to Nextclade output file
    Returns:
        A file combining Pango and Nextclade output, merged on the sequence name
    """
    # TODO: sanity checks - double check with sample sheet if needed?
    lineage_report = pd.read_csv(pango_output, sep=",")
    # notes contain semicolons, but these are used as separators in our final output,
    # so we clean these up
    lineage_report["note"] = lineage_report["note"].apply(lambda x: x.replace(";", " ")
                                                                    if pd.notna(x)
                                                                    else x)
    lineage_report["scorpio_notes"] = lineage_report["scorpio_notes"].apply(lambda x: x.replace(";",
                                                                                                " ")
                                                                                      if pd.notna(x)
                                                                                      else x)
    # keep versions as strings
    pangolin_versions = lineage_report.columns[lineage_report.columns.str.contains("version")].tolist()
    lineage_report[pangolin_versions] = lineage_report[pangolin_versions].astype(str)

    nextclade_report = pd.read_csv(nextclade_output, sep=";")
    # convert anything that contains "score", "alignment" or "Threshold" to float
    nextclade_float_field_names = ["[sS]core", "alignment", "Threshold"]
    nextclade_float_fields = \
        (nextclade_report.columns[nextclade_report.columns.str.contains("|".join(nextclade_float_field_names))]).tolist()
    nextclade_report[nextclade_float_fields] = nextclade_report[nextclade_float_fields].astype(pd.Float64Dtype())
    # anything with "total" is likely an int, so coerce it to this
    nextclade_int_fields = (nextclade_report.columns[nextclade_report.columns.str.contains("total")]).tolist()
    nextclade_report[nextclade_int_fields] = nextclade_report[nextclade_int_fields].astype(pd.Int64Dtype())
    # names need to be identical so we can join on them
    # nextclade keeps spaces in the consensus fasta's header, pangolin replaces with underscore
    # underscore naturally occurs in filename a lot, space doesn't, so we drop the space
    nextclade_report["seqName"] = nextclade_report["seqName"].str.replace(" ", "_")
    # however, the path might contain spaces as well, so we clean that too
    lineage_report["taxon"] = lineage_report["taxon"].str.replace(" ", "_")
    # we want to restore (something like) the old behavior, so we extract rundir/sample/barcode;
    # the pattern is /some/long/path/to/rundir_sample_barcode/ARTIC/medaka_refseq
    # get pattern for sample numbers here
    positive_control_pattern = f'({"|".join(workflow_config["sample_number_settings"]["positive_control"])})'
    sample_id_pattern = f'(' \
                        f'({workflow_config["sample_number_settings"]["sample_number_format"]}' \
                        f'|{workflow_config["sample_number_settings"]["negative_control"]}' \
                        f'|{positive_control_pattern})' \
                        f'_{workflow_config["barcode_format"]})'
    sample_in_filename = r"(?P<sequence_name>[-a-zA-Z0-9_]+" \
                         + f"{sample_id_pattern}(_FAILED)?)/ARTIC/(medaka|FAILED)"

    nextclade_report["seqName"] = nextclade_report["seqName"].apply(lambda x: re.search(sample_in_filename,
                                                                                        x).group("sequence_name")
                                                                    if re.search(sample_in_filename,
                                                                                 x)
                                                                    else x)
    lineage_report["taxon"] = lineage_report["taxon"].apply(lambda x: re.search(sample_in_filename,
                                                                                x).group("sequence_name")
                                                                    if re.search(sample_in_filename,
                                                                                 x)
                                                                    else x)
    # we now use pandas to join on names instead; pangolin keeps header-only fastas
    # (from samples where artic failed) while nextclade doesn't
    # -> left join to preserve everything that's in the pangolin output
    full_result = pd.merge(lineage_report, nextclade_report, how="left", left_on="taxon",
                           right_on="seqName")
    # fill in missing seqnames since R script checks for seqnames
    full_result["seqName"] = full_result["seqName"].fillna(full_result["taxon"])
    return full_result

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Merge Pango and Nextclade output.")
    arg_parser.add_argument("pango_file", help="Path to file containing Pango result")
    arg_parser.add_argument("nextclade_file", help="Path to file containing Nextclade results")
    arg_parser.add_argument("outfile", help="Path to output file")
    arg_parser.add_argument("--workflow_config_file",
                            help="Config file for run (overrides default config given in script,"
                                 " can be overridden by commandline options)")
    args = arg_parser.parse_args()

    pango_file = pathlib.Path(args.pango_file)
    nextclade_file = pathlib.Path(args.nextclade_file)
    outfile = pathlib.Path(args.outfile)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file)
        if not default_config_file.exists():
            raise FileNotFoundError("Config file not found.")
        with open(default_config_file, "r", encoding="utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        check_config.check_config(workflow_config)


    combined_result = merge_pango_and_nextclade(pango_file, nextclade_file)
    combined_result.to_csv(path_or_buf=outfile, sep=";", decimal=",", index=False)
