"""Extract "mixed" positions that may indicate mixed samples (contamination or coinfection) from
 unfiltered VCF results."""

__author__ = "Kat Steinke"

import logging
import pathlib

from argparse import ArgumentParser

import pandas as pd

from Bio import Seq, SeqIO

import version

__version__ = version.__version__

# start logging
logger = logging.getLogger("find_mixed")
logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
# we want to be chatty even if not saving to file
console_log.setLevel(logging.INFO)
logger.addHandler(console_log)


def deduplicate_vcf(vcf_data: pd.DataFrame) -> pd.DataFrame:
    """
    Remove duplicated lines introduced by a mutation being found in both primer pools, retaining
    the higher-scoring line where possible.
    Args:
        vcf_data:   data from the .merged.vcf file produced by the ARTIC pipeline

    Returns:
        Variant calling data without duplicates caused by overlap between primer pools

    """
    # if we don't have any variants we don't need to filter anything
    if vcf_data.empty:
        return vcf_data
    # sort by position (low to high), keep the best-scoring
    variants_by_score = vcf_data.sort_values(by = ["POS", "QUAL"], ascending = [True, False])
    # record count of duplicates - True is 1 so we can simply sum it up
    # scores and details may vary - we're looking for the same mutations in the same positions
    duplicates = variants_by_score.duplicated(subset = ["POS", "ALT", "REF"],
                                              keep = "first")
    duplicate_count = sum(duplicates.tolist())
    if duplicate_count:
        logger.info(f"Removing {duplicate_count} duplicated variant calls.")
    variants_by_score = variants_by_score[~duplicates]
    variants_by_score = variants_by_score.reset_index(drop=True)
    return variants_by_score


# parse merged VCF file
def parse_vcf(merged_vcf: pathlib.Path) -> pd.DataFrame:
    """Extract depth and count of reference and mutated reads from an unfiltered VCF file produced
    by the ARTIC pipeline.

    Arguments:
        merged_vcf: the .merged.vcf file produced by the ARTIC pipeline

    Returns:
        Position, reference and mutated nucleotide(s), depth at the given position, and count
        of reference and mutated reads. Duplicates caused by overlap between primer pools are
        removed.
    """
    vcf_data = pd.read_csv(merged_vcf, sep="\t", comment = "#", names = ["CHROM", "POS", "ID",
                                                                         "REF", "ALT", "QUAL",
                                                                         "FILTER", "INFO",
                                                                         "FORMAT", "SAMPLE"])
    vcf_data[["depth", "ref_count", "alt_count"]] = vcf_data["INFO"].str.extract(r"^DP="
                                                                                 r"(?P<depth>\d+);"
                                                                                 r"AC="
                                                                                 r"(?P<ref_count>\d+),"
                                                                                 r"(?P<alt_count>\d+);")
    vcf_data = vcf_data.astype({"depth": int, "ref_count": int, "alt_count": int})
    # remove duplicates here - TODO: better separately?
    vcf_data = deduplicate_vcf(vcf_data)
    return vcf_data[["POS", "REF", "ALT", "depth", "ref_count", "alt_count"]]


# filter VCF according to minimum depth and get any positions that potentially are mixed
def find_mixed_positions(vcf_with_counts: pd.DataFrame, min_depth: int=20,
                         min_alt_proportion: float=0.1,
                         max_alt_proportion: float=0.7) -> pd.DataFrame:
    """Find "mixed" positions suggesting distinct subpopulations of reads in results extracted from
    ARTIC-generated VCF files.
    Default cutoffs for proportion of alternate reads were taken from Jacot et al. (2021):
    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8451431/


    Arguments:
        vcf_with_counts:    a parsed VCF file where depth and amount of reference/alternate reads
                            have been extracted from the INFO column
        min_depth:          the minimum depth required to consider the position for analysis
        min_alt_proportion: the minimum proportion of alternate reads required to consider "mixed"
                            read populations - enough this is a believable signal
        max_alt_proportion: the maximum proportion of alternate reads required to consider "mixed"
                            read populations - not so much it's a clear mutation

    Returns:
          Position, reference and alternate allele as well as proportion of reference and alternate
          reads for all "mixed" positions.
    """
    if min_depth <= 0:
        raise ValueError("Minimum required depth cannot be lower than 1.")
    if min_alt_proportion < 0:
        raise ValueError("Minimum required proportion of alternate reads for mixed positions must"
                         " be positive.")
    if max_alt_proportion < 0:
        raise ValueError("Maximum required proportion of alternate reads for mixed positions "
                         "must be positive.")
    if min_alt_proportion > max_alt_proportion:
        raise ValueError("Minimum required proportion of alternate reads for mixed positions "
                         "cannot be higher than maximum proportion.")

    reads_by_depth = vcf_with_counts[vcf_with_counts["depth"] >= min_depth].copy()
    reads_by_depth["proportion_ref"] = reads_by_depth["ref_count"] / reads_by_depth["depth"]
    reads_by_depth["proportion_alt"] = reads_by_depth["alt_count"] / reads_by_depth["depth"]
    mixed_reads = reads_by_depth[reads_by_depth["proportion_alt"].between(min_alt_proportion,
                                                                          max_alt_proportion)]
    mixed_reads = mixed_reads.reset_index(drop = True)
    return mixed_reads



def in_homopolymer_region(reference_bases: str, mutation_bases: str, position: int,
                          reference_sequence: Seq.Seq, repeat_cutoff: int=5) -> bool:
    """
    Detect whether a position with an ambiguous mutation lies in a stretch of repeated nucleotides
    (either in the original sequence or caused by the mutation) which may cause sequencing errors.
    By default, it is assumed a stretch of five or more identical nucleotides are problematic for
    Nanopore sequencing
    (see Delahaye/Nicolas 2021;
     https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0257521)

    Arguments:
        reference_bases:    the base(s) present at the position in the reference
        mutation_bases:     the base(s) present at the position in mutated reads
        position:           the start of the ambiguous position in the genome
        reference_sequence: the reference sequence used in consensus generation
        repeat_cutoff:      the lowest amount of repeats assumed to cause errors in sequencing

    Returns:
        True if the position lies in a stretch of repeated nucleotides, False otherwise.
    """
    # VCF files report position 1-based, we need 0-based
    corrected_position = position - 1
    # for longer mutations: get the end position - account for string slicing excluding the end
    end_position = corrected_position + len(reference_bases)
    # look for repeat_cutoff repeated nucleotides in a window around the position: it always needs
    # to contain the position
    # the start needs to be (repeat_cutoff - 1) nt out from the position so it's the
    # "repeat_cutoff'th" nucleotide
    search_start = corrected_position - (repeat_cutoff - 1)
    # the end also needs to be (repeat_cutoff - 1) nt out from the last nucleotide
    search_end = end_position + (repeat_cutoff - 1)
    # we may need to adjust this if we end up before the start or after the end
    search_start = max(search_start, 0)
    search_end = min(search_end, len(reference_sequence))
    if reference_sequence[corrected_position:end_position] != reference_bases:
        raise ValueError(f"Mismatch between reference base reported and base found at "
                         f"position {position}")
    # search in the sequence: are there repeat_cutoff repeats of reference in the reference,
    # starting repeat_cutoffs before the position and ending repeat_cutoffs after it
    for reference_base in set(reference_bases):
        homopolymer_in_ref = reference_sequence.find(reference_base * repeat_cutoff,
                                                     start = search_start, end = search_end)
        # a hit here is enough to say we may have a problem
        if homopolymer_in_ref > -1:
            return True
    # but we may only have a homopolymer due to the mutation: apply it and check again
    mutated_seq = Seq.MutableSeq(reference_sequence)
    # is this a deletion, insertion, or plain substitution?
    if len(reference_bases) > len(mutation_bases):
        # apply the mutations to the end of what's there
        mutated_seq_end = corrected_position + len(mutation_bases)
        mutated_seq[corrected_position:mutated_seq_end] = mutation_bases
        # then remove the rest - take everything up to the end of the mutated sequence, then
        # everything after the original sequence
        mutated_seq = mutated_seq[:mutated_seq_end] + mutated_seq[end_position:]
    elif len(mutation_bases) > len(reference_bases):
        # change the bases that already are there
        mutated_seq[corrected_position:end_position] = mutation_bases[:len(reference_bases)]
        # then add the rest one by one
        remaining_bases = mutation_bases[len(reference_bases):]
        # we start inserting where the reference ends
        insert_position = end_position
        for mutated_base in remaining_bases:
            mutated_seq.insert(insert_position, mutated_base)
            # the next base will have to be inserted after that
            insert_position += 1
    else:
        mutated_seq[corrected_position:end_position] = mutation_bases
    for mutation_base in set(mutation_bases):
        homopolymer_in_alt = mutated_seq.find(mutation_base * repeat_cutoff, start = search_start,
                                              end = search_end)
        if homopolymer_in_alt > -1:
            return True
    # now we've checked everything
    return False


def filter_homopolymers(mixed_reads: pd.DataFrame, reference_sequence: Seq.Seq,
                        homopolymer_cutoff: int=5) -> pd.DataFrame:
    """Remove ambiguous positions likely caused by homopolymer errors.

    Args:
        mixed_reads:        Position, reference and alternate allele as well as quality and
                            proportion of reference and alternate reads for all "mixed" positions.
        reference_sequence: the reference sequence used in consensus generation
        homopolymer_cutoff: the lowest amount of repeats assumed to cause errors in sequencing

    Returns:
        Position, reference and alternate allele as well as quality and proportion of reference
        and alternate reads for all "mixed" positions likely not caused by homopolymer errors.
    """
    # if we don't have any variants to start with, we can stop here
    if mixed_reads.empty:
        return mixed_reads
    # how many homopolymers do we have?
    homopolymers = mixed_reads.apply(lambda row:
                                     in_homopolymer_region(row["REF"],
                                                           row["ALT"],
                                                           row["POS"],
                                                           reference_sequence,
                                                           homopolymer_cutoff),
                                     axis = 1)
    homopolymer_count = sum(homopolymers.tolist())
    if homopolymer_count:
        logger.info(f"Removing {homopolymer_count} variants likely caused by homopolymer errors.")
    # retain only the rows that are *not* a homopolymer position
    filtered_mixed = mixed_reads[~homopolymers]
    filtered_mixed = filtered_mixed.reset_index(drop=True)
    return filtered_mixed


if __name__ == "__main__":
    REF_PATH = (pathlib.Path(__file__).parent / "data" / "primer_schemes"/ "nCoV-2019" / "V1200.3"
                / "nCoV-2019.reference.fasta")
    arg_parser = ArgumentParser(description = "Extract potential mixed positions from unfiltered "
                                              "ARTIC VCF files for a given sample and save in"
                                              "tab-separated format. Mixed positions that may be"
                                              "caused by homopolymer errors are disregarded.")
    arg_parser.add_argument("merged_vcf",
                            help="Unfiltered merged ARTIC VCF file (*.merged.vcf) to analyze")
    arg_parser.add_argument("--sample_id", help="Sample ID (default: taken from VCF file's name)")
    arg_parser.add_argument("--outfile",
                            help="Path to save overview of mixed positions to "
                                 "(default: mixed_positions.tsv)",
                            default = "mixed_positions.tsv")
    arg_parser.add_argument("--min_depth",
                            help="minimum depth required to consider the position for analysis"
                                 " (default: 20)", default=20, type = int)
    arg_parser.add_argument("--min_alt", help='minimum proportion of alternate reads required to '
                                              'consider "mixed" read populations '
                                              '- enough this is a believable signal (default: 0.1)',
                            default=0.1, type=float)
    arg_parser.add_argument("--max_alt", help='maximum proportion of alternate reads required to '
                                              'consider "mixed" read populations '
                                              '- not so much it is a clear mutation (default: 0.7)',
                            default=0.7, type = float)
    arg_parser.add_argument("--reference_sequence",
                            help=f'Sequence to use for identifying homopolymers (default: '
                                 f'{REF_PATH}',
                            default = REF_PATH)
    arg_parser.add_argument("-l", "--logfile", help="File to write log to "
                                                    "(default: stdout)")

    args = arg_parser.parse_args()
    merged_variants = pathlib.Path(args.merged_vcf)
    outfile = pathlib.Path(args.outfile)
    reference = pathlib.Path(args.reference_sequence)
    reference_seq = SeqIO.read(reference, "fasta")
    if args.logfile:
        # set logging to file
        log_file = logging.FileHandler(args.logfile)
        log_file.setLevel(logging.INFO)
        logfile_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        log_file.setFormatter(logfile_formatter)
        logger.addHandler(log_file)
    # in our pipeline the base name is always the full sample ID - use this, but allow alternatives
    if args.sample_id:
        sample_id = args.sample_id
    else:
        # the name pattern is "SAMPLEID.merged.vcf" - get rid of both suffixes at once
        sample_id = merged_variants.with_suffix('').stem
    try:
        vcf_with_depths = parse_vcf(merged_variants)
        mixed_positions = find_mixed_positions(vcf_with_depths, min_depth = args.min_depth,
                                               min_alt_proportion = args.min_alt,
                                               max_alt_proportion = args.max_alt)
        filtered_positions = filter_homopolymers(mixed_positions, reference_seq.seq)
    except ValueError as value_err:
        logging.exception(value_err)
        raise
    filtered_columns = filtered_positions.columns.tolist()
    filtered_positions["proevenr"] = sample_id
    filtered_columns.insert(0, "proevenr")
    filtered_positions[filtered_columns].to_csv(path_or_buf = outfile, sep="\t", index=False)
