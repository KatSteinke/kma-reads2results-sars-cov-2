library(here)
#library(ISOweek)
library(knitr)
library(lubridate)
library(rmarkdown)
library(yaml)

# define location to reference Rmd later
here::i_am("Rscripts/count_samples.R")
# get date
today <- Sys.Date()

args = commandArgs(trailingOnly = TRUE)
# set up optional arguments as per https://stackoverflow.com/a/30348837 - TODO: do we need this?
if (length(args) > 4) {
  stop("Too many arguments. Please specify *only* config file, target directory, reanalysis results and optionally a cutoff date as YYYY-MM-DD.")
}
if (length(args) < 3) {
  stop("Missing arguments. Please specify config file, target directory, reanalysis results and optionally a cutoff date as YYYY-MM-DD. If no cutoff date is given, cutoff is assumed to be today.")
}
# get sample number settings and location of cleaned file
workflow.config.file <- args[1]
# save report somewhere
report.dir <- args[2]
# since we reanalyze weekly we need to supply the path to the newest reanalysis
all.results.cleaned <- args[3]
# get cutoff date if needed - if blank, insert today's date
cutoff.date <- if (length(args) == 3) Sys.Date() else args[4]
cutoff.date <- lubridate::ymd(cutoff.date)


#workflow_config = yaml.load_file(workflow.config.file)

#all.results.cleaned <- workflow_config$paths$cleaned_all_results


rmarkdown::render(here("Rscripts", "report_samples.Rmd"),
                  output_format="html_document",
                  output_file=paste0(report.dir, "/", as.character(lubridate::isoyear(cutoff.date)),
                                        "-",
                                        as.character(lubridate::isoweek(cutoff.date)),
                                        "_rapport.html"),
                  params=list("result_file" = all.results.cleaned,
                              "config_file" = workflow.config.file,
                              "cutoff_date" = cutoff.date))