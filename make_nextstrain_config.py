"""Generate builds and config file for Nextstrain with user-defined metadata and date cutoffs."""

__author__ = "Kat Steinke"

import pathlib

from argparse import ArgumentParser
from datetime import date, datetime, timedelta
from typing import Optional

import version
__version__ = version.__version__

def get_build_string(build_name: str, metadata: pathlib.Path, sequences: pathlib.Path, cutoff_date: date,
                     region: Optional[str] = "Europe", country: Optional[str] = "Denmark",
                     division: Optional[str] = "Syddanmark") -> str:
    """Generate a builds.yaml file for Nextstrain based on given metadata and sequence
    files, using a supplied cutoff date.
    Arguments:
        build_name:     Name of build
        metadata:       path to file containing tab-separated metadata for Nextstrain
        sequences:      path to file containing sequences to be analyzed with Nextstrain in FASTA format
                        (names must match those in metadata file)
        cutoff_date:    earliest date to sample from
        region:         geographical region (Africa, Asia, Europe, North America, Oceania, South America) covered by
                        build
        country:        country covered by build
        division:       division covered by build
    Returns:
        Content of the nextstrain build file
    """
    # sanity checks:
    # do files exist?
    if not metadata.exists():
        raise FileNotFoundError("Metadata file does not exist.")
    if not sequences.exists():
        raise FileNotFoundError("Sequence file does not exist.")
    # does our earliest sampling date make sense (not in the future)?
    if cutoff_date > date.today():
        raise ValueError("Earliest sampling date is in the future.")
    # is the region okay?
    allowed_regions = {"Africa", "Asia", "Europe", "North America", "Oceania", "South America"}
    if region not in allowed_regions:
        raise ValueError(f"{region} is not a valid region. Valid regions are {str(allowed_regions)}.")
    date_formated = date.strftime(cutoff_date, "%Y-%m-%d")
    build_string = f"""inputs:
  - name: ouh-covid19
    metadata: {str(metadata)}
    sequences: {str(sequences)}



builds:
  {build_name}:
      subsampling_scheme: division
      region: {region}
      country: {country}
      division: {division}


subsampling:
  {build_name}:
    division:
      group_by: "year month"
      max_sequences: 4500
      min_date: "{date_formated}"
      exclude: "--exclude-where 'division!={{division}}'"
    # context samples from rest of the country
    country:
      group_by: "division year month"
      seq_per_group: 20
      min_date: "{date_formated}"
      exclude: "--exclude-where 'country!={{country}}'"
    region:
      group_by: "country division year month"
      seq_per_group: 20
      min_date: "{date_formated}"
      exclude: "--exclude-where 'region!={{region}}'"
      """
    return build_string

def get_config_string(default_path: pathlib.Path, build_path: pathlib.Path) -> str:
    """Generate a config file for Nextstrain based on paths to default
    parameters and a Nextstrain build file.
    Arguments:
        default_path:       Path to .yaml file with default parameters
        build_path:         Path to the builds.yaml file containing Nextstrain builds
    Returns:
        Content of the Nextstrain config file.
    """
    if not default_path.exists():
        raise FileNotFoundError("Default parameter file does not exist.")
    if not build_path.exists():
        raise FileNotFoundError("Build file does not exist.")
    config_string = f"""configfile:
      - {str(default_path)}/defaults/parameters.yaml # Pull in the default values
      - {str(build_path)} # Pull in our list of desired builds

# Set the maximum number of cores you want Snakemake to use for this pipeline.
cores: 2

# Always print the commands that will be run to the screen for debugging.
printshellcmds: True

# Print log files of failed jobs
show-failed-logs: True
    """
    return config_string

if __name__ == "__main__":
    arg_parser = ArgumentParser("Generate configuration files for Nextstrain (build file and config file pointing at build file).")
    arg_parser.add_argument("metadata", help="Path to tab-separated file with metadata for Nextstrain")
    arg_parser.add_argument("sequences", help="Path to FASTA file with sequences to analyze in Nextstrain")
    arg_parser.add_argument("defaults", help="Path to .yaml file with default settings for Nextstrain")
    arg_parser.add_argument("--builds_out", help="Path to file to output build configuration to (default: builds.yaml)",
                            default="builds.yaml")
    arg_parser.add_argument("--config_out", help="Path to file to output config file to (default: config.yaml)",
                            default="config.yaml")
    arg_parser.add_argument("--region", help="Region for build to create (default: Europe)", default="Europe",
                            choices={"Africa", "Asia", "Europe", "North America", "Oceania", "South America"})
    arg_parser.add_argument("--country", help="Country for build to create (default: Denmark)", default="Denmark")
    arg_parser.add_argument("--division", help="Country for build to create (default: Syddanmark)",
                            default="Syddanmark")
    arg_parser.add_argument("--cutoff_date",
                            help="Earliest date for sampling in format YYYY-MM-DD (default: 30 days before current date)",
                            default=None)
    arg_parser.add_argument("--build_name", help="Name for nextstrain build (default: [country]_[division]",
                            default=None)
    args = arg_parser.parse_args()
    nextstrain_metadata = pathlib.Path(args.metadata)
    nextstrain_sequences = pathlib.Path(args.sequences)
    defaults = pathlib.Path(args.defaults)
    build_file = pathlib.Path(args.builds_out)
    config_file = pathlib.Path(args.config_out)
    build_region = args.region
    build_country = args.country
    build_division = args.division
    if args.cutoff_date:
        # parse date if present
        sampling_cutoff = datetime.strptime(args.cutoff_date, "%Y-%m-%d").date()
        # sanity check that date isn't in the future
        if sampling_cutoff > date.today():
            raise ValueError("Earliest sampling date is in the future.")
    else:
        sampling_cutoff = date.today() - timedelta(days=30)
    # generate build name if needed
    if args.build_name:
        buildname = args.build_name
    else:
        buildname = f"{build_country.lower()}_{build_division.lower()}"
    # generate build file
    buildstring = get_build_string(buildname, nextstrain_metadata, nextstrain_sequences, sampling_cutoff,
                                    region=build_region, country=build_country, division=build_division)
    with open(build_file, "w", encoding="utf-8") as build_outfile:
        build_outfile.write(buildstring)

    # generate config file
    configstring = get_config_string(defaults, build_file)
    with open(config_file, "w", encoding="utf-8") as config_outfile:
        config_outfile.write(configstring)
