# KMA OUH Reads-to-Results SARS-CoV-2 Nanopore pipeline
This pipeline uses ARTIC to generate consensus sequences from Nanopore reads, then runs Pangolin and Nextclade
on these sequences. The pipeline also produces automatic interpretation of the output as well as an
ASTM file for use with the MADS laboratory information system, if used.

The pipeline was originally based on a shell script that was created at Aalborg University to run ARTIC and expanded 
by Thomas Vognbjerg Sydenham at KMA Odense to run Pangolin and Nextclade and parse their output. Parts of the 
depth-by-position visualization script were adapted from a Python script created by Christian Højte Schouw at KMA 
Slagelse.\
The current maintainer is Kat Steinke at KMA Odense. 

## Prerequisites
The pipeline's prerequisites are found in `pipeline_env.yml`. To install this, conda or mamba are required (note that 
mamba is preferable as installation of some environments takes far longer with conda.) \
The pipeline automatically downloads missing primers, Guppy basecalling models,... This requires a functioning internet 
connection. As this can pose a challenge, the pipeline already comes with primers for V3, V4, V4.1 and midnight
protocols, as well as the default medaka model specified in the default pipeline config file.

### Nanopolish workflow
The Nanopolish workflow additionally requires installation of the `ont_vbz_hdf_plugin` package. Due to compatibility 
issues with the remaining ARTIC pipeline environment, this has to be installed in a separate environment; the path to
this environment is then specified in the config file (see below). It is possible to use the `hdf5_standalone` 
conda environment that comes with the pipeline for this purpose. 

## Setting up the pipeline
The pipeline can be obtained by simply cloning the repository:
```shell
git clone https://gitlab.com/KatSteinke/kma-reads2results-sars-cov-2.git path/to/pipeline 

```
To enable starting the pipeline from the desktop, copy `sars-cov2-pipeline-launcher.desktop` to the desktop,
change the path to the `setup_pipeline.sh` script and ensure that both the launcher and `setup_pipeline.sh` are
executable.\
Replace the placeholders in `setup_pipeline.sh` to point at your local conda installation and `run_pipeline.py`. \ 
The pipeline can additionally be configured to match local parameters (storage locations, sample number formats,
...), as described below. 
### Configuring the pipeline
A default config file with placeholders is provided; copy the file, rename it to `pipeline_routine.yaml` and replace 
the placeholders. 

#### Primer scheme naming
Primer schemes are named as followed under `primer_scheme` in the config file (and data directory):
* the original ARTIC primer schemes (3, 4, and 4.1) are referred to by version number
* the original Midnight primers are named "1200"
* ONT's Midnight V3 primers are named "1200.3"


#### Laboratory information system settings
Some of the features of this pipeline depend on external reports from a laboratory information system. Specifically, 
this is the case for:
* automated interpretation of results (specifically reporting of sampling dates)
* "cleanup" of results (to remove research samples etc.) before adding to an overview of all results
* generation of metadata for upload of samples to SSI

Additionally, if your laboratory system allows it, a file with all the results for import into the system can be 
generated. Currently, only a script for creating ASTM files for OUH's MADS setup exists (but if you develop one for
your setup, you're more than welcome to send a merge request!).

The pipeline was designed with OUH's MADS setup in mind. Report files therefore are 
assumed to
* be comma-separated
* be encoded using `latin-1` encoding
* contain the following fields:
  * prøvenr: sample number
  * afsendt: sampling date
  * modtaget: date received
  * cprnr.: patient's CPR number
* contain sample numbers starting with letters

If your laboratory information system differs, get in touch with this pipeline's maintainer(s) for help with
implementation - the idea is to make things as flexible as needed over time. 

The ability to deactivate laboratory information system-dependent functions or replace them with workarounds is planned 
for future updates. 

## Running the pipeline
The pipeline can be run in two modes: "classic" mode, primarily meant for users not 
comfortable with running commands on the commandline, and "commandline" mode, which 
offers greater control over the pipeline.
### Classic mode
Classic mode can be started from the desktop if enabled - clicking the desktop shortcut will automatically launch
the pipeline in classic mode. Alternatively, the pipeline can be started by running
```shell
python3 run_pipeline.py
```
without any options in the virtual environment containing all prerequisites. \
Classic mode guides the user through the process in a "questionnaire" style. \
As it is meant for routine use by non-expert users, some options cannot be changed in classic mode:
* **primer version**: primer version is set to the default primer version given in the config file (v4 by default)
* **test vs routine mode**: in classic mode, only routine runs can be started. This means fasta and other files will 
automatically be copied to the respective files and directories if specified, and error messages are output in a less 
detailed format.
* **continuing a stopped run**: a stopped run cannot be continued in classic mode, only in commandline mode
* **reanalyzing part of a run**: the dedicated reanalysis mode (see below) can only be started in commandline mode
* **passing flags to Snakemake**: additional flags can only be passed to Snakemake in commandline mode

### Commandline mode
Commandline mode is minimally run as follows:
```shell
python3 run_pipeline.py --rundir path/to/rundir --runsheet path/to/runsheet
```
This runs the pipeline with the same default settings as in classic mode. Additionally, the following options can be 
specified:
* `--outdir`: output directory (also in classic mode)
* `--ssi_dir`: directory in which to save files for upload to SSI (also in classic mode; default: date in YYMMDD format + 
department's SHAKID)
* `--primer_version`: primer version used for current run (choice of v3, v4, v4.1, and midnight)
* `--continue_pipeline`: flag to specify continuing a previously stopped run
* `--test_run`: flag to specify a test run; files generated are not copied to directories containing data from all 
(routine) runs, and more detailed error messages are output. 
* `--reanalyze`: flag to specify reanalysis of (part of) a run. In reanalysis mode:
  * results are added to both raw and cleaned result collections
  * consensus fastas are copied to all_fasta directory with a new (timestamped) name
  * samples are *not* readded to metadata database to avoid duplicates
* `--workflow_config_file`: configuration file with settings to use for current run
* `--snake_flags`: Flags to pass to Snakemake, enclosed in quotes - e.g. if one wants to use `-n` for a dry run or needs
to `--unlock` the working directory after an interrupted run. **Note** that if only one flag is passed to Snakemake, it 
needs to be followed by a space (e.g. `--snake_flags "-n "`)

### Input formats
The pipeline requires the following inputs:
* Nanopore sequencing data in .fastq format for the `medaka` workflow or in .fast5 format for the `nanopolish` workflow.
  The `nanopolish` workflow additionally requires a sequencing summary.
* a "runsheet" with information about each sample in .xlsx format, containing sample number, barcode and Ct value
* and optionally:
  * a report from your laboratory information system, containing at least sample numbers, sampling dates, date received and patients' 
  CPR numbers for output relying on this (see above under "Laboratory information system settings")
  * for Nextstrain functionality, metadata and sequence files obtained from SSI

#### Minimal runsheet
As the pipeline was developed with the runsheets already in use at KMA Odense in mind, the runsheet is expected to be 
set up as follows:
* the first row is a header with sequencing type (unused in this pipeline)
* the next two rows contain metadata on the sequencing run. If multiple sequencing protocols are used depending on lab 
parameters, the parameter needs to be specified here:
```
  |foo|bar|baz|...|protocol_identifier|
  -------------------------------------
  |bla|abc|xyz|...|   lab_parameter   |
```
The following rows need to have these three columns as the first columns:
* KMA nr: sample number
* CT/CP værdi: Ct value
* Barkode NB: barcode

A more complete example is given under `examples`. **Note** that if database features are used, 
 the layout *must* match the example runsheet's layout as closely as possible, 
 as some fields relevant for metadata currently have to be accessed by column index.

## Updating the pipeline
To update the pipeline, run
```shell
git pull
```
in the directory containing the repository. 

