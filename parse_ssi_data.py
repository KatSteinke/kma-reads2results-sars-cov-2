"""Generate a Nextstrain metadata file from the metadata supplied by SSI."""

__author__ = "Kat Steinke"

import logging
import pathlib
from argparse import ArgumentParser
from datetime import date, datetime, timedelta
from typing import Optional

import pandas as pd
import yaml

import check_config
import pipeline_config
import version
__version__ = version.__version__


# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

def parse_ssi_metadata(ssi_meta_file: pathlib.Path, sampling_start: date, sampling_end: date,
                       logfile: Optional[pathlib.Path]="ssi_log.txt",
                       hospital_samples: Optional[bool]=False) -> pd.DataFrame:
    """Parse Nextstrain information (where applicable) out of SSI metadata file.
    Arguments:
        ssi_meta_file:     File with metadata for sequences for region
        sampling_start:    Earliest sampling date to include
        sampling_end:      Latest sampling date to include
        logfile:           Path to write logfile to
        hospital_samples:  Drop all non-hospital samples
    Returns:
        An overview of the metadata relevant for Nextstrain.
    """
    # log any issues
    # start logging
    logger = logging.getLogger("extract_metadata")
    logger.setLevel(logging.INFO)
    log_to_file = logging.FileHandler(logfile)
    log_to_file.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_to_file.setFormatter(logfile_formatter)
    console_log = logging.StreamHandler()
    console_log.setLevel(logging.WARNING)
    logger.addHandler(log_to_file)
    logger.addHandler(console_log)
    # before we do anything, check if the earliest sampling date makes sense
    if sampling_start > date.today():
        raise ValueError("Earliest sampling date is in the future.")
    logger.info("Starting SSI metadata file parsing.")
    metafile = pd.read_csv(ssi_meta_file, sep="\t")[["seq_id", "date_sampling", "meta_source", "Region",
                                                     "lineage"]]
    metafile = metafile.dropna()
    metafile = metafile.rename(columns={"seq_id": "strain", "date_sampling": "date", "Region": "division",
                                        "lineage": "pango_lineage"})
    # check for malformed date formats
    fail_dates = metafile["date"][~metafile["date"].apply(str).str.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2}')].tolist()
    # TODO: match up to sample numbers!
    if fail_dates:
        logger.warning(f"Malformed dates found: {', '.join(fail_dates)}. Samples with malformed dates will be removed.")
    metafile = metafile[metafile["date"].apply(str).str.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2}')]
    # trim after date: parse date and take anything with a sampling date larger than the start
    metafile["date_parsed"] = metafile["date"].apply(lambda x: datetime.strptime(str(x), "%Y-%m-%d").date()
                                                               if pd.notna(x)
                                                               else x)
    metafile = metafile[(sampling_end >= metafile["date_parsed"]) & (metafile["date_parsed"] >= sampling_start)]
    # if we only want hospital samples, remove everything else (while trying to catch misclassified samples)
    # select everything from metafile where meta_source isn't VMSwhatever or TCDK* - the KMA category has been dropped
    if hospital_samples:
        metafile = metafile[~(metafile["meta_source"].isin({"VMS/ViFU", "TCDK_old", "TCDK",
                                                                                     "TCDK_delta"}))]
    metafile["virus"] = "ncov"
    metafile["gisaid_epi_isl"] = ""
    metafile["genbank_accession"] = ""
    metafile["region"] = "Europe"
    metafile["country"] = "Denmark"
    metafile["location"] = workflow_config["nextstrain_params"]["adm_division"]
    metafile["region_exposure"] = "Europe"  # TODO: should be able to be changed
    metafile["country_exposure"] = "Denmark"
    metafile["division_exposure"] = "Denmark"
    metafile["segment"] = "genome"
    metafile["length"] = ""  # TODO: we can get genome length from results if we need
    metafile["host"] = "Human"
    metafile["age"] = ""  # TODO: should be able to be changed
    metafile["sex"] = ""  # TODO: should be able to be changed
    metafile["originating_lab"] = ""
    metafile["submitting_lab"] = ""
    metafile["authors"] = ""
    metafile["url"] = ""
    metafile["title"] = ""
    metafile["date_submitted"] = ""
    metafile = metafile[["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                                             "region", "country", "division", "location", "region_exposure",
                                             "country_exposure", "division_exposure", "segment", "length",
                                             "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                                             "url", "title", "date_submitted", "pango_lineage"]]
    # preserve root sequence
    logger.info("Adding root sequence.")
    root_frame = pd.DataFrame(
        [["Wuhan/Hu-1/2019", "ncov", "", "", "2019-12-26", "Asia", "China", "Hubei", "Wuhan", "Asia",
         "China", "Hubei", "genome", "", "Human", "", "", "", "", "", "", "", "", "B"]],
        columns=["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                 "region", "country", "division", "location", "region_exposure",
                 "country_exposure", "division_exposure", "segment", "length",
                 "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                 "url", "title", "date_submitted", "pango_lineage"],
        )
    metafile = metafile.append(root_frame, ignore_index=True)
    logger.info("Done with file parsing.")
    return metafile

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract Nextstrain metadata from an SSI metadata file.")
    arg_parser.add_argument("metadata_file", help="Path to SSI metadata file")
    arg_parser.add_argument("-o", "--outfile",
                            help="File to output Nextstrain metadata to (default: nextstrain_metadata.tsv)",
                            default="nextstrain_metadata.tsv")
    arg_parser.add_argument("-l", "--logfile", help="Path of logfile to write to (default: ssi_log.txt)",
                            default="ssi_log.txt")
    arg_parser.add_argument("--start_date",
                            help=f"Earliest date for sampling in format YYYY-MM-DD "
                                 f"(default: {workflow_config['nextstrain_params']['sampling_timeframe_days']} "
                                 f"days before current date)",
                            default=workflow_config['nextstrain_params']['sampling_timeframe_days'])
    arg_parser.add_argument("--end_date", help="Latest date for sampling in format YYYY-MM-DD (default: today)",
                            default=date.today())
    arg_parser.add_argument("--hospital_only", action="store_true",
                            help="Only use hospital samples (no community samples)")
    arg_parser.add_argument("--workflow_config_file",
                            help="Config file for run (overrides default config given in script, can be overridden "
                                 "by commandline options)")
    args = arg_parser.parse_args()
    ssi_metafile = pathlib.Path(args.metadata_file)
    out_file = pathlib.Path(args.outfile)
    log_file = pathlib.Path(args.logfile)
    hospital_only = args.hospital_only
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file)
        if not default_config_file.exists():
            raise FileNotFoundError("Config file not found.")
        with open(default_config_file, "r", encoding="utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        check_config.check_config(workflow_config)
    if args.start_date:
        # parse date if present
        start_of_sampling = datetime.strptime(args.start_date, "%Y-%m-%d").date()
        # sanity check that date isn't in the future
        if start_of_sampling > date.today():
            raise ValueError("Earliest sampling date is in the future.")
    else:
        start_of_sampling = date.today() \
                            - timedelta(days=workflow_config["nextstrain_params"]["sampling_timeframe_days"]) # TODO: to int!
    # parse end date if given as string
    if isinstance(args.end_date, str):
        end_of_sampling = datetime.strptime(args.end_date, "%Y-%m-%d").date()
    elif isinstance(args.end_date, date):
        end_of_sampling = args.end_date
    else:
        raise ValueError("Invalid format for latest sampling date.")
    # make sure end date is after start
    if start_of_sampling > end_of_sampling:
        raise ValueError("Earliest sampling date is later than latest sampling date.")

    nextstrain_metafile = parse_ssi_metadata(ssi_metafile, start_of_sampling, end_of_sampling, log_file,
                                             hospital_samples=hospital_only)
    nextstrain_metafile.to_csv(path_or_buf=out_file, sep="\t", index=False)
