"""
Create sample sheet from runsheet (workaround to avoid run directive in Snakemake).
"""


__author__ = "Kat Steinke"

import pathlib
import re
import warnings

from argparse import ArgumentParser
from datetime import datetime
from typing import Dict, List, Optional

import pandas as pd
import yaml

import check_config
import pipeline_config
from helpers import get_number_letter_combination
import version
__version__ = version.__version__

# suppress openpyxl warning about DataValidation as it doesn't impact data
warnings.filterwarnings('ignore',
                        message="Data Validation extension is not supported and will be removed",
                        module="openpyxl")


def infer_prefix(runsheet_data: pd.DataFrame, lab_data: pd.DataFrame,
                 negk_format: Optional[str] = 'NegK[0-9]*',
                 positive_controls: Optional[List[str]] = None) -> pd.DataFrame:
    """Infer sample type prefix for sample numbers (with year, but no prefix) from LIS report
    (containing sample numbers with year and prefix). Sample numbers in LIS report MUST be unique
    even without type prefixes.
    Arguments:
        runsheet_data:      Data contained in runsheet
        lab_data:           Data contained in laboratory information system report
                            (minimum: sample numbers)
        negk_format:        Format of negative control
        positive_controls:  Sample numbers for positive controls
    Returns:
        Runsheet data with prefix added to sample number.
    """
    lab_report_data = lab_data.copy()
    # remove prefix - TODO: this currently assumes a one-letter prefix, handle cases where it's not!
    lab_report_data["proevenr_kort"] = lab_report_data["prøvenr"].str.slice(start=1)
    if any(lab_report_data.duplicated(subset=["proevenr_kort"])):
        raise ValueError("Sample numbers are not unique after removing type prefix. "
                         "Cannot identify the correct prefix for ambiguous sample numbers.")
    # match prefix-less sample numbers
    runsheet_with_prefixes = runsheet_data.merge(lab_report_data, left_on="KMA nr",
                                                 right_on="proevenr_kort", how="left")
    # are there sample numbers that can't be found in the report? Controls won't - account for this
    if not positive_controls:
        positive_controls = ['34567890', '45678901'] # TODO: currently dummy numbers
    control_format = f"{negk_format}|{'|'.join(positive_controls)}"
    # get all sample numbers for which prefix couldn't be inferred
    missing_numbers = runsheet_with_prefixes["KMA nr"][(~(runsheet_with_prefixes["KMA nr"].str.match(control_format))) &
                                                       runsheet_with_prefixes["prøvenr"].isna()].tolist()
    if missing_numbers:
        pretty_print_missing = "\n".join(missing_numbers)
        raise ValueError(f"Prefix could not be inferred for the following samples:"
                         f" {pretty_print_missing}")

    # if this is fine, we can fill in the controls from the runsheet's sample numbers
    runsheet_with_prefixes["prøvenr"] = runsheet_with_prefixes["prøvenr"].fillna(runsheet_with_prefixes["KMA nr"])
    # now the original LIS report sample number field contains all the information we need
    runsheet_with_prefixes = runsheet_with_prefixes.rename(columns={"KMA nr": "KMA_old",
                                                                    "prøvenr": "KMA nr"})
    return runsheet_with_prefixes[["KMA nr", "Barkode NB", "CT/CP værdi"]]


def add_year_by_prefix(runsheet_data: pd.DataFrame, lab_data: pd.DataFrame, sheet_prefix: str,
                       lab_data_prefix: str) -> pd.DataFrame:
    """Fix IDs in runsheet (by identifying and adding year) for a given sample type
    (indicated by sample number prefixes).
    Arguments:
        runsheet_data:      Data contained in runsheet
        lab_data:           Data contained in laboratory information system report
                            (minimum: sample numbers and date received)
        sheet_prefix:       Sample number prefix designating desired sample type in sample sheet
        lab_data_prefix:    Sample number prefix corresponding to sheet_prefix in the format
                            used in the LIS report
    Returns:
        A dataframe with sample ID, barcodes and Ct value from the runsheet, with the year
        identified from the LIS report and added to the sample ID correspondingly.
    """
    # reduce runsheet to only the relevant samples - make a copy to ensure the original stays as is
    runsheet_filtered = runsheet_data[runsheet_data["KMA nr"].str.startswith(sheet_prefix)].copy()
    # check if this leaves us with any data
    if runsheet_filtered.empty:
        raise ValueError(f"No samples with prefix {sheet_prefix} found in runsheet.")
    # piece sample number together: get the prefix, identify the year, add the last six characters
    # prefix is already known
    runsheet_filtered["proevenr_prefix"] = sheet_prefix
    # extract last six characters
    runsheet_filtered["proevenr_kort"] = runsheet_filtered["KMA nr"].str.slice(start=-6)

    # to find year, check lab information system data and go for date *received*
    # extract relevant samples again
    lab_data["prøvenr"] = lab_data["prøvenr"].astype(str)
    lab_data_filtered = lab_data[lab_data["prøvenr"].str.startswith(lab_data_prefix)].copy()
    # get last six characters of sample number to match the one from the runsheet - TODO: adapt this - prefix length + year
    lab_data_filtered["proevenr_kort"] = lab_data_filtered["prøvenr"].str.slice(start=-6)

    # check if there are duplicates in this - should not happen
    if any(lab_data_filtered.duplicated(subset=["proevenr_kort"])):
        raise ValueError("MADS report contains duplicated sample numbers. "
                         "This likely means the report covers multiple years. "
                         "Get a new MADS report with the correct start date.")
    # check if there are mismatches between runsheet and MADS data
    missing_from_mads = runsheet_filtered[~runsheet_filtered["proevenr_kort"].isin(lab_data_filtered["proevenr_kort"])]["KMA nr"].dropna().tolist()
    if missing_from_mads:
        raise ValueError(f"Samples {missing_from_mads} were not found in MADS report. "
                         f"Please check that sample numbers are correct.")
    # join subsets of sheet and report - don't drop any samples not in the report!
    runsheet_filtered = runsheet_filtered.merge(lab_data_filtered, on="proevenr_kort", how="left")
    runsheet_filtered["modtaget"] = runsheet_filtered["modtaget"].apply(lambda x:
                                                                        datetime.strptime(str(x),
                                                                                          "%d%m%Y").date()
                                                                        if pd.notna(x)
                                                                        else x)
    runsheet_filtered["år_modtaget"] = runsheet_filtered["modtaget"].apply(lambda x: x.strftime("%y")
                                                                           if pd.notna(x)
                                                                           else "")
    runsheet_filtered["KMA nr"] = runsheet_filtered["proevenr_prefix"] \
                                  + runsheet_filtered["år_modtaget"]\
                                  + runsheet_filtered["proevenr_kort"]
    return runsheet_filtered[["KMA nr", "Barkode NB", "CT/CP værdi"]]


# TODO: fallback for if no LIS report?
def translate_and_add_year(run_sheet: pathlib.Path, lab_info_report: pathlib.Path,
                           prefix_translation: Optional[Dict[str, str]] = None,
                           negk_format: Optional[str]='NegK[0-9]*',
                           positive_controls: Optional[List[str]]=None) -> pd.DataFrame:
    """Parse a runsheet, extract sample ID, barcodes and Ct value, and add year to sample ID
    if needed.
    Arguments:
        run_sheet:          Path to runsheet
        lab_info_report:    Path to report from laboratory information system containing
                            sample numbers and date received
        prefix_translation: Mapping of numbers to letters in sample number
        negk_format:        Format of negative control
        positive_controls:  Sample numbers for positive controls
    Returns:
        A dataframe containing sample ID in number format, barcodes and Ct value
    """
    # load and prepare runsheet and LIS report
    sheet_data = pd.read_excel(run_sheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                            "Barkode NB": str})
    sheet_data = sheet_data.dropna()

    lab_info_data = pd.read_csv(lab_info_report, encoding="latin1", dtype={"afsendt": str,
                                                                           "cprnr.": str,
                                                                           "modtaget": str})

    # for each prefix, extract relevant data and join on these

    errors = []
    sheets = []

    if not prefix_translation:
        prefix_translation = {"70": "P", "40": "H"}

    if not positive_controls:
        positive_controls = ['34567890', '45678901'] # TODO: currently dummy numbers
    # get prefixes for all except the negative controls
    prefix_length = len(list(prefix_translation.keys())[0]) # TODO: make pretty
    all_prefixes = sheet_data["KMA nr"].apply(lambda x: x[:prefix_length]
                                                if not (re.match(negk_format, x)
                                                        or x in positive_controls)
                                               else pd.NA)
    unique_prefixes = all_prefixes.dropna().unique()
    for prefix in unique_prefixes:
        try:
            sheet_data_for_prefix = add_year_by_prefix(sheet_data, lab_info_data, prefix,
                                                       prefix_translation[prefix])
            sheets.append(sheet_data_for_prefix)
        except ValueError as value_err:
            errors.append(value_err)

    if errors:
        print("The following issues were encountered:")
        error_text ="\n".join([str(parsing_error) for parsing_error in errors])
        raise ValueError(error_text)
    # readd controls
    sheet_data_negk = sheet_data[sheet_data["KMA nr"].str.match(negk_format)]
    sheet_data_posk = sheet_data[sheet_data["KMA nr"].isin(positive_controls)]
    sheets.extend([sheet_data_negk, sheet_data_posk])
    sheet_data = pd.concat(sheets)
    # TODO: warn for missing data?
    return sheet_data[["KMA nr", "Barkode NB", "CT/CP værdi"]]


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract sample ID and barcode from a runsheet "
                                            "and write to sample sheet")
    arg_parser.add_argument("runsheet", help="Path to run sheet")
    arg_parser.add_argument("report_file", help="Path to laboratory information system report")
    arg_parser.add_argument("outfile", help="Desired output file")
    arg_parser.add_argument("--config_file",
                            help=f"Config file for run "
                                 f"(default: {pipeline_config.default_config_file})",
                            default=pipeline_config.default_config_file)
    args = arg_parser.parse_args()
    runsheet = pathlib.Path(args.runsheet)
    report_file = pathlib.Path(args.report_file)
    outfile = pathlib.Path(args.outfile)
    configfile = pathlib.Path(args.config_file)
    if not configfile.exists():
        raise FileNotFoundError("Config file not found.")
    with open(configfile, "r", encoding="utf-8") as config_file:
        workflow_config = yaml.safe_load(config_file)
    check_config.check_config(workflow_config)
    # check in which format samples should be returned
    # only add year if it's actually needed
    # TODO: more modular handling, maybe feed into parse_and_fix after all? But can't infer both prefix *and* year
    # for now prefix handling is a new feature - if not added, assume it's not wanted
    if "infer_prefix" in workflow_config["sample_number_settings"]:
        INFER_PREFIX = workflow_config["sample_number_settings"]["infer_prefix"]
    else:
        print("Assuming sample numbers in runsheet have prefixes. If prefix should be inferred from LIS"
              "report, please add the respective setting to the config file.")
        INFER_PREFIX = False
    # -> if splice in date, do this, if infer prefix, do that, else just give the raw data
    if workflow_config["sample_number_settings"]["date_settings"]["splice_in_date"]:
        samples_translated = get_number_letter_combination(workflow_config["sample_number_settings"]["number_to_letter"],
                                                           workflow_config["sample_number_settings"]["sample_numbers_in"],
                                                           workflow_config["sample_number_settings"]["sample_numbers_out"])
        negative_control = workflow_config["sample_number_settings"]["negative_control"]
        positive_control = list(workflow_config["sample_number_settings"]["positive_control"].keys())
        samplesheet = translate_and_add_year(runsheet, report_file, samples_translated, negative_control,
                                             positive_control)
    elif INFER_PREFIX:
        run_sheet_data = pd.read_excel(runsheet, usecols = "A:C", skiprows = 3,
                                       dtype = {"KMA nr": str, "Barkode NB": str})
        run_sheet_data = run_sheet_data.dropna()
        lis_report_data = pd.read_csv(report_file, encoding = "latin1", dtype = {"afsendt": str,
                                                                                 "cprnr.": str,
                                                                                 "modtaget": str})
        negative_control = workflow_config["sample_number_settings"]["negative_control"]
        positive_control = list(workflow_config["sample_number_settings"]["positive_control"].keys())
        samplesheet = infer_prefix(run_sheet_data, lis_report_data, negk_format = negative_control,
                                   positive_controls = positive_control)
    else:
        run_sheet_data = pd.read_excel(runsheet, usecols = "A:C", skiprows = 3,
                                       dtype = {"KMA nr": str, "Barkode NB": str})
        run_sheet_data = run_sheet_data.dropna()
        samplesheet = run_sheet_data[["KMA nr", "Barkode NB", "CT/CP værdi"]]
    samplesheet.to_csv(path_or_buf=outfile, index=False)
