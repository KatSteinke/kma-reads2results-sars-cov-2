"""Configure MultiQC visualization per run."""

__author__ = "Kat Steinke"

import pathlib
from argparse import ArgumentParser
from typing import Dict

import pandas as pd
import yaml

import version
__version__ = version.__version__


def make_barcode_config(sample_sheet: pathlib.Path) -> pd.DataFrame:
    """Generate multiqc config file for creating show/hide buttons.
    Arguments:
        sample_sheet: Path to the sample sheet listing sample numbers and barcodes
    Returns:
        Configuration parameters for MultiQC's show/hide buttons
    """
    # TODO fix naming
    filter_params = pd.read_csv(sample_sheet)
    filter_params["barcode_name"] = "barcode " + filter_params["Barkode NB"]
    filter_params["show_status"] = "show"
    filter_params["pattern"] = "_" + filter_params["Barkode NB"]
    return filter_params[["barcode_name", "show_status", "pattern"]]


def get_module_order(sample_data: pd.DataFrame, run_name: str) -> Dict:
    """Create MultiQC configuration file to order output by sample number and type
    (coverage first, then mutations).
    Arguments:
        sample_data: Parsed sample sheet
        run_name:   Name of rundir (prepended to sample number in mutation visualization files)
    Returns:
        Configuration for MultiQC
    """
    # order names from lowest to highest
    sample_data = sample_data.astype({"KMA nr": str}) # coerce KMA nr to string to be able to handle it
    sample_data = sample_data.sort_values(by="KMA nr")
    sample_data["full_sample_name"] = run_name + "_" + sample_data["KMA nr"] + "_" + sample_data["Barkode NB"]
    sample_name_sorted = sample_data["full_sample_name"].tolist()
    general_overviews = ["mixed_positions", "all_mutations"]
    # order names with "_mutation" from lowest to highest
    sample_mutations = [f"{sample_name}_mutations" for sample_name in sample_name_sorted]
    all_figs = [*sample_name_sorted, *general_overviews, *sample_mutations]
    # for each figure:
    #  record a priority value (starting at (10 x length of all), decreasing by 10 for each)
    priority_value = len(all_figs) * 10
    priorities = {}
    for figure in all_figs:
        priorities[figure] = {"order": priority_value}
        priority_value -= 10

    report_order_settings = {"top_modules": ["custom_data_lineplot", "custom_data_json_table"],
                             "report_section_order": priorities}
    return report_order_settings


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Generate multiQC show/hide buttons dynamically from barcodes used")
    arg_parser.add_argument("sample_sheet", help="Path to sample sheet")
    arg_parser.add_argument("barcode_outfile", help="Path to write barcode config file to")
    arg_parser.add_argument("module_order_outfile", help="Path to write module order config file to")
    arg_parser.add_argument("run_name", help="Run name used as prefix for file names") # TODO: probably easier way?
    args = arg_parser.parse_args()
    samplesheet = pathlib.Path(args.sample_sheet)
    barcode_outfile = pathlib.Path(args.barcode_outfile)
    module_order_outfile = pathlib.Path(args.module_order_outfile)
    run_prefix = args.run_name
    config_options = make_barcode_config(samplesheet)
    config_options.to_csv(path_or_buf=barcode_outfile, sep="\t", index=False, header=False)
    sheet_data = pd.read_csv(samplesheet, dtype={"KMA nr": str})
    report_order = get_module_order(sheet_data, run_prefix)
    with open(module_order_outfile, "w", encoding="utf-8") as outfile:
        yaml.dump(report_order, outfile)
