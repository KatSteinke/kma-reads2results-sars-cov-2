#!/usr/bin/env bash
source /path/to/software/miniconda3/etc/profile.d/conda.sh
conda activate nanopore_corona_pipeline
python3 "/path/to/kma-reads2results-sars-cov-2/run_pipeline.py"
