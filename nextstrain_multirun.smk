__author__ = "Kat Steinke"

import pathlib
import re

from datetime import date, datetime, timedelta

import pandas as pd

import helpers
import version
__version__ = version.__version__

configfile: str(pathlib.Path(workflow.basedir) / "config" / "pipeline_routine.yaml")
# path to config file needs to be specified for other scripts
CONFIG_PATH = config["config_path"] if "config_path" in config else str(pathlib.Path(workflow.basedir)
                                                                        / "config" / "pipeline_routine.yaml")

# if no working directory is specified, take the path that is the parent of the base path
workdir: config["workdir"] if "workdir" in config else pathlib.Path(config["paths"]["output_base_path"]).parent

workdir_path = pathlib.Path.cwd()

PRIMER_VERSION = str(config["primer_version"])
PRIMER_NAME = "SARS-CoV-2" if PRIMER_VERSION == "4" else "nCoV-2019"

# check all runs of the last week:
data_dir = pathlib.Path(config["paths"]["output_base_path"])
#run_path = data_dir / "Kørsler"
all_runs = [x for x in data_dir.iterdir() if x.is_dir()]
# filter down to last week's runs
last_week_runs = []
last_week_base_names = []
for run in all_runs:
    # use consensus fasta date as directories apparently can glitch... TODO: test!
    if list(run.glob("*.all.consensus.fasta")):
        consensus_file = list(run.glob("*.all.consensus.fasta"))[0].resolve()
        find_base_name = re.search(r"^((?:\w|-)+)\.all\.consensus\.fasta$", consensus_file.name)
        if find_base_name:
            base_name = find_base_name.group(1)
        else:
            raise ValueError(f"Invalid run name in run directory {run.name}")
    else: # skip if there is no consensus file
        print(f"No consensus file found for {run.name}") # TODO: log?
        continue
    analysis_start = datetime.fromtimestamp(consensus_file.stat().st_ctime)
    analysis_age = datetime.now() - analysis_start
    if analysis_age <= timedelta(days=7):
        last_week_runs.append(run)
        last_week_base_names.append(base_name)

runs_with_base_names = pd.DataFrame(data={"run_names": [run.name for run in last_week_runs],
                                          "base_names": last_week_base_names})
print(runs_with_base_names)
runs_with_base_names = runs_with_base_names[~runs_with_base_names["run_names"].str.match(r"FEJL|TEST")]
outdir_names = runs_with_base_names["run_names"].tolist()
base_names = runs_with_base_names["base_names"].tolist()
#run_names = [run.name for run in last_week_runs if not (run.name.startswith("FEJL") or run.name.startswith("TEST"))]
print(f"Generating tree from {sorted(outdir_names)}")

# get date for start of analysis now so job can run overnight
analysis_date = date.strftime(date.today(),'%Y%m%d')

# make it possible to change output dir for testing?

rule run_all:
    input:
        nextstrain_result = f"trees/{analysis_date}/nextstrain/auspice/ncov_danmark_{config['nextstrain_params']['adm_division']}_{analysis_date}.json"

# extract metadata for these
rule nextstrain_metadata:
    input:
        own_metadata = f"{data_dir}/{{outdir_name}}/metadata/{{run_name}}_metadata.tsv",
        sample_sheet = f"{data_dir}/{{outdir_name}}/results/sample_sheet.csv",
        own_results = f"{data_dir}/{{outdir_name}}/{{run_name}}.results.csv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_file = CONFIG_PATH
    output:
        meta_for_nextstrain = f"trees/{analysis_date}/data/{{outdir_name}}/nextstrain/{{run_name}}_nextstrain_metadata.tsv"

    shell:
        """
        python3 "{params.snakemake_path}/parse_ouh_metadata.py" {input.own_metadata} {input.own_results} \
         -o {output.meta_for_nextstrain} --workflow_config_file {params.config_file} 
        """
# merge into metadata file - python script?

rule create_metadata_base:
    output:
        own_metadata_base =  "trees/{analysis_date}/data/own_metadata.tsv"
    shell:
        """
        printf "strain\tvirus\tgisaid_epi_isl\tgenbank_accession\tdate\tregion\tcountry\tdivision\tlocation\tregion_exposure\tcountry_exposure\tdivision_exposure\tsegment\tlength\thost\tage\tsex\toriginating_lab\tsubmitting_lab\tauthors\turl\ttitle\tdate_submitted\tpango_lineage\n" > {output.own_metadata_base} 
        """

rule merge_own_nextstrain:
    input:
        own_nextstrain_data = f"trees/{analysis_date}/data/{{outdir_name}}/nextstrain/{{run_name}}_nextstrain_metadata.tsv",
        own_metadata_base = rules.create_metadata_base.output.own_metadata_base
    output:
        check_append_nextstrain =  "trees/{analysis_date}/append_checks/{outdir_name}__{run_name}_append_nextstrain"
    threads: workflow.cores
    shell:
        """
        tail -n +2 {input.own_nextstrain_data} >> {input.own_metadata_base} && touch {output.check_append_nextstrain}
        """

rule download_primers:
    output:
        check_primer_scheme = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / f"V{PRIMER_VERSION}"/ "nCoV-2019.scheme.bed").resolve()),
        check_reference = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / f"V{PRIMER_VERSION}"/ "nCoV-2019.reference.fasta").resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
         curl --fail https://raw.githubusercontent.com/artic-network/primer-schemes/master/nCoV-2019/V{PRIMER_VERSION}/{PRIMER_NAME}.primer.bed \
         > "{output.check_primer_scheme}"
         curl --fail https://raw.githubusercontent.com/artic-network/primer-schemes/master/nCoV-2019/V{PRIMER_VERSION}/{PRIMER_NAME}.reference.fasta \
         > "{output.check_reference}"

         """
rule download_midnight_primers:
    output:
        check_primer_scheme = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCov-2019" / "V1200"/ "nCov-2019.scheme.bed").resolve()),
        check_reference = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCov-2019" / "V1200"/ "nCov-2019.reference.fasta").resolve())
    params:
        data_dir = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes").resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
         curl --fail https://zenodo.org/record/3897530/files/1200bp_amplicon_bed.tar.gz?download=1 \
         > "{params.data_dir}/midnight_1200bp_amplicon_bed.tar.gz"
        tar -zxf "{params.data_dir}/midnight_1200bp_amplicon_bed.tar.gz" --directory "{params.data_dir}/nCov-2019"

         """

# TODO: make more flexible?
rule download_ont_midnight:
    output:
        check_primer_scheme= str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / "V1200.3" / "nCoV-2019.scheme.bed").resolve()),
        check_reference = str((
                    pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / "V1200.3" / "nCoV-2019.reference.fasta").resolve())
    params:
        data_dir = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes").resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
        curl --fail https://raw.githubusercontent.com/epi2me-labs/wf-artic/master/data/primer_schemes/SARS-CoV-2/Midnight-ONT/V3/SARS-CoV-2.scheme.bed \
        > "{output.check_primer_scheme}"
        curl --fail https://raw.githubusercontent.com/epi2me-labs/wf-artic/master/data/primer_schemes/SARS-CoV-2/Midnight-ONT/V3/SARS-CoV-2.reference.fasta \
        > "{output.check_reference}"
        """

# generate consensus genome
# can we change fasta header here?
# TODO: can we slim this down?

# identify how to get primer scheme: different paths and different methods for different primers

if PRIMER_VERSION == "1200":
    reference_target = rules.download_midnight_primers.output.check_reference
elif PRIMER_VERSION == "1200.3":
    reference_target = rules.download_ont_midnight.output.check_reference
else:
    reference_target = rules.download_primers.output.check_reference


# get sequences for each and append to one file
rule create_sequences_base:
    input:
        reference = reference_target
    output:
        own_consensus_base = "trees/{analysis_date}/data/own_consensus.fasta"
    shell:
        """
        printf ">Wuhan/Hu-1/2019\n" > {output.own_consensus_base} && \
        tail -n +2 {input.reference} >> {output.own_consensus_base}
        """

# optimized version - bring your own regex, but avoids one extra sed expression
rule append_consensus:
    input:
        own_consensus = rules.create_sequences_base.output.own_consensus_base,
        sample_consensus = f"{data_dir}/{{outdir_name}}/{{run_name}}.all.consensus.fasta"
    output:
        check_append_fasta = "trees/{analysis_date}/append_checks/{outdir_name}__{run_name}_append_fasta"
    threads: workflow.cores # quickfix to ensure only one at a time runs
    shell:
        """
        sed -E 's/^>.+\\/[-a-zA-Z0-9_]+_70([0-9]{{8}})_NB[0-9]{{2}}\\/ARTIC\\/medaka MN908947\\.3/>OUH-P\\1/g; s/^>.+\\/[-a-zA-Z0-9_]+_40([0-9]{{8}})_NB[0-9]{{2}}\\/ARTIC\\/medaka MN908947\\.3/>OUH-H\\1/g' {input.sample_consensus} >> {input.own_consensus} && \
        touch {output.check_append_fasta}
        """

# adapt sample number and barcode patterns to be used in shell directive
sample_id_for_shell = helpers.double_up_escapes(config['sample_number_settings']['sample_number_format'])
barcodes_for_shell = helpers.double_up_escapes(config["barcode_format"])
sample_id_format = config['sample_number_settings']['sample_number_format'],
barcode_format = config["barcode_format"]

rule append_consensus_autoname:
    input:
        own_consensus=rules.create_sequences_base.output.own_consensus_base,
        sample_consensus=f"{data_dir}/{{outdir_name}}/{{run_name}}.all.consensus.fasta"
    threads: workflow.cores
    output:
        check_append_fasta="trees/{analysis_date}/append_checks/{outdir_name}__{run_name}_append_fasta_autoname" # for now to deactivate it
    params:
        replace_sample_numbers = "; ".join([f"s/^>{prefix_number}/>{config['nextstrain_params']['sample_prefix']}{prefix_letter}/g"
                                  for prefix_number, prefix_letter in config['sample_number_settings']['number_to_letter'].items()
                                  ]) # one expression for each letter-number pair, replacing the number with the SSI prefix and the letter
    shell:
        """
        sed -E 's/^>.+\\/[-a-zA-Z0-9_]+_({sample_id_format})_{barcode_format}\\/ARTIC\\/medaka MN908947\\.3/>\\1/g; {params.replace_sample_numbers}' {input.sample_consensus} >> {input.own_consensus} && \
        touch {output.check_append_fasta}
        """

rule autoname_all:
    input:
        expand(f"trees/{analysis_date}/append_checks/{{outdir_name}}__{{run_name}}_append_fasta_autoname",
            zip, outdir_name=outdir_names, run_name=base_names)

# merge with SSI data
rule filter_ssi_metadata:
    input:
        #ssi_meta_file = "/data/KMA_sekvenser/SARS2_data/from_SSI/metadata.tsv"
        #ssi_meta_file = "/data/KMA_sekvenser/SARS2_data/from_SSI/current/metadata.tsv",
        ssi_meta_file = config["nextstrain_params"]["ssi_metadata_path"]
        #ssi_meta_file = "/mnt/t/ouh/afd/KMA_sekvenser/SARS2_data/from_SSI/current/metadata.tsv"
    output:  # TODO: move?
        ssi_for_nextstrain = "trees/{analysis_date}/data/{analysis_date}_ssi_filtered.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        start_date = lambda wildcards: (datetime.strptime(wildcards.analysis_date, "%Y%m%d")
                                        - timedelta(days=config["nextstrain_params"]["sampling_timeframe_days"])).strftime("%Y-%m-%d"),
        end_date = lambda wildcards: (datetime.strptime(wildcards.analysis_date, "%Y%m%d")).strftime("%Y-%m-%d"),
        config_file = CONFIG_PATH
    log:
        ssi_meta_log = "trees/{analysis_date}/logs/nextstrain_prep/filter_ssi_log.txt"
    shell:
        """
        python3 "{params.snakemake_path}/parse_ssi_data.py" {input.ssi_meta_file} -o {output.ssi_for_nextstrain} \
         --start_date {params.start_date} --end_date {params.end_date} --logfile {log.ssi_meta_log} \
         --workflow_config_file {params.config_file}
        """

rule concat_nextstrain_data:
    input:
        nextstrain_data = "trees/{analysis_date}/data/own_metadata.tsv",
        ssi_data = rules.filter_ssi_metadata.output.ssi_for_nextstrain,
        all_metadata_checked = expand("trees/{{analysis_date}}/append_checks/{outdir_name}__{run_name}_append_nextstrain",
            zip, outdir_name=outdir_names, run_name=base_names, allow_missing=True)
    output:
        merged_nextstrain = f"{workdir_path}/trees/{{analysis_date}}/data/metadata_merged.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    shell:
        """
        python3 "{params.snakemake_path}/merge_ssi_ouh_metadata.py" {input.nextstrain_data} {input.ssi_data} \
        -o {output.merged_nextstrain}
        """

rule filter_all_sequences:
    input:
        #all_fastas = "/mnt/t/ouh/afd/KMA_sekvenser/SARS2_data/from_ssi/current/sequences.fasta", # TODO change to proper SSI data
        #all_fastas = "/data/KMA_sekvenser/SARS2_data/from_ssi/current/sequences.fasta",
        all_fastas = config["nextstrain_params"]["ssi_sequence_path"],
        meta_file = rules.concat_nextstrain_data.output.merged_nextstrain,
        #append_consensus = rules.concat_all_sequences.output.check_consensus_fastas
    output:
        filtered_fastas = "trees/{analysis_date}/data/ssi_fastas_filtered.fasta"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "biopython_env.yml").resolve())
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    shell:
        """
        python3 {params.snakemake_path}/filter_ssi_fasta.py {input.all_fastas} {input.meta_file} {output.filtered_fastas}
        """
rule append_filtered:
    input:
        filtered_fastas = "trees/{analysis_date}/data/ssi_fastas_filtered.fasta",
        all_consensus =  expand("trees/{{analysis_date}}/append_checks/{outdir_name}__{run_name}_append_fasta",
            zip, outdir_name=outdir_names, run_name=base_names, allow_missing=True),
        own_consensus= rules.create_sequences_base.output.own_consensus_base,
    output:
        merged_fasta = f"{workdir_path}/trees/{{analysis_date}}/data/all_fastas_merged.fasta"
    shell:
        """
        cat {input.filtered_fastas} > {output.merged_fasta} && cat {input.own_consensus} >> {output.merged_fasta}
        """

rule make_nextstrain_config:  # TODO: absolute paths! Also check structure...
    input:
        metadata = f"{workdir_path}/trees/{{analysis_date}}/data/metadata_merged.tsv",
        sequences = f"{workdir_path}/trees/{{analysis_date}}/data/all_fastas_merged.fasta"
    params:
        build_path = str((pathlib.Path(workflow.basedir) / "data" / "nextstrain" / "builds.yaml").resolve()),
        defaults_path = str((pathlib.Path(workflow.basedir) / "ncov").resolve()),
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        adm_division= {config['nextstrain_params']['adm_division']}
    output:  # TODO: move!
        nextstrain_config = "trees/{analysis_date}/config/config.yaml",
        nextstrain_build = "trees/{analysis_date}/config/{analysis_date}_builds.yaml"
    shell:
        """
        python3 "{params.snakemake_path}/make_nextstrain_config.py" {input.metadata} {input.sequences} \
         {params.defaults_path} \
        --builds_out {output.nextstrain_build} --config_out {output.nextstrain_config} \
        --build_name danmark_{params.adm_division}_{analysis_date}
        """
# run Nextstrain
rule run_nextstrain:
    input:
        nextstrain_config = rules.make_nextstrain_config.output.nextstrain_config
    params:
        profile_dir="trees/{analysis_date}/config/",
        nextstrain_path=str((pathlib.Path(workflow.basedir) / "ncov").resolve()),
        output_nextstrain = f"{config['nextstrain_params']['nextstrain_temp_path']}/{analysis_date}",
        #output_nextstrain = "/home/ouh-covid19/temp-nextstrain/{analysis_date}", # TODO make variable
        adm_division = {config['nextstrain_params']['adm_division']}
    output:
        auspice_json = f"trees/{{analysis_date}}/nextstrain/auspice/ncov_danmark_{config['nextstrain_params']['adm_division']}_{{analysis_date}}.json"

    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "nextstrain_env.yml").resolve())
    shell:
        """
        mkdir -p {params.output_nextstrain}
        snakemake -s "{params.nextstrain_path}/Snakefile" --profile "{params.profile_dir}" --config workdir="{params.output_nextstrain}" 
        mv {params.output_nextstrain}/auspice/ncov_danmark_{params.adm_division}_{analysis_date}.json {output.auspice_json}
        """

# move log file to dir for all logs
# adapted from https://stackoverflow.com/a/73215474/15704972

onsuccess:
    shell(f'cat "{{log}}" >> "{workdir_path}/trees/{analysis_date}/logs/snakemake.log"')

onerror:
    shell(f'cat "{{log}}" >> "{workdir_path}/trees/{analysis_date}/logs/snakemake.log"')
