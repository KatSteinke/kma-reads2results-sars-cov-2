#!/usr/bin/env bash
source /home/ouh-covid19/software/miniconda3/etc/profile.d/conda.sh
conda activate snakemake_pipeline_env
python3 "/home/ouh-covid19/sars-cov2-git-new/sars-cov-2-pipeline/check_runsheet.py"