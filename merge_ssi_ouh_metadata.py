"""Merge two Nextstrain metadata files - one from in-house data and one from external data
and give precedence to in-house data in case of duplicates.
"""

__author__ = "Kat Steinke"

import pathlib

from argparse import ArgumentParser

import pandas as pd

import version
__version__ = version.__version__


def merge_own_ssi_data(own_data: pathlib.Path, ssi_data: pathlib.Path) -> pd.DataFrame:
    """Combine a run's Nextstrain metadata with SSI Nextstrain metadata,
    giving priority to the internal metadata in case of duplicated sample
    IDs.
    Arguments:
        own_data:   Nextstrain metadata for the current sequencing run
        ssi_data:   Nextstrain metadata extracted from SSI file, filtered
                    by cutoff date
    Returns:
        The combination of both metadata files
    """
    if not own_data.exists():
        raise FileNotFoundError("File containing own metadata not found.")
    if not ssi_data.exists():
        raise FileNotFoundError("File containing SSI metadata not found.")
    own_data_frame = pd.read_csv(own_data, sep="\t")
    ssi_data_frame = pd.read_csv(ssi_data, sep="\t")
    merged_frame = pd.concat([own_data_frame, ssi_data_frame], ignore_index=True)
    # we now delete duplicated sample numbers, with our own data remaining if present
    # as concat adds the SSI data *after* our own, we can simply drop duplicates and
    # keep the first (default setting)
    merged_frame = merged_frame.drop_duplicates(subset=["strain"], keep="first", ignore_index=True)
    return merged_frame

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Combine a run's Nextstrain metadata file with the Nextstrain metadata "
                                            "file from SSI")
    arg_parser.add_argument("own_metadata", help="Path to Nextstrain metadata file for the current sequencing run")
    arg_parser.add_argument("ssi_metadata", help="Path to Nextstrain metadata file from SSI metadata")
    arg_parser.add_argument("-o", "--outfile", help="Path to output merged metadata file to",
                            default="metadata_merged.tsv")
    args = arg_parser.parse_args()
    own_metadata = pathlib.Path(args.own_metadata)
    ssi_metadata = pathlib.Path(args.ssi_metadata)
    outfile = pathlib.Path(args.outfile)
    if not own_metadata.exists():
        raise FileNotFoundError("File containing own metadata not found.")
    if not ssi_metadata.exists():
        raise FileNotFoundError("File containing SSI metadata not found.")

    merged_data = merge_own_ssi_data(own_metadata, ssi_metadata)
    merged_data.to_csv(path_or_buf=outfile, sep="\t", index=False)
