#!/usr/bin/env bash
source /path/to/miniconda3/etc/profile.d/conda.sh
conda activate nanopore_corona_pipeline
snakemake -s path/to/sars-cov-2-pipeline/count_samples.smk --use-conda --conda-frontend conda --conda-prefix /path/to/sars-cov-2-pipeline/.snakemake/conda --cores 2 &>> /path/to/sars-cov-2-pipeline/logs/sample_report_log
