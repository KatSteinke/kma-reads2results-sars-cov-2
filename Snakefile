__author__ = "Kat Steinke"

import pathlib
import re

from datetime import date, datetime
from glob import glob


import pandas as pd
from snakemake.io import expand

from helpers import get_number_letter_combination
from make_sample_sheet import infer_prefix, translate_and_add_year
import pipeline_config
import version

__version__ = version.__version__

# set default config if none is provided - will be overridden in most setups
configfile: pipeline_config.default_config_file
# path to config file needs to be specified for other scripts
CONFIG_PATH = config["config_path"] if "config_path" in config else pipeline_config.default_config_file

prefix_translation = get_number_letter_combination(config["sample_number_settings"]["number_to_letter"],
    config["sample_number_settings"]["sample_numbers_in"],
    config["sample_number_settings"]["sample_numbers_out"])

# add year only if needed; infer prefix if needed
if "infer_prefix" in config["sample_number_settings"]:
    INFER_PREFIX = config["sample_number_settings"]["infer_prefix"]
else:
    print("Assuming sample numbers in runsheet have prefixes. If prefix should be inferred from LIS"
          "report, please add the respective setting to the config file.")
    INFER_PREFIX = False

if config["sample_number_settings"]["date_settings"]["splice_in_date"]:
    sheet_data = translate_and_add_year(config["runsheet"], config["lab_info_system"]["lis_report"],
        prefix_translation=prefix_translation,
        negk_format=config["sample_number_settings"]["negative_control"],
        positive_controls=config["sample_number_settings"]["positive_control"].keys())
elif config["sample_number_settings"]["infer_prefix"]:  # TODO: move this to files?
    run_sheet_data = pd.read_excel(config["runsheet"],usecols = "A:C",skiprows = 3,
                                   dtype = {"KMA nr": str, "Barkode NB": str})
    run_sheet_data = run_sheet_data.dropna()
    lis_report_data = pd.read_csv(config["lab_info_system"]["lis_report"], encoding = "latin1",
                                  dtype = {"afsendt": str,"cprnr.": str, "modtaget": str})
    negative_control = config["sample_number_settings"]["negative_control"]
    positive_control = list(config["sample_number_settings"]["positive_control"].keys())
    sheet_data = infer_prefix(run_sheet_data,lis_report_data,negk_format = negative_control,
                               positive_controls = positive_control)
else:
    sheet_data = pd.read_excel(config["runsheet"],usecols = "A:C",skiprows = 3,
                               dtype = {"KMA nr": str, "Barkode NB": str})
    sheet_data = sheet_data.dropna()
    sheet_data = sheet_data[["KMA nr", "Barkode NB", "CT/CP værdi"]]

# have KMA number and barcode as expandable things
ALL_IDS = list(sheet_data["KMA nr"])
ALL_BARCODES = list(sheet_data["Barkode NB"])
sheet_without_negk = sheet_data[~sheet_data["KMA nr"].str.match(config["sample_number_settings"]["negative_control"])]
sheet_without_control = sheet_without_negk[
    ~sheet_without_negk["KMA nr"].isin(config["sample_number_settings"]["positive_control"].keys())]
IDS_WITHOUT_CONTROL = list(sheet_without_control["KMA nr"])
BARCODES_WITHOUT_CONTROL = list(sheet_without_control["Barkode NB"])

rundir_path = pathlib.Path(config["rundir"])
RUNDIR_NAME = pathlib.Path(config["rundir"]).name.split("/")[0]
OUTDIR_NAME = str(pathlib.Path(config["outdir"]))  # TODO: simplify by using workdir?

# ensure SSI dir is always a string
config["ssi_dir"] = str(config["ssi_dir"])

# clean SSI dir name without any extensions for filenames etc.
SSI_CLEANED = re.sub(r"_v?[0-9]+(_[0-9]+)?","",config["ssi_dir"])

# check primer version - now always specified
PRIMER_VERSION = str(config["primer_version"])
# set reference/primer file name
PRIMER_NAME = "SARS-CoV-2" if PRIMER_VERSION in {"4", "4.1"} else "nCoV-2019"

# set skipping scorpio - skip if specified, default to false for config files that haven't been
# updated
SKIP_SCORPIO = config.get("skip_scorpio",False)


# for now, sample number format is limited to numbers - can probably change that in our own config
positive_control_pattern = f"{'|'.join(config['sample_number_settings']['positive_control'].keys())}"
sample_id_pattern = f"({config['sample_number_settings']['sample_number_format']}" \
                    f"|{config['sample_number_settings']['negative_control']}" \
                    f"|({positive_control_pattern}))"
barcode_prefix = config["barcode_prefix"]

# constrain wildcards so nonstandard directory names don't cause confusion
wildcard_constraints:
    sample_barcode = config["barcode_format"],
    sample_id = sample_id_pattern

rule run_all_test:
    input:
        manual_report = f"{OUTDIR_NAME}/{RUNDIR_NAME}.results.csv_KMAudsvar.csv",
        metadata_report_file = f"{OUTDIR_NAME}/metadata/{RUNDIR_NAME}_metadata.tsv",
        pyco_qc = f"{OUTDIR_NAME}/read_qc/{RUNDIR_NAME}_qc.html",
        multi_qc = f"{OUTDIR_NAME}/multi_qc/multiqc_report.html",
        screened = f"{OUTDIR_NAME}/screening/{RUNDIR_NAME}_probable_lineage.csv",
        mixed =  f"{OUTDIR_NAME}/{RUNDIR_NAME}_mixed_positions.tsv" # TODO: only until this is rolled into results
    # TODO: get nextclade version
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pangolin_env.yml").resolve())
    shell:
        """
        echo "# Done testing -------------------------------"
        echo "Timestamp:"
        date
        echo "pangolin version"
        pangolin -v
        echo "PangoLEARN version"
        pangolin -pv
        """

rule run_reanalysis:
    input:
        manual_report = f"{OUTDIR_NAME}/{RUNDIR_NAME}.results.csv_KMAudsvar.csv",
        metadata_report_file = f"{OUTDIR_NAME}/metadata/{RUNDIR_NAME}_metadata.tsv",
        all_results_check = f"{OUTDIR_NAME}/{RUNDIR_NAME}-append_check",
        all_fasta_check = f"{OUTDIR_NAME}/{RUNDIR_NAME}_fasta_append_check_reanalysis",
        pyco_qc = f"{OUTDIR_NAME}/read_qc/{RUNDIR_NAME}_qc.html",
        multi_qc = f"{OUTDIR_NAME}/multi_qc/multiqc_report.html",
        screened = f"{OUTDIR_NAME}/screening/{RUNDIR_NAME}_probable_lineage.csv",
        mixed =  f"{OUTDIR_NAME}/{RUNDIR_NAME}_mixed_positions.tsv"  # TODO: only until this is rolled into results

    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pangolin_env.yml").resolve())
    shell:
        """
        echo "# Done with reanalysis -------------------------------"
        echo "Timestamp:"
        date
        echo "pangolin version"
        pangolin -v
        echo "PangoLEARN version"
        pangolin -pv
        """

rule run_all:
    input:
        manual_report = f"{OUTDIR_NAME}/{RUNDIR_NAME}.results.csv_KMAudsvar.csv",
        metadata_report_file = f"{OUTDIR_NAME}/metadata/{RUNDIR_NAME}_metadata.tsv",
        all_results_check = f"{OUTDIR_NAME}/{RUNDIR_NAME}-append_check",
        all_fasta_check = f"{OUTDIR_NAME}/{RUNDIR_NAME}_fasta_append_check",
        pyco_qc = f"{OUTDIR_NAME}/read_qc/{RUNDIR_NAME}_qc.html",
        multi_qc = f"{OUTDIR_NAME}/multi_qc/multiqc_report.html",
        screened = f"{OUTDIR_NAME}/screening/{RUNDIR_NAME}_probable_lineage.csv",
        mixed =  f"{OUTDIR_NAME}/{RUNDIR_NAME}_mixed_positions.tsv"  # TODO: only until this is rolled into results

    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pangolin_env.yml").resolve())
    shell:
        """
        echo "# Done -------------------------------"
        echo "Timestamp:"
        date
        echo "pangolin version"
        pangolin -v
        echo "PangoLEARN version"
        pangolin -pv
        """

rule copy_all_ssi:
    input:
        consensus_moved = expand(f"{OUTDIR_NAME}/{config['ssi_dir']}/{RUNDIR_NAME}_{{sample_id}}_{{sample_barcode}}_check_ssi",
            zip,sample_id = IDS_WITHOUT_CONTROL,sample_barcode = BARCODES_WITHOUT_CONTROL)

rule prep_all_ssi:
    input:
        ssi_zip = f"{OUTDIR_NAME}/{config['ssi_dir']}.zip",
        metadata = f"{OUTDIR_NAME}/metadata/{RUNDIR_NAME}_metadata.tsv"

rule make_sample_sheet:
    input:
        runsheet = config["runsheet"]
    output:
        sample_sheet = "{output_dir}/results/sample_sheet.csv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        lab_info_report = config["lab_info_system"]["lis_report"]
    conda: str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 "{params.snakemake_path}/make_sample_sheet.py" "{input.runsheet}" "{params.lab_info_report}" "{output.sample_sheet}" \
        --config_file {CONFIG_PATH}
        """


# filter reads: rule; input: result dir, work dir, output: moved files
rule filter_reads:
    params:
        result_dir = f"{config['rundir']}/rawdata/*/fastq_pass/barcode{{barcode_number}}",
        # adjust length if Midnight primers
        min_length = 1000 if PRIMER_VERSION in {"1200", "1200.3"} else 400,
        max_length = 1400 if PRIMER_VERSION in {"1200", "1200.3"} else 700
    output:
        filtered_reads = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.fastq"
    message: f"# Filtering reads for sample {{wildcards.sample_id}}_{barcode_prefix}{{wildcards.barcode_number}}...."
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
        artic guppyplex \
        --skip-quality-check \
        --min-length {params.min_length} \
        --max-length {params.max_length} \
        --directory {params.result_dir} \
        --output {output.filtered_reads}


        """

rule download_model:
    params:
        guppy_model = config["guppy_model"]
    output:
        check_model_path = str((
                    pathlib.Path(workflow.basedir) / "data" / "models" / f"{config['guppy_model']}_model.hdf5").resolve())
    shell:
        """curl --fail https://media.githubusercontent.com/media/nanoporetech/medaka/master/medaka/data/{params.guppy_model}_model.hdf5 \
         > "{output.check_model_path}" """

rule download_primers:
    output:
        check_primer_scheme = str((
                    pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / f"V{PRIMER_VERSION}" / "nCoV-2019.scheme.bed").resolve()),
        check_reference = str((
                    pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / f"V{PRIMER_VERSION}" / "nCoV-2019.reference.fasta").resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
         curl --fail https://raw.githubusercontent.com/artic-network/primer-schemes/master/nCoV-2019/V{PRIMER_VERSION}/{PRIMER_NAME}.primer.bed \
         > "{output.check_primer_scheme}"
         curl --fail https://raw.githubusercontent.com/artic-network/primer-schemes/master/nCoV-2019/V{PRIMER_VERSION}/{PRIMER_NAME}.reference.fasta \
         > "{output.check_reference}"

         """

rule download_midnight_primers:
    output:
        check_primer_scheme = str((
                    pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / "V1200" / "nCoV-2019.scheme.bed").resolve()),
        check_reference = str((
                    pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / "V1200" / "nCoV-2019.reference.fasta").resolve())
    params:
        data_dir = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes").resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
         curl --fail https://zenodo.org/record/3897530/files/1200bp_amplicon_bed.tar.gz?download=1 \
         > "{params.data_dir}/midnight_1200bp_amplicon_bed.tar.gz"
        tar -zxf "{params.data_dir}/midnight_1200bp_amplicon_bed.tar.gz" --directory "{params.data_dir}/nCoV-2019"

         """

# TODO: make more flexible?
rule download_ont_midnight:
    output:
        check_primer_scheme = str((
                    pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / "V1200.3" / "nCoV-2019.scheme.bed").resolve()),
        check_reference = str((
                pathlib.Path(workflow.basedir) / "data" / "primer_schemes" / "nCoV-2019" / "V1200.3" / "nCoV-2019.reference.fasta").resolve())
    params:
        data_dir = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes").resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
        curl --fail https://raw.githubusercontent.com/epi2me-labs/wf-artic/master/data/primer_schemes/SARS-CoV-2/Midnight-ONT/V3/SARS-CoV-2.scheme.bed \
        > "{output.check_primer_scheme}"
        curl --fail https://raw.githubusercontent.com/epi2me-labs/wf-artic/master/data/primer_schemes/SARS-CoV-2/Midnight-ONT/V3/SARS-CoV-2.reference.fasta \
        > "{output.check_reference}"
        """

# generate consensus genome
# can we change fasta header here?
# TODO: can we slim this down?

# identify how to get primer scheme: different paths and different methods for different primers

if PRIMER_VERSION == "1200":
    primer_target = rules.download_midnight_primers.output.check_primer_scheme
    reference_target = rules.download_midnight_primers.output.check_reference
elif PRIMER_VERSION == "1200.3":
    primer_target = rules.download_ont_midnight.output.check_primer_scheme
    reference_target = rules.download_ont_midnight.output.check_reference
else:
    primer_target = rules.download_primers.output.check_primer_scheme
    reference_target = rules.download_primers.output.check_reference

if config["artic"]["workflow"] == "medaka":
    rule generate_consensus:
        input:
            read_file = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.fastq",
            medaka_model = rules.download_model.output.check_model_path,
            # path to primer scheme depends on version
            primer_scheme = primer_target,
            reference = reference_target
        params:
            sample_file = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}",
            scheme_dir = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes").resolve()),
            no_longshot = "--no-longshot" if config["artic"]["no_longshot"] else ""
        output:
            consensus_fasta = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.consensus.fasta",
            alignreport = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.alignreport.txt",
            depths_file_1 = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.nCoV-2019_1.depths" \
                if PRIMER_VERSION == "1200" else "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.1.depths",
            depths_file_2 = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.nCoV-2019_2.depths" \
                if PRIMER_VERSION == "1200" else "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.2.depths",
            merged_vcf = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.merged.vcf"
        message: "# Consensus generation for sample {wildcards.sample_id}_{wildcards.sample_barcode}...."
        log: "{output_dir}/logs/artic_minion/{rundir_name}_{sample_id}_{sample_barcode}_artic_minion_log.txt"
        threads: 2  # can we/do we need to scale?
        conda:
            str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
        # run artic minion, create empty consensus fasta if it fails - won't come along in the concatenated consensus
        shell:
            """

            artic minion \
                --medaka \
                --medaka-model "{input.medaka_model}" \
                --normalise 200 \
                --threads {threads} \
                --scheme-directory "{params.scheme_dir}" \
                --read-file {input.read_file} \
                {params.no_longshot} \
                nCoV-2019/V{PRIMER_VERSION} \
               {params.sample_file} &> {log} || \
            {{ printf ">{wildcards.output_dir}/artic/{wildcards.sample_id}_{wildcards.sample_barcode}/{wildcards.rundir_name}_{wildcards.sample_id}_{wildcards.sample_barcode}_FAILED/ARTIC/FAILED\n" > {output.consensus_fasta}; printf 'N%.0s' {{1..29903}} | fold -w 60 >> {output.consensus_fasta}; printf '\n' >> {output.consensus_fasta}; touch {output.depths_file_1}; touch {output.depths_file_2}; }}
            """
    # solution for appending n's: https://stackoverflow.com/questions/5349718/how-can-i-repeat-a-character-in-bash
elif config["artic"]["workflow"] == "nanopolish":
    rule generate_consensus:
        input:
            read_file = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.fastq",
            sequencing_summary = lambda wildcards: glob(f"{config['rundir']}/rawdata/*/sequencing_summary_*.txt"),
            primer_scheme = primer_target,
            reference = reference_target
        params:
            sample_file = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}",
            scheme_dir = str((pathlib.Path(workflow.basedir) / "data" / "primer_schemes").resolve()),
            fast5_directory = lambda wildcards: glob(f"{config['rundir']}/rawdata/*/fast5_pass/barcode{wildcards.barcode_number}"),# TODO: TEST
            hdf5_plugin_path = config["artic"]["hdf5_path"]

        output:
            consensus_fasta = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.consensus.fasta",
            alignreport = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.alignreport.txt",
            depths_file_1 = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.coverage_mask.txt.nCoV-2019_1.depths" \
                if PRIMER_VERSION == "1200" else f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.coverage_mask.txt.1.depths",
            depths_file_2 = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.coverage_mask.txt.nCoV-2019_2.depths" \
                if PRIMER_VERSION == "1200" else f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.coverage_mask.txt.2.depths",
            merged_vcf = f"{{output_dir}}/artic/{{sample_id}}_{barcode_prefix}{{barcode_number}}/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}.merged.vcf"

        message: f"# Consensus generation for sample {{wildcards.sample_id}}_{barcode_prefix}{{wildcards.barcode_number}}...."
        log: f"{{output_dir}}/logs/artic_minion/{{rundir_name}}_{{sample_id}}_{barcode_prefix}{{barcode_number}}_artic_minion_log.txt"
        threads: 2
        conda:
            str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh_nanopolish.yml").resolve())
        # run artic minion, create empty consensus fasta if it fails - won't come along in the concatenated consensus
        shell:
            """
            if [ -d {params.fast5_directory} ] ; then
                export HDF5_PLUGIN_PATH={params.hdf5_plugin_path}
                artic minion \
                    --normalise 200 \
                    --threads {threads} \
                    --scheme-directory "{params.scheme_dir}" \
                    --read-file {input.read_file} \
                    --fast5-directory {params.fast5_directory} \
                    --sequencing-summary {input.sequencing_summary} \
                    nCoV-2019/V{PRIMER_VERSION} \
                   {params.sample_file} &> {log} || \
                {{ printf ">{wildcards.output_dir}/artic/{wildcards.sample_id}_{barcode_prefix}{wildcards.barcode_number}/{wildcards.rundir_name}_{wildcards.sample_id}_{barcode_prefix}{wildcards.barcode_number}_FAILED/ARTIC/FAILED\n" > {output.consensus_fasta}; printf 'N%.0s' {{1..29903}} | fold -w 60 >> {output.consensus_fasta}; printf '\n' >> {output.consensus_fasta}; touch {output.depths_file_1}; touch {output.depths_file_2}; }}
            else {{ printf ">{wildcards.output_dir}/artic/{wildcards.sample_id}_{barcode_prefix}{wildcards.barcode_number}/{wildcards.rundir_name}_{wildcards.sample_id}_{barcode_prefix}{wildcards.barcode_number}_FAILED/ARTIC/FAILED\n" > {output.consensus_fasta}; printf 'N%.0s' {{1..29903}} | fold -w 60 >> {output.consensus_fasta}; printf '\n' >> {output.consensus_fasta}; touch {output.depths_file_1}; touch {output.depths_file_2}; }}
            fi
            """
else:
    raise ValueError(f"{config['artic']['workflow']} is not a valid ARTIC workflow. Valid options are medaka or nanopolish.")

# copy consensus to result dir
rule prepare_results:
    input:
        consensus_fasta = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.consensus.fasta"
    output:
        copied_consensus = "{output_dir}/results/{rundir_name}_{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.consensus.fasta"
    message: "# Prepare results for upload -------------------------------"
    shell:
        """
        cp {input.consensus_fasta} {output.copied_consensus}
        """


# move relevant files to SSI directory, remove negative control

rule copy_consensus_ssi:
    input:
        consensus_fasta = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.consensus.fasta"
    params:
        consensus_moved = f"{{output_dir}}/{config['ssi_dir']}/{SSI_CLEANED}.{{sample_id}}.fasta"
    output:
        check_copy_consensus = temp(f"{{output_dir}}/{config['ssi_dir']}/{{rundir_name}}_{{sample_id}}_{{sample_barcode}}_check_ssi")
    shell:
        """
        cp {input.consensus_fasta} {params.consensus_moved} && touch {output.check_copy_consensus}
        """

# concatenate fastas
rule concatenate_fastas:
    input:
        consensus_fasta = expand("{{output_dir}}/artic/{sample_id}_{sample_barcode}/{{rundir_name}}_{sample_id}_{sample_barcode}.consensus.fasta",
            zip,sample_id = ALL_IDS,sample_barcode = ALL_BARCODES,allow_missing = True)
    params:
        artic_dir = "{output_dir}/artic"
    output:
        consensus_fasta = "{output_dir}/{rundir_name}.all.consensus.fasta"
    message: "concatenating fasta files"
    shell:
        """
        cat {params.artic_dir}/*/*.consensus.fasta > {output.consensus_fasta} 

        """

rule add_to_all_fastas:
    input:
        consensus_fasta = "{output_dir}/{rundir_name}.all.consensus.fasta"
    params:
        all_fasta_path = config["paths"]["all_fasta_path"]
    output:
        check_add_fasta = "{output_dir}/{rundir_name}_fasta_append_check"
    shell:
        """
        cp {input.consensus_fasta} "{params.all_fasta_path}/{wildcards.rundir_name}.all.consensus.fasta"
        touch {output.check_add_fasta}
        """

rule add_new_to_all_fastas:
    input:
        consensus_fasta = "{output_dir}/{rundir_name}.all.consensus.fasta"
    params:
        all_fasta_path = config["paths"]["all_fasta_path"],
        analysis_time = datetime.now().strftime("%Y-%m-%d-%H.%M")
    output:
        check_add_fasta = "{output_dir}/{rundir_name}_fasta_append_check_reanalysis"
    shell:
        """
        cp {input.consensus_fasta} "{params.all_fasta_path}/{wildcards.rundir_name}.reanalysis.{params.analysis_time}.all.consensus.fasta"
        touch {output.check_add_fasta}
        """

# identify potentially contaminated samples
rule find_mixed_positions:
    input:
        merged_vcf = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.merged.vcf",
        reference_fasta = reference_target
    output:
        single_sample_mixed = "{output_dir}/mixed_positions/{rundir_name}_{sample_id}_{sample_barcode}.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        min_depth = 20,
        min_alt_proportion = 0.1,
        max_alt_proportion = 0.7
    conda: str((pathlib.Path(workflow.basedir) / "envs" / "biopython_env.yml").resolve())
    log: "{output_dir}/logs/mixed_positions/{rundir_name}_{sample_id}_{sample_barcode}_mixed_pos.log"
    shell:
        """
        python3 {params.snakemake_path}/detect_mixed_positions.py "{input.merged_vcf}" \
        --outfile "{output.single_sample_mixed}" --min_depth {params.min_depth} \
        --min_alt {params.min_alt_proportion} --max_alt {params.max_alt_proportion} \
        --reference_sequence "{input.reference_fasta}" --logfile {log}
        """


rule get_all_mixed:
    input:
        mixed_per_sample = expand(f"{OUTDIR_NAME}/mixed_positions/{RUNDIR_NAME}_{{sample_id}}_{{sample_barcode}}.tsv",
                                  zip,sample_id = ALL_IDS,sample_barcode = ALL_BARCODES)
    output:
        mixed_all = f"{OUTDIR_NAME}/{RUNDIR_NAME}_mixed_positions.tsv"
    shell:
        """
        printf "proevenr\tPOS\tREF\tALT\tdepth\tref_count\talt_count\tproportion_ref\tproportion_alt\\n" > {output.mixed_all}
        tail --lines +2 --quiet {input.mixed_per_sample} >> {output.mixed_all}
        
        """


# TODO: test implementation of database update - currently goes nowhere because of network errors
# rule update_pango_db:
#     params:
#         data_dir = str((pathlib.Path(workflow.basedir) / "data" / "pangolin").resolve())
#     output:
#         check_pango_db = temp("{output_dir}/check_update_pango_db")
#     conda:
#         str((pathlib.Path(workflow.basedir) / "envs" / "newkmacovid_minimal.yml").resolve())
#     message: "Updating pangoLEARN database"
#     shell:
#         """
#         echo "Pangolin designations at version"
#         pangolin -dv
#         echo "PangoLEARN database at version"
#         pangolin -pv
#         pangolin --update-data --datadir {params.data_dir} && \
#         touch {output.check_pango_db}
#         echo "Updated Pangolin designations to"
#         pangolin -dv
#         echo "Updated PangoLEARN to"
#         pangolin -pv
#         """

rule update_pango_db_manual:
    params:
        data_dir = str((pathlib.Path(workflow.basedir) / "data" / "pangolin").resolve())
    output:
        check_pango_db = temp("{output_dir}/check_update_pango_db_manual")
    message: "Downloading newest pangoLEARN database"
    # only download the zip if it's newer than the version already downloaded
    # overwrite existing files only if they're older than the ones in the zip file, add any new ones
    shell:
        """
        wget -N --directory-prefix "{params.data_dir}" https://github.com/cov-lineages/pangoLEARN/archive/refs/heads/master.zip && \
        unzip -o -u "{params.data_dir}/master.zip" -d "{params.data_dir}"  && \
        touch {output.check_pango_db}
        """

# run pangolin
rule run_pangolin:
    input:
        consensus_fasta = "{output_dir}/{rundir_name}.all.consensus.fasta"
    output:
        lineage_report = "{output_dir}/{rundir_name}.all.consensus.lineage_report.csv"
    params:
        analysis_mode = {config["pango_prediction"]},
        skip_scorpio = "--skip-scorpio" if SKIP_SCORPIO else ""
    message: "running pangolin"
    log: "{output_dir}/logs/pangolin/{rundir_name}_log.txt"
    threads: workflow.cores  # majority of downstream results depends on this
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pangolin_env.yml").resolve())
    shell:
        """
        echo "Pangolin version"
        pangolin -v
        pangolin {input.consensus_fasta} --outfile {output.lineage_report} --threads {threads} \
        --analysis-mode {params.analysis_mode} {params.skip_scorpio} &> {log}
        """

# may not work because of SSL issues... again
# rule download_nextclade_dataset:
#     output:
#         check_nextclade_download = str(pathlib.Path(workflow.basedir) / "data" / "nextclade"
#                                        /f"download_check_{date.strftime(date.today(), '%Y-%m-%d')}_nextclade")
#     params:
#         nextclade_dataset_path = str(pathlib.Path(workflow.basedir) / "data" / "nextclade")
#     message: "Downloading Nextclade dataset"
#     shell:
#         """
#         nextclade dataset get --name 'sars-cov-2' --output-dir '{params.nextclade_dataset_path}'
#         touch {output.check_nextclade_download}
#         """


# run nextclade
rule run_nextclade:
    input:
        consensus_fasta = "{output_dir}/{rundir_name}.all.consensus.fasta",
    #check_nextclade_download = str(pathlib.Path(workflow.basedir) / "data" / "nextclade"
    #                               /f"download_check_{date.strftime(date.today(), '%Y-%m-%d')}_nextclade")
    output:
        nextclade_report = "{output_dir}/{rundir_name}.all.consensus.nextclade.csv",
        nextclade_errors = "{output_dir}/nextclade/{rundir_name}.all.consensus.errors.csv"
    params:
        nextclade_dataset_path = str(pathlib.Path(workflow.basedir) / "data" / "nextclade"),
        nextclade_output_dir = "{output_dir}/nextclade"
    threads: workflow.cores  # this is a bottleneck for the other results
    message: "running nextclade"
    log: "{output_dir}/logs/nextclade/{rundir_name}_nextclade.txt"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "newkmacovid_minimal.yml").resolve())
    shell:
        """
        echo "nextclade version"
        nextclade --version
        nextclade run --input-dataset={params.nextclade_dataset_path} --jobs={threads} \
        --output-csv={output.nextclade_report} --output-all={params.nextclade_output_dir} \
        --output-basename={wildcards.rundir_name}.all.consensus \
        --output-translations {params.nextclade_output_dir}/{wildcards.rundir_name}.gene_{{gene}}.translation.fasta \
        {input.consensus_fasta} &> {log}
        """


rule combine_results:
    input:
        lineage_report = "{output_dir}/{rundir_name}.all.consensus.lineage_report.csv",
        nextclade_report = "{output_dir}/{rundir_name}.all.consensus.nextclade.csv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_path = CONFIG_PATH
    output:
        combined_result = "{output_dir}/{rundir_name}.results.csv"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """python3 {params.snakemake_path}/merge_pango_nextstrain.py {input.lineage_report} \
        {input.nextclade_report} {output.combined_result} \
         --workflow_config_file {params.config_path}"""


# append to all_results
rule append_to_all:
    input:
        combined_result = "{output_dir}/{rundir_name}.results.csv",
        all_results = config["paths"]["all_results_path"],
        cleaned_results = config["paths"]["cleaned_all_results"],
        sample_sheet = "{output_dir}/results/sample_sheet.csv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    output:
        all_results_check = temp("{output_dir}/{rundir_name}-append_check")
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 {params.snakemake_path}/append_to_all_results.py {input.combined_result} \
        {input.all_results} {input.cleaned_results} {input.sample_sheet} \
        --workflow_config_file {CONFIG_PATH} \
        && touch {output.all_results_check}
        """

# create MADS files
rule create_mads_files:
    input:
        combined_results = "{output_dir}/{rundir_name}.results.csv",
        sample_sheet = rules.make_sample_sheet.output.sample_sheet,
        mixed_positions = rules.get_all_mixed.output.mixed_all
    params:
        ssi_dir = config["ssi_dir"],
        rscript_path = str((pathlib.Path(workflow.basedir) / config["lab_info_system"][
            "script_for_importable_file"]).resolve()),

    output:
        manual_report = "{output_dir}/{rundir_name}.results.csv_KMAudsvar.csv"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "newkmacovid_minimal.yml").resolve())
    shell:
        """
        Rscript '{params.rscript_path}' {input.combined_results} {input.sample_sheet} '{input.mixed_positions}' '{CONFIG_PATH}' && \
        echo "Done. Result files in {wildcards.output_dir}. Files for upload in {wildcards.output_dir}/{params.ssi_dir}"
        """


# zip up SSI dir
rule zip_all_ssi:
    input:
        all_ssi_fastas = expand(f"{OUTDIR_NAME}/{config['ssi_dir']}/{RUNDIR_NAME}_{{sample_id}}_{{sample_barcode}}_check_ssi",
                                zip, sample_id=IDS_WITHOUT_CONTROL, sample_barcode=BARCODES_WITHOUT_CONTROL)
    params:
        ssi_fasta = expand(f"{OUTDIR_NAME}/{config['ssi_dir']}/{SSI_CLEANED}.{{sample_id}}.fasta",
            sample_id = IDS_WITHOUT_CONTROL,
            allow_missing = True)
    output:
        ssi_zip = f"{OUTDIR_NAME}/{config['ssi_dir']}.zip"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        zip -r -j -0 "{output.ssi_zip}" {params.ssi_fasta}  && \
        echo "Done with zip-file. You will need to check metadata.tsv"
        """


rule get_metadata:
    input:
        runsheet = config["runsheet"],
        consensus_fastas = expand(f"{OUTDIR_NAME}/artic/{{sample_id}}_{{sample_barcode}}/{RUNDIR_NAME}_{{sample_id}}_{{sample_barcode}}.consensus.fasta",
            zip,sample_id = ALL_IDS,sample_barcode = ALL_BARCODES,
            allow_missing = True),
        ssi_zip = f"{{output_dir}}/{config['ssi_dir']}.zip",
        sample_sheet = "{output_dir}/results/sample_sheet.csv"
    params:
        mads_report = config["lab_info_system"]["lis_report"],
        ssi_dir = config["ssi_dir"],
        consensus_fasta_dir = "{output_dir}/artic",# TODO: look at SSI dir stuff instead?
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())

    output:
        metadata_report_file = f"{{output_dir}}/metadata/{RUNDIR_NAME}_metadata.tsv"
    log:
        metadata_log = "{output_dir}/logs/metadata/metadata_log.txt"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 {params.snakemake_path}/extract_metadata.py "{params.mads_report}" \
         "{params.consensus_fasta_dir}" "{input.sample_sheet}" "{input.runsheet}" "{params.ssi_dir}" \
          -l {log.metadata_log} -o {output.metadata_report_file} \
         --workflow_config_file {CONFIG_PATH}
        """

rule read_qc:
    input:
        # solution from https://bioinformatics.stackexchange.com/a/7190 - TODO: can this be made more robust w/pathlib?
        sequencing_summary = lambda wildcards: glob(f"{config['rundir']}/rawdata/*/sequencing_summary_*.txt")
    output:
        read_quality = f"{OUTDIR_NAME}/read_qc/{RUNDIR_NAME}_qc.html"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "quality_control-env.yml").resolve())
    shell:
        """
        pycoQC --summary_file {input.sequencing_summary} --html_outfile {output.read_quality}

        """


rule generate_artic_qc:
    input:
        primer_scheme = rules.download_midnight_primers.output.check_primer_scheme if PRIMER_VERSION == "1200" else rules.download_primers.output.check_primer_scheme,
        alignreport = f"{{output_dir}}/artic/{{sample_id}}_{{sample_barcode}}/{{rundir_name}}_{{sample_id}}_{{sample_barcode}}.alignreport.txt",
    output:
        amplicon_plot = "{output_dir}/multi_qc/{rundir_name}_{sample_id}_{sample_barcode}.amplicon_plot_data_mqc.json",
        amplicon_data = "{output_dir}/multi_qc/{rundir_name}_{sample_id}_{sample_barcode}.amplicon_stats_data_mqc.json"
    params:
        sample_name = "{output_dir}/multi_qc/{rundir_name}_{sample_id}_{sample_barcode}"
    message:
        "# Running amplicon quality control"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "covid19_onsite_dk_ouh.yml").resolve())
    shell:
        """
        artic_get_stats {params.sample_name} --scheme {input.primer_scheme} --align-report {input.alignreport}
        """

# TODO: generate and export image and include in MQC?
rule get_depth_by_position:
    input:
        depths_file_1 = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.nCoV-2019_1.depths" \
            if PRIMER_VERSION == "1200" else "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.1.depths",
        depths_file_2 = "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.nCoV-2019_2.depths" \
            if PRIMER_VERSION == "1200" else "{output_dir}/artic/{sample_id}_{sample_barcode}/{rundir_name}_{sample_id}_{sample_barcode}.coverage_mask.txt.2.depths",
    output:
        depth_by_position = "{output_dir}/multi_qc/{rundir_name}_{sample_id}_{sample_barcode}_mqc.jpg"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    conda: str((pathlib.Path(workflow.basedir) / "envs" / "r_datavis_env.yml").resolve())
    shell:
        """
        Rscript {params.snakemake_path}/Rscripts/plot_depth_by_position.R {input.depths_file_1} {input.depths_file_2} {output.depth_by_position} 
        """

rule visualize_mutations:
    input:
        all_results = f"{OUTDIR_NAME}/{RUNDIR_NAME}.results.csv"
    params:
        base_path = f"{OUTDIR_NAME}/multi_qc/",
        r_path = str((pathlib.Path(workflow.basedir) / "Rscripts").resolve())
    output:
        all_mutations = f"{OUTDIR_NAME}/multi_qc/all_mutations_mqc.png"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "mutation_vis_trackview.yml").resolve())
    shell:
        """
        Rscript {params.r_path}/visualize_mutations.R {input.all_results} {params.base_path}
        """

rule track_mixed_positions:
    input:
        all_mixed_positions = f"{OUTDIR_NAME}/{RUNDIR_NAME}_mixed_positions.tsv",
        sample_sheet = f"{OUTDIR_NAME}/results/sample_sheet.csv"
    output:
        mixed_positions_plot = f"{OUTDIR_NAME}/multi_qc/mixed_positions_mqc.png"
    params:
        r_path = str((pathlib.Path(workflow.basedir) / "Rscripts").resolve())
    conda: str((pathlib.Path(workflow.basedir) / "envs" / "r_datavis_env.yml").resolve())
    shell:
        """
        Rscript {params.r_path}/track_mixed_positions.R {input.all_mixed_positions} {input.sample_sheet} {output.mixed_positions_plot}
        """


# add buttons to show/hide samples by barcode
rule make_multiqc_config:
    input:
        sample_sheet = f"{OUTDIR_NAME}/results/sample_sheet.csv"
    output:
        multiqc_barcode_configfile = f"{OUTDIR_NAME}/multi_qc/{RUNDIR_NAME}_multiqc_config.tsv",
        multiqc_modules_config = f"{OUTDIR_NAME}/multi_qc/{RUNDIR_NAME}_multiqc_config.yaml"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        rundir = RUNDIR_NAME
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 {params.snakemake_path}/generate_multiqc_config.py {input.sample_sheet} {output.multiqc_barcode_configfile} \
        {output.multiqc_modules_config} {params.rundir}
        """

rule multiqc:
    input:
        amplicon_files = expand(f"{OUTDIR_NAME}/multi_qc/{RUNDIR_NAME}_{{sample_id}}_{{sample_barcode}}.amplicon_plot_data_mqc.json",
            zip,sample_id = ALL_IDS,
            sample_barcode = ALL_BARCODES),
        depth_by_position = expand(f"{OUTDIR_NAME}/multi_qc/{RUNDIR_NAME}_{{sample_id}}_{{sample_barcode}}_mqc.jpg",
            zip,sample_id = ALL_IDS,sample_barcode = ALL_BARCODES),
        mutations = f"{OUTDIR_NAME}/multi_qc/all_mutations_mqc.png",
        mixed_positions = f"{OUTDIR_NAME}/multi_qc/mixed_positions_mqc.png",
        config_file = rules.make_multiqc_config.output.multiqc_barcode_configfile,
        multiqc_config = rules.make_multiqc_config.output.multiqc_modules_config
    output:
        qc_report = f"{OUTDIR_NAME}/multi_qc/multiqc_report.html"
    message:
        "# Generating multiqc report"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "quality_control-env.yml").resolve())
    shell:
        """
        multiqc {OUTDIR_NAME}/multi_qc -o {OUTDIR_NAME}/multi_qc --sample-filters {input.config_file} --config {input.multiqc_config}
        """


#### SCREENING MODULES
rule screen_for_probable:
    input:
        merged_results = "{output_dir}/{rundir_name}.results.csv"
    params:
        snakemake_dir = str(pathlib.Path(workflow.basedir))
    output:
        screened_for_probable = "{output_dir}/screening/{rundir_name}_probable_lineage.csv"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 {params.snakemake_dir}/screen_for_flag.py {input.merged_results} --search_column scorpio_call \
        --search_term "Probable" -o {output.screened_for_probable} 
        """

# EXPERIMENTAL STUFF STARTS HERE
rule nextstrain_metadata:
    input:
        own_metadata = rules.get_metadata.output.metadata_report_file,
        sample_sheet = rules.make_sample_sheet.output.sample_sheet,
        own_results = rules.combine_results.output.combined_result
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
    output:
        meta_for_nextstrain = "{output_dir}/nextstrain/{rundir_name}_nextstrain_metadata.tsv"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 "{params.snakemake_path}/parse_ouh_metadata.py" {input.own_metadata} -o {output.meta_for_nextstrain} \
        --workflow_config_file {CONFIG_PATH}
        """

rule filter_ssi_metadata:
    input:
        ssi_meta_file = config["nextstrain_params"]["ssi_metadata_path"]
    output:  # TODO: move?
        ssi_for_nextstrain = f"{OUTDIR_NAME}/nextstrain/{date.strftime(date.today(),'%Y-%m-%d')}_ssi_filtered.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    log:
        ssi_meta_log = f"{OUTDIR_NAME}/logs/nextstrain_prep/filter_ssi_log.txt"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 "{params.snakemake_path}/parse_ssi_data.py" {input.ssi_meta_file} -o {output.ssi_for_nextstrain} \
         --logfile {log.ssi_meta_log} --workflow_config_file {CONFIG_PATH}
        """

# concat metadata - could probably also do this as a shell thing?
rule concat_nextstrain_data:
    input:
        nextstrain_data = rules.nextstrain_metadata.output.meta_for_nextstrain,
        ssi_data = rules.filter_ssi_metadata.output.ssi_for_nextstrain
    output:
        merged_nextstrain = "{output_dir}/nextstrain/{rundir_name}_metadata_merged.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 "{params.snakemake_path}/merge_ssi_ouh_metadata.py" {input.nextstrain_data} {input.ssi_data} \
        -o {output.merged_nextstrain}
        """

# TODO: restructure! Append to filtered; change filtering routine to also remove seqs that are in the current run
rule filter_all_sequences:
    input:
        all_fastas = config["nextstrain_params"]["ssi_sequence_path"],
        meta_file = rules.concat_nextstrain_data.output.merged_nextstrain,
    output:
        filtered_fastas = "{output_dir}/nextstrain/{rundir_name}_filtered.fasta"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "biopython_env.yml").resolve())
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    shell:
        """
        python3 {params.snakemake_path}/filter_ssi_fasta.py {input.all_fastas} {input.meta_file} {output.filtered_fastas}
        """

rule concat_all_sequences:
    input:
        consensus_fasta = "{output_dir}/{rundir_name}.all.consensus.fasta",
        filtered_fastas = rules.filter_all_sequences.output.filtered_fastas,
        reference = rules.download_midnight_primers.output.check_reference if PRIMER_VERSION == "1200" else rules.download_primers.output.check_reference
    output:
        check_consensus_fastas = "{output_dir}/{rundir_name}_append_all_sequences"
    shell:  # append reference here?
        """
        sed -E 's/^>.+\\/[-a-zA-Z0-9_]+_70([0-9]{{8}})_NB[0-9]{{2}}\\/ARTIC\\/medaka MN908947\\.3/>OUH-P\\1/g; s/^>.+\\/[-a-zA-Z0-9_]+_40([0-9]{{8}})_NB[0-9]{{2}}\\/ARTIC\\/medaka MN908947\\.3/>OUH-H\\1/g' {input.consensus_fasta} >> {input.filtered_fastas} && \
        touch {output.check_consensus_fastas}
        """

rule append_reference:
    input:
        filtered_fastas = rules.filter_all_sequences.output.filtered_fastas,
        check_consensus_fastas = rules.concat_all_sequences.output.check_consensus_fastas,
        reference = rules.download_midnight_primers.output.check_reference if PRIMER_VERSION == "1200" else rules.download_primers.output.check_reference,
    output:
        check_append_reference = touch("{output_dir}/{rundir_name}_append_reference")
    shell:  # if the reference isn't in the filtered fastas, append header and the reference sequence
        """
        if ! grep -q "Wuhan" {input.filtered_fastas}; then printf ">Wuhan/Hu-1/2019\n" >> {input.filtered_fastas} && tail -n +2 {input.reference} >> {input.filtered_fastas}; fi
        """

# nextstrain config files are stored in the repo, whose location varies - config file needs to be constructed dynamically
rule make_nextstrain_config:  # TODO: check structure
    input:
        metadata = f"{OUTDIR_NAME}/nextstrain/{RUNDIR_NAME}_metadata_merged.tsv",
        sequences = f"{OUTDIR_NAME}/nextstrain/{RUNDIR_NAME}_filtered.fasta",
        check_ref_appended = f"{OUTDIR_NAME}/{RUNDIR_NAME}_append_reference"
    params:
        build_path = str((pathlib.Path(workflow.basedir) / "data" / "nextstrain" / "builds.yaml").resolve()),
        defaults_path = str((pathlib.Path(workflow.basedir) / "ncov").resolve()),
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        date_today = date.strftime(date.today(),"%Y%m%d"),
        adm_division = config["nextstrain_params"]["adm_division"]
    output:
        nextstrain_config = f"{OUTDIR_NAME}/nextstrain/config.yaml",
        nextstrain_build = f"{OUTDIR_NAME}/nextstrain/{RUNDIR_NAME}_builds.yaml"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 "{params.snakemake_path}/make_nextstrain_config.py" {input.metadata} {input.sequences} \
         {params.defaults_path} \
        --builds_out {output.nextstrain_build} --config_out {output.nextstrain_config} \
        --build_name danmark_{params.adm_division}_{params.date_today} --division {params.adm_division}
        """

rule nextstrain:
    input:
        nextstrain_config = rules.make_nextstrain_config.output.nextstrain_config
    params:
        profile_dir = "{output_dir}/nextstrain/",
        nextstrain_path = str((pathlib.Path(workflow.basedir) / "ncov").resolve()),
        output_nextstrain = config['nextstrain_params']['nextstrain_temp_path'],# TODO fix solution
        date_today = date.strftime(date.today(),'%Y%m%d')
    output:  # TODO: redirect auspice output somehow?
        # TODO: change auspice JSON thing
        auspice_json = f"{{output_dir}}/nextstrain/auspice/ncov_danmark_{config['nextstrain_params']['adm_division']}_{date.strftime(date.today(),'%Y%m%d')}.json"
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "nextstrain_env.yml").resolve())
    shell:
        """
        snakemake -s "{params.nextstrain_path}/Snakefile" --profile "{params.profile_dir}" --config workdir="{params.output_nextstrain}" 
        mv {params.output_nextstrain}/auspice/ncov_danmark_rsyd_{params.date_today}.json {output.auspice_json}
        """

# move log file to dir for all logs
# adapted from https://stackoverflow.com/a/73215474/15704972

onsuccess:
    shell(f'cat "{{log}}" >> "{OUTDIR_NAME}/logs/snakemake.log"')

onerror:
    shell(f'cat "{{log}}" >> "{OUTDIR_NAME}/logs/snakemake.log"')
