#!/usr/bin/env bash
source /home/ouh-covid19/software/miniconda3/etc/profile.d/conda.sh
conda activate snakemake_pipeline_env
snakemake -s /home/ouh-covid19/sars-cov2-git-new/sars-cov-2-pipeline/nextstrain_multirun.smk --use-conda --conda-frontend conda --conda-prefix /home/ouh-covid19/sars-cov2-git-repo/sars-cov-2-pipeline/.snakemake/conda --cores 2 &>> /home/ouh-covid19/sars-cov2-git-new/sars-cov-2-pipeline/logs/nextstrain_log
