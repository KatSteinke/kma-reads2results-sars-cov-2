__author__ = "Kat Steinke"

import pathlib

from datetime import date, datetime

import version
__version__ = version.__version__

configfile: str(pathlib.Path(workflow.basedir) / "config" / "pipeline_routine.yaml")

# output paths are absolute as determined by config, but Rscript needs to be run in pipeline basedir
workdir: workflow.basedir

CONFIG_PATH = config["config_path"] if "config_path" in config else str(pathlib.Path(workflow.basedir)
                                                                        / "config" / "pipeline_routine.yaml")
# set a cutoff date if needed - mainly for testing, but can be used for trying to reconstruct missed
# reports
# if no cutoff given, use the current date
report_day = datetime.strptime(config["cutoff_date"], "%Y-%m-%d") if "cutoff_date" in config \
             else date.today()
iso_date = report_day.isocalendar()
this_year = iso_date[0] # iso_date.year - TODO: why does this not work in Snakemake when it works outside
this_week = iso_date[1] # iso_date.week

FASTA_PATH = pathlib.Path(config['paths']['all_fasta_path'])

# set skipping scorpio - skip if specified, default to false for config files that haven't been
# updated
SKIP_SCORPIO = config.get("skip_scorpio", False)

# target output: week's report
rule run_all:
    input:
        weekly_report = str(FASTA_PATH / "reports" / f"{this_year}-{this_week}" / f"{this_year}-{this_week}_rapport.html")

# combine all fasta files
rule combine_fastas:
    input:
        all_consensus_fastas = lambda wildcards: [str(fasta.resolve())
                                                  for fasta in FASTA_PATH.glob("*all.consensus.fasta")]
    output:
        consensus_fastas_combined = str(FASTA_PATH / "combined_fastas" / "{report_year}-{report_week}.all.consensus.fasta")
    shell:
        """
        cat {input.all_consensus_fastas} > {output.consensus_fastas_combined}
        """

# run analysis on all fasta files
rule pangolin_all:
    input:
        consensus_fastas_combined = str(FASTA_PATH / "combined_fastas" / "{report_year}-{report_week}.all.consensus.fasta")
    output:
        all_fastas_pangolin = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}_pangolin.csv")
    params:
        analysis_mode = config["pango_prediction"],
        skip_scorpio= "--skip-scorpio" if SKIP_SCORPIO else ""
    message: "running pangolin"
    log: str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "logs" /"pangolin" / "{report_year}-{report_week}_log.txt")
    threads: workflow.cores  # majority of downstream results depends on this
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pangolin_env.yml").resolve())
    shell:
        """
        echo "Pangolin version"
        pangolin -v
         pangolin {input.consensus_fastas_combined} --outfile {output.all_fastas_pangolin} \
        --threads {threads} \
        --analysis-mode {params.analysis_mode} {params.skip_scorpio} &> {log}
        """

rule nextclade_all:
    input:
        consensus_fastas_combined = str(FASTA_PATH / "combined_fastas" / "{report_year}-{report_week}.all.consensus.fasta")
    output:
        nextclade_report = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}.all.consensus.nextclade.csv"),
        nextclade_errors = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "nextclade" / "{report_year}-{report_week}.all.consensus.errors.csv")
    params:
        nextclade_dataset_path = str(pathlib.Path(workflow.basedir) / "data" / "nextclade"),
        nextclade_output_dir = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "nextclade")
    threads: workflow.cores # this is a bottleneck for the other results
    message: "running nextclade"
    log: str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "logs" / "nextclade" / "{report_year}-{report_week}_log.txt")
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "newkmacovid_minimal.yml").resolve())
    shell:
        """
        echo "nextclade version"
        nextclade --version
        nextclade run \
        --input-dataset={params.nextclade_dataset_path} \
        --jobs={threads} --output-csv={output.nextclade_report} \
        --output-basename={wildcards.report_year}-{wildcards.report_week}.all.consensus \
        --output-all={params.nextclade_output_dir} {input.consensus_fastas_combined}  &> {log}
        """


rule combine_all_results:
    input:
        all_fastas_pangolin = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}_pangolin.csv"),
        nextclade_report = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" /"{report_year}-{report_week}.all.consensus.nextclade.csv")
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_path = CONFIG_PATH
    output:
        raw_result = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}.raw_results.csv")
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pipeline_env.yml").resolve())
    shell:
        """
        python3 {params.snakemake_path}/merge_pango_nextstrain.py \
        {input.all_fastas_pangolin} \
        {input.nextclade_report} {output.raw_result} --workflow_config_file {params.config_path}
        """

# filter by sequences present in cleaned output
rule filter_all_results:
    input:
        raw_result = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}.raw_results.csv"),
        cleaned_results = config["paths"]["cleaned_all_results"]
    output:
        cleaned_rerun_result = str(FASTA_PATH / "reports" / "{report_year}-{report_week}"
                                   / "{report_year}-{report_week}.rerun_cleaned.csv")
    params:
        config_path = CONFIG_PATH,
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "pandas_env.yml").resolve())
    log: str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "logs" / "filter_results" / "{report_year}-{report_week}_log.txt")
    shell:
        """
        python3 {params.snakemake_path}/filter_by_cleaned.py {input.raw_result} {input.cleaned_results} \
        --outfile {output.cleaned_rerun_result} --workflow_config_file {params.config_path} \
        --logfile {log}
        """


# generate report
rule make_report:
    input:
        cleaned_rerun_result = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}.rerun_cleaned.csv")
    output:
        weekly_report = str(FASTA_PATH / "reports" / "{report_year}-{report_week}" / "{report_year}-{report_week}_rapport.html")
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        report_dir = str(FASTA_PATH / "reports" / "{report_year}-{report_week}"),
        report_date= report_day.strftime("%Y-%m-%d"),
        configfile = CONFIG_PATH
    conda:
        str((pathlib.Path(workflow.basedir) / "envs" / "report_env.yml").resolve())
    shell:
        """
        Rscript {params.snakemake_path}/Rscripts/count_samples.R {params.configfile} \
        {params.report_dir} {input.cleaned_rerun_result} {params.report_date}
        """

# move log file to dir for all logs
# adapted from https://stackoverflow.com/a/73215474/15704972

onsuccess:
    shell(f'cat "{{log}}" >> "{FASTA_PATH}/reports/{this_year}-{this_week}/logs/snakemake.log"')

onerror:
    shell(f'cat "{{log}}" >> "{FASTA_PATH}/reports/{this_year}-{this_week}/logs/snakemake.log"')
